<?php 
	class Administrators extends CI_Controller {

		function __construct(){
			parent::__construct();
			authenticate();

			$this->load->model('administrator_model');
			$this->load->model('activity_model');
		}

		function index(){
			only_super_administrator();
			$data['section_title'] = $this->lang->line('administrators');
			$data['administrators'] = $this->administrator_model->get_administrators();
			$data['section'] = $this->load->view('/administrators/list', $data, true); 
			
			$this->load->view('/template/index', $data);
		}

		function user(){
			$user_id = $this->uri->segment(3, 0);
			$data['section_title'] = $this->lang->line('administrators');
			$data['user'] = $this->administrator_model->get_administrator($user_id);
			$data['tables'] = $this->administrable_table_model->get_tables();
			$data['section'] = $this->load->view('/administrators/user', $data, true); 
			
			$this->load->view('/template/index', $data);
		}

		function create(){
			$data['section_title'] = $this->lang->line('administrators');
			$data['tables'] = $this->administrable_table_model->get_tables();
			$data['section'] = $this->load->view('/administrators/user', $data, true); 
			
			$this->load->view('/template/index', $data);
		}

		function new_administrator(){	
			if ($this->is_valid_email()) {
				$data = $this->input->post();
				$indicate_files = array();
				if(isset($_FILES) && !empty($_FILES)){
					foreach ($_FILES as $file) {
						foreach ($file['name'] as $key => $value) {
							$field_name = $key;
							$file_name = date("YmdHis").$value;
							$data[$field_name] = $file_name;
							array_push($indicate_files, $field_name);
						}
					}
					$this->upload_file($indicate_files, 'files');
				}

				$this->administrator_model->new_administrator( $data );
			}else{
        		return_json(array('error' => true, 'msg' => $this->lang->line('email_registered')));
				
		//$callback = $this->input->get('callback');
		//echo $callback . '('.json_encode(array('error' => true, 'msg' => $this->lang->line('email_registered'))).')';
			}
		}

		function edit_administrator(){
			$email_in_db = $this->administrator_model->get_email_administrator( $this->input->post('id') );
			if ($email_in_db == $this->input->post('email')) {
				$data = $this->input->post();
				$indicate_files = array();
				if(isset($_FILES) && !empty($_FILES)){
					foreach ($_FILES as $file) {
						foreach ($file['name'] as $key => $value) {
							$field_name = $key;
							$file_name = date("YmdHis").$value;
							$data[$field_name] = $file_name;
							array_push($indicate_files, $field_name);
						}
					}
					$this->upload_file($indicate_files, 'files');
				}
 
				$this->administrator_model->edit_administrator( $data );
				return_json(array('error' => false, 'msg' => $this->lang->line('information_updated')));
		//				$callback = $this->input->get('callback');
	//echo $callback . '('.json_encode(array('error' => false, 'msg' => $this->lang->line('information_updated'))).')';
			}else{
				if ($this->is_valid_email()) {
					$this->administrator_model->edit_administrator( $this->input->post() );
					return_json(array('error' => false, 'msg' => $this->lang->line('information_updated')));
					
						//$callback = $this->input->get('callback');
		//echo $callback . '('.json_encode(array('error' => false, 'msg' => $this->lang->line('information_updated'))).')';
				}else{
								//$callback = $this->input->get('callback');			
	//	echo $callback . '('.json_encode(array('error' => true, 'msg' => $this->lang->line('email_registered'))).')';
					return_json(array('error' => true, 'msg' => $this->lang->line('email_registered')));
				}
			}			
		}

		function is_valid_email(){
			$return = $this->administrator_model->verify_email( $this->input->post('email') );
			if($return)
				return false;

			return true;
		}

		function delete(){
			$user_id = $this->uri->segment(3, 0);
			$this->administrator_model->delete_administrator($user_id);
			$this->activity_model->delete_activity($user_id);

			redirect('administrators', 'refresh');
		}

		function delete_administrators(){
			$users = $this->input->post('users');
			for ($i=0; $i < count($users); $i++) { 
				$this->administrator_model->delete_administrator($users[$i]);
				$this->activity_model->delete_activity($users[$i]);
			}
		}

		function upload_file($indicate_files, $folder){
			$this->load->library('upload');
		    $files = $_FILES;
		    $count = count($_FILES['file']['name']);
		    for($i=0; $i<$count; $i++){          
		        $_FILES['file']['name'] = date("YmdHis").clear($files['file']['name'][ $indicate_files[$i] ]);
		        $_FILES['file']['type'] = $files['file']['type'][ $indicate_files[$i] ];
		        $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][ $indicate_files[$i] ];
		        $_FILES['file']['error'] = $files['file']['error'][ $indicate_files[$i] ];
		        $_FILES['file']['size'] = $files['file']['size'][ $indicate_files[$i] ];    

		        $this->upload->initialize($this->set_upload_options($folder));
		        if(!$this->upload->do_upload('file')){
		        	print_r($this->upload->display_errors());
		        	exit();
		        }

		    }
		}

		private function set_upload_options($folder){   
		    if (!file_exists('./uploads/'.$folder.'/')) 
			    mkdir('./uploads/'.$folder.'/', 0777, true);

		    $config['upload_path'] = './uploads/'.$folder.'/';
	        $config['allowed_types'] = '*';
	        $config['max_size'] = 10000; // Maximun size 10mb
	        $config['overwrite'] = true;

		    return $config;
		}
	}
?>