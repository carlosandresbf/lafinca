<?php 
class Configuration extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();
		only_super_administrator();

		$this->load->model('configuration_model');
	}

	function index(){
		$data['section_title'] = $this->lang->line('configuration');
		$data['configuration'] = $this->configuration_model->get_configuration(); 
		$data['section'] = $this->load->view('/configuration/index', $data, true); 
		
		$this->load->view('/template/index', $data);
	}

	function edit_configuration(){
		$data = $this->input->post();
		$indicate_files = array();
		if(isset($_FILES) && !empty($_FILES)){
			foreach ($_FILES as $file) {
				foreach ($file['name'] as $key => $value) {
					$field_name = $key;
					$file_name = date("YmdHis").$value;
					$data[$field_name] = $file_name;
					array_push($indicate_files, $field_name);
				}
			}
			$this->upload_file($indicate_files, 'files');
		}

		$this->configuration_model->edit_configuration($data);
						//		$callback = $this->input->get('callback');
		//echo $callback . '('.json_encode(array('msg' => $this->lang->line('configuration_updated'))).')';
		return_json(array('msg' => $this->lang->line('configuration_updated')));
	}

	function upload_file($indicate_files, $folder){
		$this->load->library('upload');
	    $files = $_FILES;
	    $count = count($_FILES['file']['name']);
	    for($i=0; $i<$count; $i++){          
	        $_FILES['file']['name'] = date("YmdHis").clear($files['file']['name'][ $indicate_files[$i] ]);
	        $_FILES['file']['type'] = $files['file']['type'][ $indicate_files[$i] ];
	        $_FILES['file']['tmp_name'] = $files['file']['tmp_name'][ $indicate_files[$i] ];
	        $_FILES['file']['error'] = $files['file']['error'][ $indicate_files[$i] ];
	        $_FILES['file']['size'] = $files['file']['size'][ $indicate_files[$i] ];    

	        $this->upload->initialize($this->set_upload_options($folder));
	        if(!$this->upload->do_upload('file')){
	        	print_r($this->upload->display_errors());
	        	exit();
	        }
 
	    }
	}
	
	private function set_upload_options($folder){   
	    if (!file_exists('./uploads/'.$folder.'/')) 
		    mkdir('./uploads/'.$folder.'/', 0777, true);

	    $config['upload_path'] = './uploads/'.$folder.'/';
        $config['allowed_types'] = '*';
        $config['max_size'] = 10000; // Maximun size 10mb
        $config['overwrite'] = true;

	    return $config;
	}
}
?>