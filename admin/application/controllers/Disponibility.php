<?php 
class Disponibility extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();
		$this->property_id = $table = $this->uri->segment(3, 0);
		$this->load->model('disponibility_model');
		$this->load->model('administrable_table_model');
	}

	function show(){
		$data['property_id'] = $this->property_id;
		$data['finca_nombre']= $this->administrable_table_model->get_property_by_id($data['property_id']);
		$data['section_title'] = 'Disponibilidad';
		$data['section'] = $this->load->view('/administrable_tables/disponibility', $data, true); 
 
		$this->load->view('/template/index', $data);
	}

	function events_disponibility(){
		$response = array();
		$events = $this->disponibility_model->get_events_disponibility($this->property_id); 
		
		foreach ($events as $item) {
			$event = array();
		    $event['id'] = $item->id;
		    $event['title'] = $item->title;
		    $event['start'] = $item->start_date;
		    $event['end'] = $item->end_date;

		    $allday = ($item->all_day == "true") ? true : false;
		    $event['allDay'] = $allday;

		    array_push($response, $event);
		}
		
		echo json_encode($response);
	}

	function new_event_disponibility(){
		$data_ = $this->input->post();
		unset($data_['callback']);
		unset($data_['_']);
		//print_r($data_);
		$event = $this->disponibility_model->add_event_disponibility($data_, $this->property_id); 
		$callback = $this->input->get('callback');
		echo json_encode(array('status' => 'success', 'eventid' => $event));
	}

	function update_event_disponibility(){
		$data_ = $this->input->get();
		unset($data_['callback']);
		unset($data_['_']);
		$this->disponibility_model->update_event_disponibility($data_, $this->property_id); 
		return_json(array('status' => 'success'));
			
		
	}

	function delete_event_disponibility(){
		$data_ = $this->input->get();
		unset($data_['callback']);
		unset($data_['_']);
		$this->disponibility_model->delete_event_disponibility($data_, $this->property_id); 
		echo json_encode(array('status' => 'success'));
	}

	function events_price(){
		$response = array();
		$events = $this->disponibility_model->get_events_price($this->property_id); 
		foreach ($events as $item) {
			$event = array();
		    $event['id'] = $item->id;
		    $event['title'] = $item->title;
		    $event['start'] = $item->start_date;
		    $event['end'] = $item->end_date;

		    $allday = ($item->all_day == "true") ? true : false;
		    $event['allDay'] = $allday;

		    array_push($response, $event);
		}
       return_json($response);
		
	}

	function new_event_price(){
		$data_ = $this->input->get();
		unset($data_['callback']);
		unset($data_['_']);
		$event = $this->disponibility_model->add_event_price($data_, $this->property_id); 
		return_json(array('status' => 'success', 'eventid' => $event));
		
	}

	function update_event_price(){
		$data_ = $this->input->get();
		unset($data_['callback']);
		unset($data_['_']);
		$this->disponibility_model->update_event_price($data_ , $this->property_id); 
		echo json_encode(array('status' => 'success'));
	}

	function delete_event_price(){
		$data_ = $this->input->get();
		unset($data_['callback']);
		unset($data_['_']);
		$this->disponibility_model->delete_event_price($data_, $this->property_id); 
		echo json_encode(array('status' => 'success'));
	}

}