<?php 
class Email_send extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();

		$this->load->model('administrator_model');
		$this->load->model('activity_model');		
        $this->load->library('pdf_generator');		
        $this->load->library('email_sender');  
		
        
		
	}

public	function Go(){

 	date_default_timezone_set('America/Bogota');
 	$table = 'reservas_de_clientes';
	$data['id'] = $this->input->post('dt1');
	$data['reservation'] = $this->administrable_table_model->get_record_table($table, $data['id']);
	$data['finca']= $this->administrable_table_model->get_record_table('fincas', $data['reservation']->fincas_relation);
	$pdf_name ='Confirmacion_Reserva_No_'.$data['id'].'_'.time().'.pdf';
	$data['agente']= $this->administrator_model->get_administrator($data['finca']->administrator_relation);
	$data['super_admins']= $this->administrator_model->get_super_administrators();

	$this->pdf_generator->build_pdf($data['id'], $pdf_name, 0);//1 - return json
	
	$data['mail'] = $this->load->view('/mail/booking_confirmation', $data, true); 
	$destino[0]['email']=$data['reservation']->email_text;
	$destino[0]['nombre']=$data['reservation']->nombre_text.' '.$data['reservation']->apellido_text;
	$destino[1]['email']=$data['agente']->email;
	$destino[1]['nombre']=$data['agente']->name;
	$cnt = 1;
	foreach($data['super_admins'] as $item){
	if(($data['agente']->email != $item->email) && !empty($item->email)){
		$cnt++;
	$destino[$cnt]['email']=$item->email;
	$destino[$cnt]['nombre']=$item->name;
											}
									}
	$result = $this->email_sender->send_email($destino, 'Confirmacion de Reserva - LaFinca.com.co', $data['mail'], '../admin/uploads/files/pdf_files/'.$pdf_name );
	if($result==true){
	$arr = array ('enviado'=>'Si');	
	$enviado['email_enviado_radio']='Si';
	 $this->administrable_table_model->save_table($table, $data['id'], $enviado );								
					}else{
	$arr = array ('enviado'=>'No');	
	}
	echo json_encode($arr);
	$this->pdf_generator->unset_pdf($pdf_name);
	//print_r($destino);
	 }	
	 
} 