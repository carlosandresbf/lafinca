<?php 
class Excel_gen extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();

		$this->load->model('activity_model');
		$this->load->model('excel_reports_model');
		$this->load->library('excel');
		
	}

public	function report_visitors(){ 
$desde = $this->uri->segment(3, 0);
$hasta = $this->uri->segment(4, 0);
	
$Fincas = $this->excel_reports_model->get_all_fincas($desde,$hasta);
//print_r($Fincas);
//exit();
$this->excel->setActiveSheetIndex(0);	
$this->excel->getActiveSheet()->setTitle('Reporte de visitas a fincas');	
//set cell A1 content with some text
$this->excel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);
$this->excel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);
$this->excel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);
$this->excel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);

$this->excel->getActiveSheet()->setCellValue('A1', 'REPORTE DE VISITAS A FINCAS DESDE: '.$desde.' - HASTA: '.$hasta.' ');
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(16);
$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
$this->excel->getActiveSheet()->mergeCells('A1:D1');
$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$this->excel->getActiveSheet()->setCellValue('A2', 'NOMBRE FINCA');
$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setSize(14);
$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


$this->excel->getActiveSheet()->setCellValue('B2', 'CODIGO');
$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setSize(14);
$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$this->excel->getActiveSheet()->setCellValue('C2', 'DEPARTAMENTO');
$this->excel->getActiveSheet()->getStyle('C2')->getFont()->setSize(14);
$this->excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('C2')->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

$this->excel->getActiveSheet()->setCellValue('D2', 'VISITAS');
$this->excel->getActiveSheet()->getStyle('D2')->getFont()->setSize(14);
$this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
$this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$a=3;
$counter_visitas = 0;
foreach($Fincas as $foreached)
{
$this->excel->getActiveSheet()->setCellValue('A'.$a, $foreached->nombre);
$this->excel->getActiveSheet()->setCellValue('B'.$a, $foreached->codigo);
$this->excel->getActiveSheet()->setCellValue('C'.$a, $foreached->departamento);
$this->excel->getActiveSheet()->setCellValue('D'.$a, $foreached->visitas);
	$counter_visitas+= $foreached->visitas;
	$a++;
	}


$this->excel->getActiveSheet()->setCellValue('A'.$a, 'TOTAL DE FINCAS: '.($a-3).' | TOTAL VISITAS: '.$counter_visitas );
$this->excel->getActiveSheet()->getStyle('A'.$a)->getFont()->setSize(16);
$this->excel->getActiveSheet()->getStyle('A'.$a)->getFont()->setBold(true);
$this->excel->getActiveSheet()->mergeCells('A'.$a.':D'.$a);
$this->excel->getActiveSheet()->getStyle('A'.$a)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$this->excel->getActiveSheet()->getStyle('A'.$a)->getAlignment()->setvertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
 
$filename='Reporte de fincas desde '.$desde.' - hasta: '.$hasta.'-'.date('H_m_i',time()).'.xls'; 
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$filename.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
$objWriter->save('php://output');
		
	 }	
	
}