<?php 
class Pdf_gen extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();
		$this->load->model('activity_model');
		$this->load->model('administrable_table_model');
        $this->load->library('Pdf');
        $this->load->library('pdf_generator'); 
		
	}

	function build_pdf(){
		$data['id'] = $this->input->post('dt1');
		$data['callback'] = $this->input->get('callback');
		$pdf_name = 'Confirmacion_Reserva_No_'.$data['id'].'_'.time().'.pdf';
		$this->pdf_generator->build_pdf($data['id'], $pdf_name, 1, $data['callback']);//1 - return json
		}
								
	function unset_pdf(){
		
	$data['file_name'] = $this->input->post('dt1');
	$data['callback'] = $this->input->get('callback');
	unlink('../admin/uploads/files/pdf_files/'.$data['file_name']);
	
		}
	
}