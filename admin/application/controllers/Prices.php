<?php 
class Prices extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();
		//$this->property_id = $table = $this->uri->segment(3, 0);
		$this->property_id ='';
		$this->load->model('disponibility_model');
		$this->load->model('Administrable_table_model');
		$this->properties = $this->Administrable_table_model->get_records_table('fincas');
		
	}

	function show(){
		$data['property_id'] = $this->property_id;
		$data['section_title'] = 'Precios';
		$data['section'] = $this->load->view('/administrable_tables/prices', $data, true); 

		$this->load->view('/template/index', $data);
	}

	function events_price(){
		$response = array();
		$events = $this->disponibility_model->get_events_price($this->property_id); 
		foreach ($events as $item) {
			$event = array();
		    $event['id'] = $item->id;
		    $event['title'] = $item->title;
		    $event['start'] = $item->start_date;
		    $event['end'] = $item->end_date;

		    $allday = ($item->all_day == "true") ? true : false;
		    $event['allDay'] = $allday;

		    array_push($response, $event);
		}
        return_json($response);
	}

	function new_event_price(){
		foreach($this->properties as $item){
		$event = $this->disponibility_model->add_event_price($this->input->post(), $item->id); 	
		}
		return_json(array('status' => 'success', 'eventid' => $event));
	
	}

	function update_event_price(){
		foreach($this->properties as $item){
		$this->disponibility_model->update_event_price($this->input->post(), $item->id); 
		}
		return_json(array('status' => 'success'));
	}

	function delete_event_price(){
		foreach($this->properties as $item){
		$this->disponibility_model->delete_event_price($this->input->post(), $item->id); 
		}
		return_json(array('status' => 'success'));
	}

}
?>