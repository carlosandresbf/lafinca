<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
class pdf_generator {
    
	public function	build_pdf($id=0, $pdf_name='', $type = 1, $callback){
	 $CI = & get_instance();
	 $CI->load->model('administrable_table_model');
     $CI->load->library('Pdf');
			
		
 	date_default_timezone_set('America/Bogota');
	$data['id'] = $id;

 	$table = 'reservas_de_clientes';
	$reservation = $CI->administrable_table_model->get_record_table($table, $data['id']);
	$finc = $CI->administrable_table_model->get_record_table('fincas', $reservation->fincas_relation);

	
	$pdf = new FPDF('P', 'pt', array(1240, 1619)); 
	$pdf->AddPage(); 
	if($reservation->valor_saldo_restante_number == 0){
	$pdf->Image('../admin/uploads/files/pdf_fotmat_finca_100.jpg', 0, 0, 1240, 1619); 	
	}else{
	$pdf->Image('../admin/uploads/files/pdf_fotmat_finca.jpg', 0, 0, 1240, 1619); 		
		}
	$pdf->SetFont('Arial', '', 22); 	
    $pdf->SetTextColor(16,15,13);
	
	$pdf->Text(208, 275, utf8_decode($reservation->nombre_text)); 
	$pdf->Text(208, 307, utf8_decode($reservation->documento_de_identidad_text));
	$pdf->Text(208, 339, utf8_decode($reservation->direccion_text));  
	$pdf->Text(208, 371, utf8_decode($reservation->telefono_text));  
	$pdf->Text(208, 402, utf8_decode($reservation->email_text)); 
	
	
	$pdf->SetFont('Arial', 'B', 23);
	$pdf->Text(965, 297, date('d/m/Y',time()));  
	$pdf->Text(1069, 387, utf8_decode($reservation->numero_de_reserva_text));  //No Reserva
	
	$pdf->SetFont('Arial', '', 22);
	$pdf->Text(217, 540,  utf8_decode($finc->nombre_text)); 
	$pdf->Text(178, 571,  utf8_decode($reservation->llegada_datetime)); 
	$pdf->Text(530, 571,  utf8_decode($reservation->salida_datetime)); 
	$pdf->Text(311, 602,  $reservation->numero_de_noches_number); 
	$pdf->Text(337, 633,  $reservation->numero_de_personas_number); 
	$pdf->Text(749, 602,  "$".number_format($reservation->valor_por_persona_adicional_number,0,'','.'));
	$pdf->Text(250, 666,  "$".number_format($reservation->total_a_pagar_number,0,'','.')." + $".number_format($reservation->aseo_final_number,0,'','.')." ASEO FINAL");
	
	$pdf->SetTextColor(229,48,79);
	$pdf->SetFont('Arial', 'B', 12); 
	$pdf->Text(660, 666,   "*Deposito por ". utf8_decode("daños:")." $".number_format($reservation->deposito_por_daños_number,0,'','.'));
	
	$xpl =  explode(" ",$reservation->llegada_datetime);
	//$xpl_date = explode("/",$xpl[0]);
	//$str_time = $xpl_date[2].'-'.$xpl_date[1].'-'.$xpl_date[0];
	$fecha = new DateTime($reservation->llegada_datetime);//$str_time);
	$fecha->sub(new DateInterval('P1D'));
	
    $pdf->SetTextColor(255,255,255);
	$pdf->SetFont('Arial', 'B', 22);
	
	if($reservation->valor_saldo_restante_number != 0){
	$pdf->Text(959,  542,   "$".number_format($reservation->valor_abono_number,0,'','.')); 
	$pdf->Text(1064, 575,   utf8_decode($fecha->format('d/m/Y')));
		
	$pdf->Text(1024, 632,   "$".number_format($reservation->valor_saldo_restante_number,0,'','.')); 
	$pdf->Text(1070, 664,   utf8_decode($xpl[0])); 
	}else{
	$pdf->Text(1024, 547,   "$".number_format($reservation->valor_abono_number,0,'','.')); 
	$pdf->Text(1064, 574,   utf8_decode($fecha->format('d/m/Y')));	
		}
			
	$data['pdf_link'] = '/admin/uploads/files/pdf_files/'.$pdf_name;
	$pdf->Output('..'.$data['pdf_link'],'F');  									
										
	$arr = array ('LinkReserva'=>urlencode('https://www.lafinca.com.co'.$data['pdf_link']),'Nombre'=>$reservation->nombre_text,'Apellido'=>$reservation->apellido_text,'Documento'=>$reservation->documento_de_identidad_text,'Telefono'=>$reservation->telefono_text,'E_mail'=>$reservation->email_text);
	if($type==1){
		
	echo $callback . '('.json_encode($arr).')';
								
							}
								}
								
	function unset_pdf($filename){
		
	$data['file_name'] = $filename;
	unlink('../admin/uploads/files/pdf_files/'.$data['file_name']);
	
		}
	
    }
 