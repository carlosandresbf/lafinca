<?php
Class Disponibility_model extends CI_Model {

	function get_events_disponibility($property) {
		$this->db->select('*');
		$this->db->where('property_id', $property);
		$this->db->from('property_disponibility');

		$query = $this->db->get();
		return $query->result();
	}

	function add_event_disponibility($data, $property) {		
		$this->clear_superimposed_events_disponibility($data['start_date'], $data['end_date'], $property);
		
		$this->db->set('property_id', $property);
		$this->db->insert('property_disponibility', $data);
		//echo $this->db->last_query();
		return $this->db->insert_id();
	}

	function update_event_disponibility($data, $property){
		$event = $data['event_id'];
		unset($data['event_id']);

		$this->db->where('id', $event);
		$this->db->update('property_disponibility', $data);
	}

	function delete_event_disponibility($data, $property){
		$event = $data['event_id'];
		unset($data['event_id']);
		
		$this->db->where('id', $event);
		$this->db->delete('property_disponibility');
	}




	function get_events_price($property) {
		
		
		$this->db->select('id');
		$this->db->from('fincas');		
		$this->db->where('activo_radio', 'Si');		
		$this->db->limit(1);
		$query = $this->db->get();
		$finca =  $query->result();			

		
		$this->db->select('*');
		$this->db->where('property_id', $finca[0]->id);
	
		$this->db->from('property_price');

		$query = $this->db->get();
		$a =  $query->result();
		//echo $this->db->last_query();
		return $a;
	}

	function add_event_price($data, $property) {
		$this->clear_superimposed_events_prices($data['start_date'], $data['end_date'], $property);
		
		$this->db->set('property_id', $property);
		$this->db->insert('property_price', $data);
		return $this->db->insert_id();
	}

	function update_event_price($data, $property){
		$event = $data['event_id'];
		unset($data['event_id']);

		$this->db->where('id', $event);
		$this->db->update('property_price', $data);
	}

	function delete_event_price($data, $property){
		$event = $data['event_id'];
		unset($data['event_id']);
		
		$this->db->where('id', $event);
		$this->db->delete('property_price');
	}
	
	function clear_superimposed_events_prices($start, $end, $property)
	{
	
		$this->db->query("DELETE FROM property_price WHERE  (
		( DATE('".$start."') = start_date  AND  DATE('".$end."') = end_date )   OR 
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= start_date ) OR 
		( DATE('".$start."') <= end_date    AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') >= start_date  AND  DATE('".$end."') <= end_date )
															 ) AND property_id = '".$property."';");


				}	
	function clear_superimposed_events_disponibility($start, $end, $property)
	{
	
		$this->db->query("DELETE FROM property_disponibility WHERE  (
		( DATE('".$start."') = start_date  AND  DATE('".$end."') = end_date )   OR 
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= start_date ) OR 
		( DATE('".$start."') <= end_date    AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') >= start_date  AND  DATE('".$end."') <= end_date )
															 ) AND property_id = '".$property."';");


				}
				
	function clear_event_disponibility($start, $end, $property)
	{
		
	
		$this->db->query("DELETE FROM property_disponibility WHERE ( DATE('".$start."') = start_date  AND  DATE('".$end."') = end_date ) AND property_id = '".$property."';");
		


				}

}