<?php
Class Excel_reports_model extends CI_Model {

	function get_all_fincas($desde, $hasta){
		$array = array('vis.fecha_date >=' => $desde, 'vis.fecha_date <=' => $hasta);
		($this->session->userdata('logged_in')['is_super_administrator'] != 1)? $array['administrator_relation']= $this->session->userdata('logged_in')['user_id'] : '';
		$this->db->select('count(vis.id) AS visitas, finca.`nombre_text` AS nombre, finca.`identificador_text` AS codigo, depto.`nombre_text` AS departamento');
		$this->db->from('`fincas` AS finca');
		$this->db->join('fincas_visitas_fecha AS vis', 'finca.id = vis.`id_finca_relation`');
		$this->db->join('departamentos AS depto', 'depto.id = finca.`departamentos_relation`');
		$this->db->where($array);
		$this->db->group_by('finca.`nombre_text`'); 
		$this->db->order_by("visitas", "desc");

		$query = $this->db->get();
		return $query->result();
	}

}