<div class="content">
   
    <?=$custom_filter?>
        <div class="row">                   
            <div class="col-md-12">
                <div class="card table-content">
                    <div class="content table-responsive table-full-width">
                        <button type="button" class="btn btn-action btn-default remove-all-data" id="remove-all-administrable-tables" data-table="<?= $table ?>"><?= $this->lang->line('delete_all_selected') ?></button>
                        <div class="scroll-h" style="overflow-y: scroll;">
                            <table style="width:100%" class="table table-hover table-striped  data-table <?php if($table == 'fincas'){	echo 'custom-data-table';}?> administrable-data-table" data-table="<?= $table ?>">
                                <thead>
                                    <th width="50px"></th>
                                    <th width="200px"></th>
                                    <?php 
									if($table == 'fincas'){										
									array_push($fields, array('complete_name' => 'visitas_text', 'name' => 'Numero Visitas', 'type' => 'text', 'configuration' => '', 'options' => ''));	
										}
									
									foreach ($fields as $field) { 
									if($field['complete_name']=='numero_de_visitas_text' && $table == 'fincas'){ continue;}
									?>
                                         <th><?= $field['name'] ?></th>
                                    <?php } ?>
                                </thead>
                                <tfoot>
                                    <th width="50px"></th>
                                    <th width="200px"></th>
                                    <?php foreach ($fields as $field) { 
									if($field['complete_name']=='numero_de_visitas_text' && $table == 'fincas'){ continue;}
									?>
                                        <th></th>
                                    <?php } ?>
                                </tfoot>
                                <tbody>
                                <?php $order=1; foreach ($records as $record) { ?>
                                    <tr id="<?= $record->id ?>" data-table="<?= $table ?>">
                                        <td width="50px"><?= $order ?></td>
                                        <td width="200px">
                                            <a data-toggle="tooltip" title="<?= $this->lang->line('edit') ?>" href="<?= base_url() ?>administrable_tables/edit/<?= $table ?>?record=<?= $record->id ?>"><button type="button" class="btn btn-warning btn-info"><i class="fa fa-pencil" aria-hidden="true"></i></button></a>

                                            <?php if($table == 'fincas'){ ?>
                                            <a data-toggle="tooltip" title="Disponibilidad" href="<?= base_url() ?>disponibility/show/<?= $record->id ?>"><button type="button" class="btn btn-warning btn-info"><i class="fa fa-calendar" aria-hidden="true"></i></button></a>
                                            <?php } ?>
                                            
                                            <?php if($table == 'reservas_de_clientes'){ ?>
                                            <a data-toggle="modal" title="Enviar Reserva a Email" data-record="<?= $record->id ?>" class="email-send" data-target="#myModal" href="#"><button type="button" class="btn btn-warning btn-info"><i class="fa fa-envelope" aria-hidden="true"></i></button></a>
                                            <?php } ?>

                                            <a data-toggle="tooltip" title="<?= $this->lang->line('delete') ?>" href="<?= base_url() ?>administrable_tables/delete/<?= $table ?>?record=<?= $record->id ?>" onclick="return confirm('<?= $this->lang->line('are_you_sure') ?>')"><button type="button" class="btn btn-action btn-default"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                                        </td>
                                        <?php foreach ($fields as $field) { 
										if($field['complete_name']=='numero_de_visitas_text' && $table == 'fincas'){ continue;}
										?>
                                            <?php if($field['type'] == 'relation'){ ?>
                                                <?php $exits = false; ?>
                                                <?php $j=0; foreach ($field['options'] as $option) { ?>
                                                    <?php if($option['id'] == $record->$field['complete_name']){
                                                        $exits = true;
                                                        $position = $j;
                                                    } ?>
                                                <?php $j++; } ?>
                                                <?php if($exits){ ?>
                                                    <th><?= $field['options'][$position]['name'] ?></th>
                                                <?php }else{ ?>
                                                    <th></th>
                                                <?php } ?>
                                            <?php }else if($field['type'] == 'color'){ ?>
                                                <th>
                                                    <span class="color-block" style="background:<?= $record->$field['complete_name']; ?>"></span>
                                                    <?= $record->$field['complete_name'] ?>
                                                </th>
                                            <?php }else if($field['type'] == 'number' && $table == 'reservas_de_clientes'){ ?>
                                                         <th style="text-align:center"><?= number_format($record->$field['complete_name'],0,'',' '); ?></th>
                                            <?php }else if($field['type'] == 'file'){ ?>
                                                <th>
                                                    <?php $file = explode('.', $record->$field['complete_name']); ?>
                                                    <?php $extension = $file[ count($file)-1 ]; ?>
                                                    <?php if($extension == "jpg" || $extension == "jpeg" || $extension == "png" || $extension == "gif"){ ?>
                                                        <img data-action="zoom" src="<?= base_url() ?>uploads/files/<?= $record->$field['complete_name'] ?>">
                                                    <?php }else{ ?>
                                                        <?php if(!empty($file[0])){ ?>
                                                            <a target="_blank" href="<?= base_url() ?>uploads/files/<?= $record->$field['complete_name'] ?>"><?= $this->lang->line('file') ?></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </th>
                                            <?php }else{ ?>
                                                <th <?php if($field['complete_name']=='email_enviado_radio'){?>id="fl<?= $record->id ;}?>"><?= $record->$field['complete_name'] ?></th>
                                            <?php } ?>
                                        <?php } ?>
                                    </tr>
                                <?php $order++; } ?>
                                </tbody>
                            </table> 
                        </div>
                    </div>
                </div>
            </div>        
        </div>                    
    </div>
</div>
<?php if($table == 'reservas_de_clientes'){ ?>
<input type="hidden" name="pdf_link_to_unset" id="pdf_link_to_unset"  value=""/>
<script>
$(document).ready(function(){
	
	$.support.cors = true;
	
setTimeout(	function(){
	$("th[aria-label='Fincas']").html('');
$("th[aria-label='Fincas']").html('Referencia Finca');	}, 1000);
	


		
$("#myModal").on("hidden.bs.modal", function () {
	$.ajax({
  method: "POST",
  url: "<?php echo base_url()?>pdf_gen/unset_pdf/",
  dataType: "jsonp",
  cache: false,
  data: { dt1: $("#pdf_link_to_unset").val() },
    error: function(jqXHR, textStatus, errorThrown){
 console.log(jqXHR);
  console.log(textStatus);
   console.log(errorThrown);
      }  
	});
				});
	
	
$(".email-send").on("click",function(){
	
		$("#myModalLabel").empty();	
		$("#myModalLabel").append("<strong>Confirmación de envío de reserva</strong>");	
		$("#myModalBody").empty();		
		$("#myModalFooter").empty();	
		
		var record_id = $(this).attr('data-record');
		
<?php /*?>jQuery.support.cors = true;<?php */?>

	$.ajax({
  method: "POST",
  url: "<?php echo base_url()?>pdf_gen/build_pdf/",
  dataType: "jsonp",
  crossDomain: true, 
  cache: false,
  data: { dt1: record_id},
    success: function (data) {
			console.log('here im2');
		var Mylink = decodeURIComponent(data.LinkReserva);
		$("#myModalBody").append('<p class="custom_padding"></p>');		
		$("#myModalBody").append('<style>.big{TEXT-TRANSFORM: UPPERCASE;}.custom_padding{padding-left: 23px;}</style>');
		$(".custom_padding").append("<strong class='big'>Se enviará la siguiente Confirmación de Reserva:</strong><br>");	
		$(".custom_padding").append('<strong class="big">pdf:</strong> <a href="'+Mylink+'" target="_blank"> Clic aquí</a><br>');	
		$(".custom_padding").append('<strong class="big">Nombre:</strong> '+data.Nombre+'<br>');		
		$(".custom_padding").append('<strong class="big">Apellido:</strong> '+data.Apellido+'<br>');	
		$(".custom_padding").append('<strong class="big">Documento:</strong> '+data.Documento+'<br>');	
		$(".custom_padding").append('<strong class="big">Telefono:</strong> '+data.Telefono+'<br>');
		$(".custom_padding").append('<strong class="big">E-mail:</strong> '+data.E_mail+'<br>');	
		$("#myModalFooter").html('<button type="button" class="btn btn-secondary btn-fill" id="Cancel_send" data-dismiss="modal">Cancelar</button><button data-record="'+record_id+'" type="button" id="Send_my_mail" class="btn btn-warning btn-fill">Enviar E-Mail</button>');
		
		var script = document.createElement( 'script' );
			$("#myModalFooter").append( script ); 
			
		$("#myModalFooter script").append('$("#Send_my_mail").on("click",function(){	if(confirm(\'Esta seguro de enviar la Confirmación de Reserva?\')){ $("#Send_my_mail").hide();	$("#Cancel_send").text(\'Enviando...\');$("#Cancel_send").attr(\'data-dismiss\',\'\');	 $.ajax({  method: "POST",  url: "<?php echo base_url()?>email_send/Go/",  dataType: "json",  cache: false,  data: { dt1: $(this).attr(\'data-record\') },    success: function (data) { $("#Cancel_send").text(\'Mensaje enviado con éxito. (Cerrar)\'); $("#Cancel_send").removeClass(\'btn-secondary\'); $("#Cancel_send").addClass(\'btn-warning\'); $("#Cancel_send").attr(\'data-dismiss\',\'modal\'); $("#fl'+record_id+'").text("Si");	    }	});			}	});');
		
		Mylink = Mylink.split('/');
		$("#pdf_link_to_unset").val(Mylink[7]);
    						},
    error: function(jqXHR, textStatus, errorThrown){
 console.log(jqXHR);
  console.log(textStatus);
   console.log(errorThrown);
      }  
	});
				
												});
	});
//<?= base_url() ?>Email_send/go/<?= $record->id ?>
</script>



<?php }?>