<div class="content">
    <div class="container-fluid">    
       
        <div class="row">
            <div class="col-md-12">
                <h4>Precio</h4>
                <p><small><em>Este calendario es el usado para establecer los precios de cada uno de los días de alquiler de tu finca. <br>

                Para establecer el precio de temporada ALTA selecciona los días arrastrando el puntero por el calendario, una vez seleccionados los días en la ventana emergente se despliega escribe la palabra “alta” para que aplique el costo que previamente ingresaste en “precio de temporada alta” al momento de crear tu finca, de la misma forma funciona para temporada MEDIA con la diferencia que aquí en la ventana emergente debes escribir “media”; para el resto de los días del calendario aplica el costo de temporada baja.</em></small></p>
                <div class="card" style="overflow: hidden;">
                    <div id="calendar_price"></div>
                </div>
            </div>
        </div>    
    </div>    
</div>

<script>
$(document).ready(function(){


    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
   
    var calendar_price = $('#calendar_price').fullCalendar({
            monthNames: ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
            monthNamesShort: ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'],
            dayNames: ['Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado'],
            dayNamesShort: ['Dom','Lun','Mar','Mié','Jue','Vie','Sáb'],
            buttonText:{ today:"Hoy",month:"Mes",week:"Semana",day:"Día"},
            events: $('#base_url').val()+'prices/events_price/<?= $property_id ?>',
            editable: true,
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,agendaWeek,agendaDay',
            },
        eventRender: function(event, element, view) {
            if (event.allDay === 'true')
                event.allDay = true;
            else
                event.allDay = false;
        },
        selectable: true,
        selectHelper: true,
        select: function(start, end, allDay) {
            var title = prompt('');
            if (title) {
                var start = $.fullCalendar.formatDate(start, "yyyy-MM-dd HH:mm:ss");
                var end = $.fullCalendar.formatDate(end, "yyyy-MM-dd HH:mm:ss");
                $.ajax({
                    url: $('#base_url').val()+'prices/new_event_price/<?= $property_id ?>',
                    data: {title: title, start_date: start, end_date: end},
                    type: "POST"
                }).done(function(response) {
                    calendar_price.fullCalendar('renderEvent',{
                        id: response.eventid,
                        title: title,
                        start: start,
                        end: end,
                        allDay: allDay
                    },true);
                });
                location.reload();
            }
            calendar_price.fullCalendar('unselect');
        },

        editable: true,
        eventDrop: function(event, delta) {
            var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
            $.ajax({
                url: $('#base_url').val()+'prices/update_event_price/<?= $property_id ?>',
                data: {title: event.title, start_date: start, end_date: end, event_id: event.id},
                type: "POST"
            });
        },
        eventClick: function(event) {
            if (confirm($('#are_you_sure').val())) {
                $.ajax({
                    type: "POST",
                    url: $('#base_url').val()+'prices/delete_event_price/<?= $property_id ?>',
                    data: {event_id: event.id},
                    success: function(json) {
                        $('#calendar_price').fullCalendar('removeEvents', event.id);
                    }
                });
            }
        },
        eventResize: function(event) {
            var start = $.fullCalendar.formatDate(event.start, "yyyy-MM-dd HH:mm:ss");
            var end = $.fullCalendar.formatDate(event.end, "yyyy-MM-dd HH:mm:ss");
            $.ajax({
                url: $('#base_url').val()+'prices/update_event_price/<?= $property_id ?>',
                data: {start_date: start, end_date: end, event_id: event.id},
                type: "POST"
            });
        }
    });
});
</script>