 <div class="container-fluid">
       <div class="card table-content" style="padding:10px !important">
       
          <div class="row" style="margin-bottom:8px !important;"> <!--Row Report Excel-->                  
              <div class="col-md-2">
              <label>GENERAR REPORTE DE VISITAS:</label>                
             </div>
           
             <div class="col-md-2">
                <div class="input-group date">
                  <input type="text" class="save-input form-control" name="Desde"  id="desde_tm" placeholder="Desde" value="" >
                         <span class="input-group-addon"> 
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                </div>
             </div>
                       
             <div class="col-md-2">
                 <div class="input-group date">      
                   <input type="text" class="save-input form-control" name="Hasta" id="hasta_tm" placeholder="Hasta" value="" >
                        <span class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </span>
                 </div>
             </div>
             
             <div class="col-md-1">
               <button type="button" id="Report_generator" class="btn btn-warning btn-info"><i class="fa fa-file-excel-o" aria-hidden="true"></i> Descargar</button>
             </div>
             
             <div class="col-md-3"></div>
                 
            </div><!--Row Report Excel-->
            
<?php if ($this->session->userdata('logged_in')['is_super_administrator'] == 1){ ?>    
            <div class="row"> <!--Row Calendar-->                  
              <div class="col-md-2">
              <label>DEFINIR DISPONIBILIDAD/PRECIO:</label>                
             </div>
           
             <div class="col-md-2">
                <a data-toggle="tooltip" title="" href="https://www.lafinca.com.co/admin/prices/show/all" data-original-title="Disponibilidad">
                	<button type="button" class="btn btn-warning btn-info">
                    	<i class="fa fa-calendar" aria-hidden="true"></i> CONFIGURAR
                    </button>
                </a>
             </div>
             <div class="col-md-8"></div>
            </div><!--Row Calendar-->
<?php }?>           
            
        </div>
 </div><!--Container-->
    
    <script>
	$(document).ready(function(e) {
       
		$("#desde_tm").datetimepicker({
			format: "YYYY-MM-DD"
		});	
		$("#hasta_tm").datetimepicker({
			format: "YYYY-MM-DD"
		});
		
		$("#Report_generator").on("click",function(){
			if($("#desde_tm").val() != '' && $("#hasta_tm").val() != ''){
				window.open('<?=base_url()?>Excel_gen/report_visitors/'+$("#desde_tm").val()+'/'+$("#hasta_tm").val()+'/');
				}else{
				alert('Ingresa un rango de fechas');
				}
			});
	});
</script>