<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><?= $this->lang->line('edit_configuration') ?></h4>
                    </div>
                    <div class="content">
                        <form onkeypress="return event.keyCode != 13;">
                        
                        
                        
                        
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="field">Logo
                                            <?php if(isset($configuration) && !empty($configuration->picture)){ ?>
                                                <small><strong>&nbsp;&nbsp;<a href="<?= base_url() ?>uploads/files/<?= isset($configuration)?$configuration->picture:"" ?>" target="_blank"><i class="pe-7s-file"></i> <?= $this->lang->line('current_file') ?></a></strong></small>
                                            <?php } ?>
                                            
                                        </label>
                                        <br>
                                        <input id="picture" class="path-file form-control" placeholder="<?= $this->lang->line('picture') ?>" disabled="disabled" />
                                        <button class="btn btn-default btn-sm clear-file" rel="tooltip" data-original-title="Delete file" data-clear="picture"><i class="pe-7s-close"></i></button>
                                        <div class="fileUpload btn btn-default btn-fill">
                                            <span><?= $this->lang->line('select_a_file') ?></span>
                                            <input id="upload-picture" type="file" data-type="file" class="upload" name="picture" />
                                        </div>
                                    </div>
                                    <script>
                                        $("#upload-picture").on('change', function(){
                                            $("#picture").val( $(this).val() );
                                        })

                                        $('.clear-file').click(function(e){
                                            e.preventDefault();
                                            var name = $(this).attr('data-clear'),
                                                input = $('#upload-'+name)
                                            input.replaceWith(input.val('').clone(true));
                                            $('#'+name).val('');
                                        })
                                    </script>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('google_analytics_code') ?> <small><a href="httpss://www.google.com/analytics/" target="_blank"><?= $this->lang->line('more_info') ?></a></small></label>
                                        <input type="text" class="save-input form-control" name="google_analytics_code" placeholder="<?= $this->lang->line('google_analytics_code') ?>" value="<?= $configuration->google_analytics_code ?>">
                                    </div>        
                                </div>
                    
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('website_description') ?> <small><a href="httpss://moz.com/learn/seo/meta-description" target="_blank"><?= $this->lang->line('more_info') ?></a></small></label>
                                        <input type="text" class="save-input form-control" name="website_description" placeholder="<?= $this->lang->line('website_description') ?>" value="<?= $configuration->website_description ?>">
                                    </div>        
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label><?= $this->lang->line('website_keywords') ?> <small><a href="https://www.wordstream.com/meta-keyword" target="_blank"><?= $this->lang->line('more_info') ?></a></small></label><br>
                                        <input type="text" class="save-input form-control" data-role="tagsinput" name="website_keywords" placeholder="<?= $this->lang->line('website_keywords') ?>" value="<?= $configuration->website_keywords ?>">
                                    </div>        
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Facebook</label>
                                        <input type="text" class="save-input form-control" name="facebook" placeholder="Facebook" value="<?= $configuration->facebook ?>">
                                    </div>        
                                </div>
                    
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Twitter</label>
                                        <input type="text" class="save-input form-control" name="twitter" placeholder="twitter" value="<?= $configuration->twitter ?>">
                                    </div>        
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Google+</label>
                                        <input type="text" class="save-input form-control" name="google" placeholder="google" value="<?= $configuration->google ?>">
                                    </div>        
                                </div>
                            </div>



<div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Blog</label>
                                        <input type="text" class="save-input form-control" name="blog" placeholder="blog" value="<?= $configuration->blog ?>">
                                    </div>        
                                </div>
                    
                              
                            </div>
                            
                            
                            
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Copy</label><br>
                                        <input type="text" class="save-input form-control" name="copy" placeholder="Copy" value="<?= $configuration->copy ?>">
                                    </div>        
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Número de fincas destacada en el home</label><br>
                                        <input type="text" class="save-input form-control" name="num_properties_in_the_home" placeholder="Número de fincas destacada en el home" value="<?= $configuration->num_properties_in_the_home ?>">
                                    </div>        
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Emails para los reportes de errores</label><br>
                                        <input type="text" class="save-input form-control" data-role="tagsinput" name="error_reports_emails" placeholder="Emails para los reportes de errores" value="<?= $configuration->error_reports_emails ?>">
                                    </div>        
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Emails para los mensajes de contacto</label><br>
                                        <input type="text" class="save-input form-control" data-role="tagsinput" name="contact_emails" placeholder="Emails para los mensajes de contacto" value="<?= $configuration->contact_emails ?>">
                                    </div>        
                                </div>
                            </div>



                            <button type="submit" class="update-configuration btn btn-warning btn-fill pull-right"><?= $this->lang->line('update') ?></button>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>                    
    </div>
</div> 