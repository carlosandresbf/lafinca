$(document).ready(function(){
	$('.update-configuration').click(function(e){
		e.preventDefault();
		var data = new FormData();
		$('.save-input').each(function(){
			data.append($(this).attr('name'),$(this).val());
		})
		$.each($("input[type=file]"), function(i, obj) {
	        $.each(obj.files,function(j,file){
	            data.append('file['+$(obj).attr('name')+']', file);
	        })
		});

		$.each(data, function(key, value) {
		    if (Array.isArray(value))
		    	data[key] = value.join();
		});
		console.log('im');
		$.ajax({
		  	url: $('#base_url').val()+'configuration/edit_configuration',
		  	method: 'post',
		  	cache: false,
			contentType: false,
		    error: function(jqXHR, textStatus, errorThrown){
 console.log(jqXHR);
  console.log(textStatus);
   console.log(errorThrown);
      } ,
			dataType:"json",
			processData: false,
		  	data: data
		}).done(function(response) {
            $.notify({
                icon: "pe-7s-check",
                message: response.msg
            },{
                type: 'warning',
                timer: 4000,
                placement: {
                    from: 'bottom',
                    align: 'left'
                }
            });
		});
    })
})