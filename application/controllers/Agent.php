<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('page_model');
		$this->load->model('auth_model');
		$this->load->model('property_model');
		$this->load->model('configuration_model');
	}

	public function show(){
		$agent_id = $this->uri->segment(3);
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['properties'] = $this->property_model->get_property_by_agent($agent_id);
		$data['agent'] = $this->auth_model->get_agent($agent_id);
		$data['section'] = $this->load->view('agent', $data, true); 

		$this->load->view('template/main', $data);
	}
}
