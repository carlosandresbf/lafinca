<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comparative extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('page_model');
		$this->load->model('configuration_model');
		$this->load->model('comparative_model');
	}

	public function index(){
		$comparative = array();
		$session_comparative = $this->session->userdata('property_comparative');
		if (isset($session_comparative)){
			$count = count($this->session->userdata('property_comparative'));
			if ($count > 3) {
				$data_1 = $this->session->userdata('property_comparative')[$count-3];
				$data_2 = $this->session->userdata('property_comparative')[$count-2];
				$data_3 = $this->session->userdata('property_comparative')[$count-1];
				$comparative = array($data_1, $data_2, $data_3);
			}else{
				$comparative = $this->session->userdata('property_comparative');
			}
		} 
			
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['properties'] = $this->comparative_model->get_data($comparative);

		$data['section'] = $this->load->view('comparative', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function add_comparative(){
		$comparative = $this->session->userdata('property_comparative');
		if (empty($comparative))
			$comparative = array();

		array_push($comparative, $this->input->post('property'));

		$this->session->set_userdata('property_comparative', $comparative);
	}

	public function remove_comparative(){
		$comparative = $this->session->userdata('property_comparative');
		$comparative = array_diff( $comparative, [$this->input->post('property')] );

		$this->session->set_userdata('property_comparative', $comparative);
	}

}
