<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('page_model');
		$this->load->model('contact_model');
		$this->load->model('configuration_model');
	}

	public function index(){
		$data['pages'] = $this->page_model->get_pages();
		$data['config'] = $this->configuration_model->get_data();
		$data['banner'] = $this->contact_model->get_data();
		$data['section'] = $this->load->view('contact', $data, true); 

		$this->load->view('template/main', $data);
	}
}
