<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favorites extends CI_Controller {

	function __construct(){
		parent::__construct();
		authenticate();
		$this->load->model('page_model');
		$this->load->model('favorite_model');
		$this->load->model('configuration_model');
	}

	public function index(){
		$data['pages'] = $this->page_model->get_pages();
		$data['config'] = $this->configuration_model->get_data();
		$data['properties'] = $this->favorite_model->get_data($this->session->userdata('logged_front')['user_id']);
		
		$data['section'] = $this->load->view('favorites', $data, true); 

		$this->load->view('template/main', $data);
	}
}
