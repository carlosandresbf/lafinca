<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Featured extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('featured_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
	}

	public function show(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();

		$featured = str_replace('_', ' ', $this->uri->segment(3));
		$data['featured'] = $this->featured_model->get_featured($featured);

		$data['section'] = $this->load->view('featured', $data, true); 

		$this->load->view('template/main', $data);
	}
}
