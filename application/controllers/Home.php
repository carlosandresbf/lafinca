<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('us_model');
		$this->load->model('home_model');
		$this->load->model('department_model');
		$this->load->model('featured_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
		$this->load->model('property_model');
		$this->load->model('contact_model');
	}

	public function index(){
		$data['config'] = $this->configuration_model->get_data();
		$data['testimonials'] = $this->us_model->get_testimonials();
		$data['departments'] = $this->department_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['home'] = $this->home_model->get_data();
		$data['banner_images'] = get_images_gallery($data['home']->imagenes_gallery);
		$data['steps'] = $this->home_model->get_steps();
		$data['videos'] = $this->home_model->get_videos();
		$data['properties_featured'] = $this->property_model->get_featured();
		$data['featured'] = $this->featured_model->get_featured();
		$data['section'] = $this->load->view('home', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function error_report(){
		$config = $this->configuration_model->get_data();
		$this->home_model->add_report($this->input->post());
		$this->load->library('email', config_mail());
		$this->email->set_mailtype("html");
		$this->email->from('no-reply@lfinmobiliaria.com', 'Lafinca');
		$this->email->to($config->error_reports_emails); 
		$this->email->subject('Reporte de error - Lafinca.com.co');
		$this->email->message($this->load->view('mail/error_report', $this->input->post(),true));	
		$this->email->send();

	}

	public function contact(){
		$config = $this->configuration_model->get_data();
		$this->contact_model->add_contact($this->input->post());
		$this->load->library('email', config_mail());
		$this->email->set_mailtype("html");
		$this->email->from('no-reply@lfinmobiliaria.com', 'Lafinca');
		$this->email->to($config->contact_emails); 
		$this->email->to('reservas@lfinmobiliaria.com');//  
		$this->email->subject('Mensaje de contacto - Lafinca.com.co');
		$this->email->message($this->load->view('mail/contact', $this->input->post(),true));	
		$this->email->send();	

	}

	public function contact_agent(){
		$config = $this->configuration_model->get_data();
		$this->load->library('email', config_mail());
		$this->email->set_mailtype("html");
		$this->email->from('no-reply@lfinmobiliaria.com', 'Lafinca');
		$this->email->to($config->contact_emails, $this->input->post('agent_email'));
		$this->email->subject('Mensaje de contacto - Lafinca.com.co');
		$this->email->message($this->load->view('mail/contact_agent', $this->input->post(),true));	
		$this->email->send();
	}
}
