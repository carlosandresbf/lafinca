<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Newsletter extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('newsletter_model');
	}

	public function save(){
		$this->newsletter_model->save_email($this->input->post('email'));
	}
}
