<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class P404 extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('us_model');
		$this->load->model('home_model');
		$this->load->model('department_model');
		$this->load->model('featured_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
		$this->load->model('property_model');
		$this->load->model('contact_model');
	}

	public function index(){
		$data['config'] = $this->configuration_model->get_data();
		$data['testimonials'] = $this->us_model->get_testimonials();
		$data['departments'] = $this->department_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['home'] = $this->home_model->get_data();
		$data['banner_images'] = get_images_gallery($data['home']->imagenes_gallery);
		$data['steps'] = $this->home_model->get_steps();
		$data['videos'] = $this->home_model->get_videos();
		$data['properties_featured'] = $this->property_model->get_featured();
		$data['featured'] = $this->featured_model->get_featured();
		$data['section'] = $this->load->view('p404', $data, true); 

		$this->load->view('template/main', $data);
	}

}
