<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('property_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
		$this->load->model('department_model');
	}

	public function index(){
		redirect('/properties/search');
	}

	public function search(){
		$filters = array();
		$parameters = $this->input->get();

		if (isset($parameters['start'])) {
			$filters['start'] = $parameters['start'];
		}else{
			$filters['start'] = '';
		}

		if (isset($parameters['end'])) {
			$filters['end'] = $parameters['end'];
		}else{
			$filters['end'] = '';
		}

		if (isset($parameters['order'])) {
			$filters['order'] = $parameters['order'];
		}else{
			$filters['order'] = '';
		}

		if (isset($parameters['price'])) {
			$filters['price'] = $parameters['price'];
		}else{
			$filters['price'] = '';
		}

		if (isset($parameters['capacity'])) {
			$filters['capacity'] = $parameters['capacity'];
		}else{
			$filters['capacity'] = '';
		}
		if (isset($parameters['type'])) {
			$filters['type'] = $parameters['type'];
		}else{
			$filters['type'] = '';
		$parameters['type'] = '';
		}	
		if (isset($parameters['department'])) {
			$filters['department'] = $parameters['department'];
		}else{
			$filters['department'] = '';
			$parameters['department'] = '';
		}

		if (isset($parameters['services'])) {
			$filters['services'] = $parameters['services'];
		}else{
			$filters['services'] = '';
		}

		if (isset($parameters['name_id'])) {
			$filters['name_id'] = $parameters['name_id'];
		}else{
			$filters['name_id'] = '';
		}


		$max_p =  $this->property_model->get_properties_max_price($parameters['type'], $parameters['department'], $filters);
		$min_p =  $this->property_model->get_properties_min_price($parameters['type'], $parameters['department'], $filters);
		
		
		
		if(count($max_p)>0){
		$data['max_price'] =$max_p[0]->price;
		}else{$data['max_price']=0;}
		if(count($min_p)>0){
		$data['property_min_price'] =$min_p[0]->price;
		}else{$data['property_min_price']=0;}
		
		if(isset($filters['price']) & !empty($filters['price'])){ 
			$pricez = explode(';', $filters['price']);
			if(@!empty($pricez[1])){
			if($pricez[1]< $data['property_min_price'] ){$filters['price'] = $pricez[0].';'.$data['max_price'];}
			}
					}

		if(empty($filters['price'])){$filters['price']=$data['property_min_price'].';'.$data['max_price'];}

		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['config_search'] = $this->property_model->get_config_serch($parameters['type'], $parameters['department'], $filters);

		$this->load->library('pagination');
		$config['base_url'] = base_url().'properties/search/';
		$config['per_page'] = 12;
		$config['uri_segment'] = 3;
		
		$url = explode('?', $_SERVER["https_HOST"].$_SERVER["REQUEST_URI"]);
    	@$config['suffix'] = "?".$url[1];
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    	$config['cur_tag_open'] = '<li><span class="active">';
    	$config['cur_tag_close'] = '</span></li>';
    	$config['next_link'] = 'Siguiente';
	    $config['next_tag_open'] = '<div class="nav-next">';
	    $config['next_tag_close'] = '</div>';
	    $config['prev_link'] = 'Anterior';
	    $config['prev_tag_open'] = '<div class="nav-previous">';
	    $config['prev_tag_close'] = '</div>';

	    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

	    $data['properties'] = $this->property_model->get_properties_search($parameters['type'], $parameters['department'], $config['per_page'], $page, $filters);
		$count_all =  $this->property_model->get_properties_search_count($parameters['type'], $parameters['department'], $filters);

	    $config['total_rows'] = ($count_all)?count($count_all):0;
	    $choice = $config['total_rows'] / $config['per_page'];
    	$config['num_links'] = round($choice);

		$this->pagination->initialize($config);

        if (!empty($filters['start']) && !empty($filters['end'])) {
        	$filter_properties = array();
        	$start_date = date("Y-m-d ", strtotime( str_replace('/', '-', $filters['start']) ));
	        $end_date = date("Y-m-d", strtotime( str_replace('/', '-', $filters['end']) )) ;
	        
        	foreach ($data['properties'] as $property) {
	        	$available = true;
	        	$disponibility = $this->property_model->get_disponibility($property->id);
	        	foreach ($disponibility as $date) {
	        		$daterange1 = array($start_date, $end_date);
					$daterange2 = array($date->start_date, $date->end_date);

					$range_min = new DateTime(min($daterange1));
					$range_max = new DateTime(max($daterange1));

					$start = new DateTime(min($daterange2));
					$end = new DateTime(max($daterange2));

	        		if($start >= $range_min && $end <= $range_max){
					    $available = false;
					}
	        	}

	        	if($available){
					array_push($filter_properties, $property);
				}

	        	$data['properties'] = $filter_properties;
	        }
        }  

		
       // $data['max_price'] = ($data['config_search'])?(empty($data['config_search']->max_price)?100000000:$data['config_search']->max_price):100000000;
        $data['services'] = $this->property_model->get_services();
        $data['pagination'] = $this->pagination->create_links();
		$data['departments'] = $this->department_model->get_data();

		$data['section'] = $this->load->view('properties/search', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function show(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();

		$property = str_replace('_', ' ', $this->uri->segment(3));
		$data['property'] = $this->property_model->get_property_single($property);
		if (!$data['property']) {
			redirect('/');
		}
		$data['comments'] = $this->property_model->get_comments_by_property($data['property']->id);
		$data['disponibility'] = $this->property_model->get_disponibility($data['property']->id); 
		$data['total_views'] = $this->property_model->total_views($data['property']->id);

		$disponibility = array();
		foreach ($data['disponibility'] as $dates) {
			$new_dates = $this->create_date_range_array($dates->start_date, $dates->end_date);
			$disponibility = array_merge($disponibility, $new_dates);
		}

		unset($disponibility[count($disponibility)-1]);
		
		$data['disponibility'] = implode('", "', $disponibility);
		$data['same_properties'] = $this->property_model->get_same_properties($data['property']);

		$data['section'] = $this->load->view('properties/show', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function get_prices(){
		$data = $this->input->post();
		$dates_price = $this->property_model->get_prices($data);
		$property = $this->property_model->get_property_single($data['property_id'], 'id');
		$dates_db = array();
		
		if(!empty($data['to']) && !empty($data['from'])){
			
			$dates_search = $this->create_date_range_array(date("Y-m-d", strtotime($data['from'])), date("Y-m-d", strtotime($data['to'])));
			$price_number = 0;
			$num_check = 0;

			foreach ($dates_price as $date) {
				for ($i=0; $i < count($dates_search) ; $i++) { 
			
					$check = $this->check_in_range( date("Y-m-d", strtotime($date->start_date)), date("Y-m-d", strtotime($date->end_date)), $dates_search[$i]);
					
					if ($check) {
						if ($date->title == 'alta') {
						
							$price_number += (float) str_replace('.', '', $property->precio_temporada_alta_number);
						}else if($date->title == 'media'){
							
							$price_number += (float) str_replace('.', '', $property->precio_temporada_media_number);
						}else{
							
							
							$price_number += (float) str_replace('.', '', $property->precio_temporada_baja_number);
						}
					$num_check++;
					}else{
					
					/*echo 'baja  no che<br>';
							echo $property->precio_temporada_baja_number.' no  chequeo <br>';
						$price_number += (float) str_replace('.', '', $property->precio_temporada_baja_number);*/
					}

					
				}
			}
			
		
			if ($num_check == 0){
				$num_check = 1;
				$price_number += (float) str_replace('.', '', $property->precio_temporada_baja_number);
				}
			$price_number = $price_number/$num_check;
			$price = number_format($price_number, 0, ',', '.');

			$this->output
		        ->set_content_type('application/json')
		        ->set_output(json_encode(array('price' => $price)));
		}
		
	}

	public function is_aviable(){
		$data = $this->input->post();
		$aviable = $this->property_model->property_is_aviable($data['from'], $data['to'], $data['property_id']);
 		if($aviable > 0){
		$response = $aviable;
			}else{
		$response =  0;		
				}
			$this->output
		        ->set_content_type('application/json')
		        ->set_output(json_encode(array('response' => $response)));
	
		
	}

	public function new_comment(){
		$this->property_model->new_comment($this->input->post());
	}
	
	
	public function new_reservation(){
			
		$config = $this->configuration_model->get_data();
		$this->property_model->new_reservation($this->input->post());
		$this->load->library('email', config_mail());
		$this->email->set_mailtype("html");		
		$this->email->from('no-reply@lfinmobiliaria.com', 'Lafinca');
		
		$destinos = array();
		
		$admins = $this->property_model->get_super_administrators_mail();
		foreach($admins as $row){
		if(!in_array($row->email,$destinos)){ $destinos[]=$row->email; }	
		}
		
		
		
		$agent = $this->property_model->get_agent_property_mail($this->input->post('administrator_relation'));
		foreach($agent as $row){
		if(!in_array($row->email,$destinos)){ $destinos[]=$row->email; }	
		}
		
		if(!in_array('reservas@lfinmobiliaria.com',$destinos)){ $destinos[]='reservas@lfinmobiliaria.com'; }	
		
		$property = $this->property_model->get_property_single($this->input->post('fincas_relation'),1);
		$this->email->to($destinos);
		
//		print_r($destinos);
		$this->email->subject('Reserva '.$property->nombre_text.' - Lafinca.com.co');
		$this->email->message($this->load->view('mail/reservation', $this->input->post(),true));
		$this->email->send();	
 

	}

	public function new_view(){
		$this->property_model->new_view($this->input->post('property'));
	}

	public function add_favorite(){
		$this->property_model->add_favorite($this->input->post('user_id'),$this->input->post('property_id'));
	}

	public function remove_favorite(){
		$this->property_model->remove_favorite($this->input->post('user_id'),$this->input->post('property_id'));
	}


	private function create_date_range_array($strDateFrom,$strDateTo){
	    $aryRange=array();

	   @$iDateFrom=mktime(1,0,0,substr($strDateFrom,5,2),     substr($strDateFrom,8,2),substr($strDateFrom,0,4));
	    @$iDateTo=mktime(1,0,0,substr($strDateTo,5,2),     substr($strDateTo,8,2),substr($strDateTo,0,4));

	    if ($iDateTo>=$iDateFrom)
	    {
	        array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
	        while ($iDateFrom<$iDateTo)
	        {
	            $iDateFrom+=86400; // add 24 hours
	            array_push($aryRange,date('Y-m-d',$iDateFrom));
	        }
	    }
	    return $aryRange;
	}

	private function check_in_range($start_date, $end_date, $date_from_user){
	  	$start_ts = strtotime($start_date);
	  	$end_ts = strtotime($end_date);
	  	$user_ts = strtotime($date_from_user);

	  	return (($user_ts >= $start_ts) && ($user_ts <= $end_ts));
	}
	
}
