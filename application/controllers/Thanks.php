<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Thanks extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('home_model');
		$this->load->model('featured_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
		$this->load->model('property_model');
	}

	public function index(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['section'] = $this->load->view('thanks', $data, true); 

		$this->load->view('template/main', $data);
	}
}
