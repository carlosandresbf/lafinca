<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tourism extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('tourism_model');
		$this->load->model('depto_model');
		$this->load->model('municipio_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
	}

	public function index(){
		redirect('/tourism/depto');
	}


	public function depto(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();

		$data['image_tourism'] = $this->tourism_model->get_data();

		$this->load->library('pagination');
		$config['base_url'] = base_url().'/tourism/depto/';
		$config['total_rows'] = $this->depto_model->get_total_depto();
		$config['per_page'] = 15;
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
    	$config['num_links'] = round($choice);
		
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    	$config['cur_tag_open'] = '<li><span class="active">';
    	$config['cur_tag_close'] = '</span></li>';
    	$config['next_link'] = 'Siguiente';
	    $config['next_tag_open'] = '<div class="nav-next">';
	    $config['next_tag_close'] = '</div>';
	    $config['prev_link'] = 'Anterior';
	    $config['prev_tag_open'] = '<div class="nav-previous">';
	    $config['prev_tag_close'] = '</div>';


		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $data['depto'] = $this->depto_model->get_depto($config['per_page'], $page);
        $data['pagination'] = $this->pagination->create_links();

		$data['section'] = $this->load->view('tourism/depto', $data, true); 

		$this->load->view('template/main', $data);
	}	
	
	public function municipios(){
		$data['depto_id'] = $this->uri->segment(3);
		($data['depto_id']) ? '' : redirect('/tourism/depto');		
		$exists = $this->depto_model->get_depto_exists($data['depto_id']);
		($exists > 0) ? '' : redirect('/tourism/depto');
				
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();

		$data['image_tourism'] = $this->tourism_model->get_data();

		$this->load->library('pagination');
		$config['base_url'] = base_url().'tourism/municipios/'.$data['depto_id'].'/';
		$config['total_rows'] = $this->municipio_model->get_total_municipio($data['depto_id']);
		$config['per_page'] = 15;
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
    	$config['num_links'] = round($choice);
		
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    	$config['cur_tag_open'] = '<li><span class="active">';
    	$config['cur_tag_close'] = '</span></li>';
    	$config['next_link'] = 'Siguiente';
	    $config['next_tag_open'] = '<div class="nav-next">';
	    $config['next_tag_close'] = '</div>';
	    $config['prev_link'] = 'Anterior';
	    $config['prev_tag_open'] = '<div class="nav-previous">';
	    $config['prev_tag_close'] = '</div>';


		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['municipios'] = $this->municipio_model->get_municipio($config['per_page'], $page, $data['depto_id']);
        $data['pagination'] = $this->pagination->create_links();

		$data['section'] = $this->load->view('tourism/municipios', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function grid(){
		 $data['municipio'] = $this->uri->segment(3);
		($data['municipio']) ? '' : redirect('/tourism/depto');		
		$exists = $this->municipio_model->get_municipio_exists(urldecode($data['municipio']));
		$data['municipio']= urldecode($data['municipio']);
		($exists > 0) ? '' : redirect('/tourism/depto');
		
		$this_municipio = $this->municipio_model->get_municipio_single($data['municipio']);
		$data['id_municipio'] = $this_municipio->id;		
		
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		$data['image_tourism'] = $this->tourism_model->get_data();

		$this->load->library('pagination');
		$config['base_url'] = base_url().'tourism/grid/'.$data['municipio'].'/';
		$config['total_rows'] = $this->tourism_model->get_total_tourism($data['id_municipio']);
		$config['per_page'] = 15;
		$config['uri_segment'] = 3;
		$choice = $config['total_rows'] / $config['per_page'];
    	$config['num_links'] = round($choice);
		
    	$config['num_tag_open'] = '<li>';
    	$config['num_tag_close'] = '</li>';
    	$config['cur_tag_open'] = '<li><span class="active">';
    	$config['cur_tag_close'] = '</span></li>';
    	$config['next_link'] = 'Siguiente';
	    $config['next_tag_open'] = '<div class="nav-next">';
	    $config['next_tag_close'] = '</div>';
	    $config['prev_link'] = 'Anterior';
	    $config['prev_tag_open'] = '<div class="nav-previous">';
	    $config['prev_tag_close'] = '</div>';


		$this->pagination->initialize($config);

		$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $data['tourism'] = $this->tourism_model->get_tourism($config['per_page'], $page, $data['id_municipio']);
        $data['pagination'] = $this->pagination->create_links();

		$data['section'] = $this->load->view('tourism/list', $data, true); 

		$this->load->view('template/main', $data);
	}

	public function show(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();

		$tourism = str_replace('_', ' ', $this->uri->segment(3));
		$data['tourism'] = $this->tourism_model->get_tourism_single($tourism);
		$data['municipios'] = $this->municipio_model->get_municipio_single_by_id($data['tourism']->municipios_relation);

		$data['section'] = $this->load->view('tourism/show', $data, true); 

		$this->load->view('template/main', $data);
	}
}
