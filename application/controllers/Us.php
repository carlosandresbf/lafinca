<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Us extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->load->model('us_model');
		$this->load->model('page_model');
		$this->load->model('configuration_model');
	}

	public function index(){
		$data['config'] = $this->configuration_model->get_data();
		$data['pages'] = $this->page_model->get_pages();
		
		$data['us'] = $this->us_model->get_data();
		$data['testimonials'] = $this->us_model->get_testimonials();
		$data['section'] = $this->load->view('us', $data, true); 

		$this->load->view('template/main', $data);
	}
}
