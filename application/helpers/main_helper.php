<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_images_gallery')){
    function get_images_gallery($gallery){
    	$CI = &get_instance();
    	$CI->load->database();
    	$sql = "SELECT * FROM upload WHERE gallery_id = '".$gallery."' ORDER BY 'order'";
		$query = $CI->db->query($sql);
		return $query->result();
    }   
}

if ( ! function_exists('get_copy_r')){
    function get_copy_r(){
    	$CI = &get_instance();
    	$CI->load->database();
    	$sql = "SELECT copy FROM configuration  WHERE id = '1'";
		$query = $CI->db->query($sql);
		return $query->result();
    }   
}
if ( ! function_exists('get_logo_page')){
    function get_logo_page(){
    	$CI = &get_instance();
    	$CI->load->database();
    	$sql = "SELECT picture FROM configuration  WHERE id = '1'";
		$query = $CI->db->query($sql);
		return $query->result();
    }   
}

if ( ! function_exists('multirelacion_data')){
    function multirelacion_data($data, $table){
    	$CI = &get_instance();
    	$CI->load->database();
    	$data = explode(',', $data);
    	$response = array();
    	for ($i=0; $i < count($data) ; $i++) { 
    		$sql = "SELECT * FROM $table WHERE id = '".$data[$i]."'";
			$query = $CI->db->query($sql);
			array_push($response, $query->row());
    	}
    	return $response;
    }   
}

if ( ! function_exists('clip_text')){
    function clip_text($text, $limit = 300){   
        $text = trim($text);
        $text = strip_tags($text);
        $size = strlen($text);
        $result = '';
        if($size <= $limit){
            return $text;
        }else{
            $text = substr($text, 0, $limit);
            $words = explode(' ', $text);
            $result = implode(' ', $words);
            $result .= '...';
        }   
        return $result;
    }
}

if ( ! function_exists('url_encode')){
    function url_encode($text){   
        $text = str_replace(' ', '_', $text);
        $text = sanitize($text);
        $text = strtolower($text);
        return urlencode($text);
    }
}

if ( ! function_exists('sanitize')){
    function sanitize($string){
     
        $string = trim($string);
     
        $string = str_replace(
            array('á', 'à', 'ä', 'â', 'ª', 'Á', 'À', 'Â', 'Ä'),
            array('a', 'a', 'a', 'a', 'a', 'A', 'A', 'A', 'A'),
            $string
        );
     
        $string = str_replace(
            array('é', 'è', 'ë', 'ê', 'É', 'È', 'Ê', 'Ë'),
            array('e', 'e', 'e', 'e', 'E', 'E', 'E', 'E'),
            $string
        );
     
        $string = str_replace(
            array('í', 'ì', 'ï', 'î', 'Í', 'Ì', 'Ï', 'Î'),
            array('i', 'i', 'i', 'i', 'I', 'I', 'I', 'I'),
            $string
        );
     
        $string = str_replace(
            array('ó', 'ò', 'ö', 'ô', 'Ó', 'Ò', 'Ö', 'Ô'),
            array('o', 'o', 'o', 'o', 'O', 'O', 'O', 'O'),
            $string
        );
     
        $string = str_replace(
            array('ú', 'ù', 'ü', 'û', 'Ú', 'Ù', 'Û', 'Ü'),
            array('u', 'u', 'u', 'u', 'U', 'U', 'U', 'U'),
            $string
        );
     
        $string = str_replace(
            array('ñ', 'Ñ', 'ç', 'Ç'),
            array('n', 'N', 'c', 'C',),
            $string
        );     
     
        return $string;
    }
}

if ( ! function_exists('authenticate')){
    function authenticate(){
        $CI = & get_instance();
        $user = $CI->session->userdata('logged_front');

        if (empty($user['name']) || empty($user['user_id']) )
            redirect('/', 'refresh');
    }   
}

if ( ! function_exists('menu_turismo')){
function menu_turismo(){
	     			$CI = & get_instance();	
					
					$CI->load->model('department_model');
					$departamentos = $CI->department_model->get_menu_deptos();
					foreach($departamentos as $depto){
					$data['custom_array_menu'][$depto->id]['nombre'] = $depto->nombre_text;
					$data['custom_array_menu'][$depto->id]['link'] = 'https://www.lafinca.com.co/tourism/municipios/'.$depto->id;
					$municipios = $CI->department_model->get_menu_municipios($depto->id);
					foreach($municipios as $mun){
					$data['custom_array_menu'][$depto->id]['municipios'][$mun->id]['nombre'] = $mun->nombre_text;
					$data['custom_array_menu'][$depto->id]['municipios'][$mun->id]['link'] = 'https://www.lafinca.com.co/tourism/grid/'.$mun->nombre_text;
					
												}
														}
														
					foreach($data['custom_array_menu'] as $depto_id => $values){
                        echo ' <li class="dropdown-submenu-more sub">
                            <a href="'. $values['link'].'">'. $values['nombre'].'</a>
                            <ul class="dropdown-menu ">';
					foreach($data['custom_array_menu'][$depto_id]['municipios'] as $mun_id => $values_mun){
                                echo '<li class="sub"><a href="'.$values_mun['link'].'">'.$values_mun['nombre'].'</a></li>';
																				}
                        echo '</ul>
                          </li>';
                     
               
			   
					}
					}
		}
		
if ( ! function_exists('menu_eventos')){	
function menu_eventos(){
		
						$CI = & get_instance();	
						$CI->load->model('department_model');
					$departamentos = $CI->department_model->get_menu_deptos();
					foreach($departamentos as $depto){
					$data['custom_array_menu'][$depto->id]['nombre'] = $depto->nombre_text;
					$data['custom_array_menu'][$depto->id]['link'] = 'https://www.lafinca.com.co/events/municipios/'.$depto->id;
					$municipios = $CI->department_model->get_menu_municipios($depto->id);
					foreach($municipios as $mun){
					$data['custom_array_menu'][$depto->id]['municipios'][$mun->id]['nombre'] = $mun->nombre_text;
					$data['custom_array_menu'][$depto->id]['municipios'][$mun->id]['link'] = 'https://www.lafinca.com.co/events/grid/'.$mun->nombre_text;
					
												}
														}
														
					foreach($data['custom_array_menu'] as $depto_id => $values){
                        echo ' <li class="dropdown-submenu-more sub">
                            <a href="'. $values['link'].'">'. $values['nombre'].'</a>
                            <ul class="dropdown-menu ">';
					foreach($data['custom_array_menu'][$depto_id]['municipios'] as $mun_id => $values_mun){
                                echo '<li class="sub"><a href="'.$values_mun['link'].'">'.$values_mun['nombre'].'</a></li>';
																				}
                        echo '</ul>
                          </li>';
                     
                     }
		}
}

if ( ! function_exists('verify_favorite')){
    function verify_favorite($user, $property){
        $CI = &get_instance();
        $CI->load->database();
        $sql = "SELECT * FROM favorites WHERE user_id = '".$user."' AND property_id = '".$property."'";
        $query = $CI->db->query($sql);
        if ($query->num_rows() > 0)
            return true;
        
        return false;
    }   
}


if ( ! function_exists('config_mail')){
    function config_mail(){
        return $config = array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => '465',
                'smtp_user' => 'david.ardila@imaginamos.com.co',
                'smtp_pass' => 'Dvt94012213705',
                'wordwrap' => TRUE,
                'charset' => 'iso-8859-1',
                'mailtype' => 'html'
            );
    }   
}