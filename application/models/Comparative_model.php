<?php
Class Comparative_model extends CI_Model {

	function get_data($properties) {
		if (empty($properties))
			return array();
		
		$this->db->select('*');
		$this->db->from('fincas');
		$this->db->where_in('id', $properties);
		
		return $this->db->get()->result();
	}
}