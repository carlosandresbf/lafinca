<?php
Class Contact_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('imagen_contacto');
		
		return $this->db->get()->row();
	}

	function add_contact($data) {
		date_default_timezone_set('America/Bogota');
		$this->db->set('created_at', date('Y-m-d h:i:s'));
		$this->db->set('fecha_de_envio_datetime', date('Y-m-d h:i:s'));
		$this->db->insert('mensajes_de_contacto', $data);
	}
}