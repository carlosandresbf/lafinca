<?php
Class Department_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('departamentos');
		$this->db->order_by('nombre_text');

		return $this->db->get()->result();
	}
	
	function get_menu_deptos() {
		$this->db->select('*');
		$this->db->from('departamentos_eventos_turismo');
		$this->db->order_by('nombre_text');

		return $this->db->get()->result();
	}
		function get_menu_municipios($depto) {
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->where('departamentos_eventos_turismo_relation',$depto);		
		$this->db->order_by('nombre_text');

		return $this->db->get()->result();
	}
}