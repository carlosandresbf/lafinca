<?php
Class Depto_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('departamentos_eventos_turismo');
		
		return $this->db->get()->row();
	}

	function get_depto($limit, $start) {
		$this->db->select('*');
		$this->db->from('departamentos_eventos_turismo');
		$this->db->where('activo_radio', 'Si');
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit($limit, $start);
		
		return $this->db->get()->result();
	}

	function get_total_depto() {
		$this->db->select('COUNT(*) as count');
		$this->db->from('departamentos_eventos_turismo');
		$this->db->where('activo_radio', 'Si');
		
		return $this->db->get()->row('count');
	}
	
	function get_depto_exists($id) {
		$this->db->select('COUNT(*) as count');
		$this->db->from('departamentos_eventos_turismo');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('id', $id);
		
		return $this->db->get()->row('count');
	}

	function get_depto_single($event){
		$this->db->select('*');
		$this->db->from('departamentos_eventos_turismo');
		$this->db->like('nombre_text', $event);
		
		return $this->db->get()->row();
	}
}