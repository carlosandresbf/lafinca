<?php
Class Event_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('imagen_eventos');
		
		return $this->db->get()->row();
	}

	function get_events($limit, $start, $id_mun) {
		$this->db->select('*');
		$this->db->from('eventos');
		$this->db->where('activo_radio', 'Si');		
		$this->db->where('municipios_relation', $id_mun);		
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit($limit, $start);
		
		return $this->db->get()->result();
	}

	function get_total_events($id_mun) {
		$this->db->select('COUNT(*) as count');
		$this->db->from('eventos');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('municipios_relation', $id_mun);
		
		return $this->db->get()->row('count');
	}

	function get_event($event){
		$this->db->select('*');
		$this->db->from('eventos');
		$this->db->like('titulo_text', $event);
		
		return $this->db->get()->row();
	}
}