<?php
Class Favorite_model extends CI_Model {

	function get_data($user) {
		$this->db->select('fincas.*');
		$this->db->from('favorites, fincas');
		$this->db->where('user_id', $user);
		$this->db->where('fincas.id = favorites.property_id');
		
		return $this->db->get()->result();
	}
}