<?php
Class Featured_model extends CI_Model {

	function get_featured($featured = null) {
		$this->db->select('*');
		$this->db->from('recomendados');
		$this->db->where('activo_radio', 'Si');
		$this->db->order_by('created_at', 'DESC');

		if (!is_null($featured)) {
			$this->db->like('titulo_text', $featured);
			return $this->db->get()->row();
		}
		return $this->db->get()->result();
	}
}