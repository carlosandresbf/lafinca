<?php
Class Home_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('home');
		
		return $this->db->get()->row();
	}

	function get_steps(){
		$this->db->select('*');
		$this->db->from('alquilar_en_3_pasos');
		$this->db->order_by('record_order');
		
		return $this->db->get()->result();
	}

	function get_videos(){
		$this->db->select('*');
		$this->db->from('videos_home');
		$this->db->order_by('record_order');
		
		return $this->db->get()->result();
	}

	function add_report($data){
		$this->db->set('created_at', date('Y-m-d h:i:s'));
		$this->db->insert('reporte_de_errores', $data);
	}

}