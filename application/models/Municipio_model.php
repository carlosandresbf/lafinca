<?php
Class Municipio_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('municipios');
		
		return $this->db->get()->row();
	}
	
	function get_data_by_depto($id_Depto) {
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->where('departamentos_eventos_turismo_relation', $id_Depto);
		
		return $this->db->get()->row();
	}
	function get_municipio($limit, $start, $id_Depto) {
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->where('activo_radio', 'Si');		
		$this->db->where('departamentos_eventos_turismo_relation', $id_Depto);
		$this->db->order_by('created_at', 'DESC');
		$this->db->limit($limit, $start);
		
		return $this->db->get()->result();
	}

	function get_total_municipio($id_Depto) {
		$this->db->select('COUNT(*) as count');
		$this->db->from('municipios');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('departamentos_eventos_turismo_relation', $id_Depto);
		
		return $this->db->get()->row('count');
	}

	function get_municipio_exists($event){
		$this->db->select('COUNT(*) as count');
		$this->db->from('municipios');
		$this->db->like('nombre_text', $event);
		$this->db->where('activo_radio', 'Si');
		
		return $this->db->get()->row('count');
	}

	function get_municipio_single($event){
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->like('nombre_text', $event);
		
		return $this->db->get()->row();
	}	
	
	function get_municipio_single_by_id($event){
		$this->db->select('*');
		$this->db->from('municipios');
		$this->db->like('id', $event);
		
		return $this->db->get()->row();
	}
}