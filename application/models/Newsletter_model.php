<?php
Class Newsletter_model extends CI_Model {

	function save_email($email) {
		if (!$this->exist_email($email)) {
			$data['created_at'] = date('Y-m-d h:i:s');
			$data['email_text'] = $email;
			$this->db->insert('newsletter', $data);
		}
		
	}

	function exist_email($email){
		$this->db->select('*');
		$this->db->from('newsletter');
		$this->db->where('email_text', $email);
		
		$query = $this->db->get();

		if ($query->num_rows() > 0)
		   return true;

		return false;
	}
}