<?php
Class Page_model extends CI_Model {

	function get_pages($page = null) {
		$this->db->select('*');
		$this->db->from('links_de_interes');
		$this->db->where('activo_radio', 'Si');
		$this->db->order_by('record_order', 'DESC');

		if (!is_null($page)) {
			$this->db->like('titulo_text', $page);
			return $this->db->get()->row();
		}
		return $this->db->get()->result();
	}
}