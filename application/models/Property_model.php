<?php
Class Property_model extends CI_Model {
	function __construct(){
		parent::__construct();

		$this->load->model('configuration_model');
	}
	public $start_date = '';
	public $end_date = '';
	
	function get_featured() {
		$config = $this->configuration_model->get_data();

		$this->db->select('*');
		$this->db->from('fincas');
		$this->db->where('recomendada_radio', 'Si');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('estado_aprobacion_radio', 'Aprobada');			
		$this->db->limit($config->num_properties_in_the_home);
		
		return $this->db->get()->result();
	}

	function get_property_single($property, $type = null){
		$this->db->select('fincas.*,
							administrator.id as administrator_id, 
							administrator.name as administrator_name,
							administrator.picture as administrator_picture,
							administrator.email as administrator_email,
						');
		$this->db->from('fincas');
		$this->db->join('administrator', 'administrator.id = fincas.administrator_relation', 'left');		
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');
		if (is_null($type))
			$this->db->where('nombre_text', $property);
		else
			$this->db->where('fincas.id', $property);
		
		return $this->db->get()->row();
	}

	function get_comments_by_property($property){
		$this->db->select('*');
		$this->db->from('comentarios');
		$this->db->where('fincas_relation', $property);
		$this->db->where('aprobado_radio', 'Si');
		$this->db->order_by('created_at', 'DESC');
		
		return $this->db->get()->result();
	}

	function get_same_properties($property){
		//$latlng = explode(',', $property->ubicacion_map);
		/*$this->db->select('*, 
			((ACOS(SIN('.$latlng[0].' * PI() / 180) * SIN(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) + COS('.$latlng[0].' * PI() / 180) * COS(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) * COS(('.$latlng[1].' - SUBSTRING_INDEX(ubicacion_map,",",-1)) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance');*/

		$this->db->select('fincas.*');
		$this->db->from('fincas, departamentos');
		$this->db->like('modalidad_select', $property->modalidad_select);
		$this->db->like('tiempo_select', $property->tiempo_select);
		$this->db->where_not_in('fincas.id', $property->id);
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 
		$this->db->where('departamentos.id', $property->departamentos_relation); 		
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');
		$this->db->where('fincas.activo_radio', 'Si');
		//$this->db->having('distance <= 25');
		$this->db->limit('3');
		$this->db->order_by('fincas.id','RANDOM');
		//$this->db->order_by('distance');
		
		return $this->db->get()->result();
	}

	function new_comment($comment){
		$comment['created_at'] = date('Y-m-d h:i:s');
		$comment['fecha_de_comentario_datetime'] = date('Y-m-d h:i:s');
		$this->db->insert('comentarios', $comment);
	}

	function new_reservation($comment){
		date_default_timezone_set('America/Bogota');
		$comment['created_at'] = date('Y-m-d H:i:s');
		$comment['reserva_hecha_el_dia_datetime'] = date('Y-m-d H:i:s');
		
		$this->db->insert('reservas_de_clientes', $comment);
	}

	function new_view($property){
		/*$this->db->set('numero_de_visitas_text', 'numero_de_visitas_text + 1', false);
		$this->db->where('id', $property);
		$this->db->update('fincas');
		Se actualiza el registro de las visitas para poder medir el periodo de fechas en el cual se hacen
		C Beltran
		*/
		$data = array(
   'id_finca_relation' => $property,
   'fecha_date' => date('Y-m-d H:i:s') );
	$this->db->insert('fincas_visitas_fecha', $data); 	
		
	}
	
	function total_views($property)
		{
		$this->db->select('COUNT(*) as count');	
		$this->db->from('fincas_visitas_fecha');
		$this->db->where('id_finca_relation', $property); 
		return	$this->db->get()->row();
	 				}
				
	function get_config_serch($type, $department, $filters = array()){
		/*$this->db->select('COUNT(*) as count, MAX(precio_temporada_alta_number) AS max_price,
			((ACOS(SIN('.$lat.' * PI() / 180) * SIN(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) * COS(('.$lng.' - SUBSTRING_INDEX(ubicacion_map,",",-1)) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance');*/
		$this->db->select('COUNT(*) as count, MAX(CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED )) AS max_price');
		if (!empty($filters['price'])){
			$this->db->select('CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED ) AS price');
		}
		
		$this->db->from('fincas, departamentos');
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 
		if(!empty($type)){$this->db->like('modalidad_select', $type);}
		if(!empty($department)){$this->db->where('departamentos.id', $department);}
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada'); 
		$this->db->where('fincas.activo_radio', 'Si');

		if (!empty($filters['capacity'])){
			$capacity = explode('-', $filters['capacity']);
			$this->db->where('capacidad_text >=', $capacity[0]);
			$this->db->where('capacidad_text <=', $capacity[1]);
		}

		if (!empty($filters['start']) && !empty($filters['end'])){
			$prop = $this->get_properties_not_available($filters['start'], $filters['end']);
			$custom=array();
			
			foreach($prop as $itemxx){
			$custom[] = $itemxx->property_id;
									} 
			if(count($custom) > 0){
			$this->db->where_not_in('fincas.id', $custom);
			}
																}
		if (!empty($filters['services'])){
			$services = explode(',', $filters['services']);
			$this->db->group_start();
			for ($i=0; $i < count($services); $i++) { 
				$this->db->or_like('caracteristicas_y_servicios_multirelation', $services[$i]);
			}
			$this->db->group_end();
		}


		//$this->db->having('distance <= 25');
		
		$a =  $this->db->get()->row();
		return $a;
	}

	function get_properties_search($type, $department, $limit, $start, $filters = array()){
		
		/*$this->db->select('*, 
			((ACOS(SIN('.$lat.' * PI() / 180) * SIN(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) * COS(('.$lng.' - SUBSTRING_INDEX(ubicacion_map,",",-1)) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance');*/
			if (!empty($filters['start']) && !empty($filters['end'])){
			$xplod1=explode('/',$filters['start']);
			$xplod2=explode('/',$filters['end']);
			$filters['start']=$xplod1[2].'-'.$xplod1[1].'-'.$xplod1[0];
			$filters['end']=$xplod2[2].'-'.$xplod2[1].'-'.$xplod2[0];
			
			$this->start_date = $filters['start'];
			$this->end_date = $filters['end'];
			
			$this->db->select(' DISTINCT(fincas.nombre_text) AS clear, PP.*,
  IF(((( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )  AND LOWER(PP.title) = "alta"),  CAST( REPLACE( fincas.precio_temporada_alta_number, ".", "" ) AS UNSIGNED )  , IF (((( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )  AND LOWER(PP.title) = "media"),  CAST( REPLACE( fincas.precio_temporada_media_number, ".", "" ) AS UNSIGNED ) ,  CAST( REPLACE( fincas.precio_temporada_baja_number, ".", "" ) AS UNSIGNED ) )) AS price
 , fincas.*');			
			}else{
		$this->db->select('fincas.*, CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED ) AS price');		
				}
		
		/*if (!empty($filters['price'])){
			$this->db->select('CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED ) AS price');
		}*/
		$this->db->from('fincas, departamentos');
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 	
		
			if (!empty($filters['start']) && !empty($filters['end'])){
				
				$this->db->join('property_price AS PP', '`PP`.`property_id` = `fincas`.`id`  AND (( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )', 'LEFT');
																		}


			

			
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');		
		$this->db->where('fincas.activo_radio', 'Si');

		//$this->db->having('distance <= 25');
		
		if (!empty(trim($filters['name_id']))) {
			$this->db->group_start();
			$this->db->like('fincas.nombre_text', $filters['name_id']);
			$this->db->or_like('identificador_text', $filters['name_id']);
			$this->db->group_end();
		}/*else{*/
		
		if (!empty($filters['start']) && !empty($filters['end'])){
			$prop = $this->get_properties_not_available($filters['start'], $filters['end']);
			$custom=array();
			//print_r($prop);
			foreach($prop as $item){
			array_push($custom, $item->property_id);
									}
			if(count($custom) > 0){
			$this->db->where_not_in('fincas.id', $custom);
			}
																}
																
		if(!empty($type)){	$this->db->like('modalidad_select', $type); }
			if(!empty($department)){
			$this->db->where('departamentos.id', $department); 
			}
			
			if (!empty($filters['order']))
				$this->db->order_by(str_replace('precio_temporada_alta_number','price',$filters['order']));

			if (!empty($filters['capacity'])){
				$capacity = explode('-', $filters['capacity']);
				$this->db->where('capacidad_text >=', $capacity[0]);
				$this->db->where('capacidad_text <=', $capacity[1]);				
				$this->db->order_by('capacidad_text', 'DESC');
			}

			if (!empty($filters['price'])){
			
				$price = explode(';', $filters['price']);
				//$this->db->where('price BETWEEN "'.$price[0].'" AND "'.$price[1].'"');
				$this->db->having('(price BETWEEN "'.$price[0].'" AND "'.$price[1].'")'); 
			}

			if (!empty($filters['services'])){
				$services = explode(',', $filters['services']);
				$this->db->group_start();
				for ($i=0; $i < count($services); $i++) { 
					$this->db->or_like('caracteristicas_y_servicios_multirelation', $services[$i]);
				}
				$this->db->group_end();
			}
	/*	}*/

		$this->db->limit($limit, $start);
		
		$a = $this->db->get()->result();
		//echo $this->db->last_query();
		return $a;
	}

function get_properties_search_count($type, $department,  $filters = array()){
		/*$this->db->select('*, 
			((ACOS(SIN('.$lat.' * PI() / 180) * SIN(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) * COS(('.$lng.' - SUBSTRING_INDEX(ubicacion_map,",",-1)) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance');*/
		if (!empty($filters['start']) && !empty($filters['end'])){
			$xplod1=explode('/',$filters['start']);
			$xplod2=explode('/',$filters['end']);
			$filters['start']=$xplod1[2].'-'.$xplod1[1].'-'.$xplod1[0];
			$filters['end']=$xplod2[2].'-'.$xplod2[1].'-'.$xplod2[0];
			$this->start_date = $filters['start'];
			$this->end_date = $filters['end'];
			$this->db->select('DISTINCT(fincas.nombre_text) AS clear, PP.*,
  IF(((( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )  AND LOWER(PP.title) = "alta"),  CAST( REPLACE( fincas.precio_temporada_alta_number, ".", "" ) AS UNSIGNED )  , IF (((( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )  AND LOWER(PP.title) = "media"),  CAST( REPLACE( fincas.precio_temporada_media_number, ".", "" ) AS UNSIGNED ) ,  CAST( REPLACE( fincas.precio_temporada_baja_number, ".", "" ) AS UNSIGNED ) )) AS price
 , fincas.*');			
			}else{
		$this->db->select('fincas.*, CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED ) AS price');		
				}
		
		/*if (!empty($filters['price'])){
			$this->db->select('CAST( REPLACE( fincas.precio_temporada_alta_number,  ".",  "" ) AS UNSIGNED ) AS price');
		}*/
		$this->db->from('fincas, departamentos');
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 	
		
			if (!empty($filters['start']) && !empty($filters['end'])){
				
				$this->db->join('property_price AS PP', '`PP`.`property_id` = `fincas`.`id`  AND (( DATE("'.$filters['start'].'") = PP.start_date  AND  DATE("'.$filters['end'].'") = PP.end_date )   OR  ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.start_date ) OR ( DATE("'.$filters['start'].'") <= PP.end_date    AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") <= PP.start_date  AND  DATE("'.$filters['end'].'") >= PP.end_date )   OR ( DATE("'.$filters['start'].'") >= PP.start_date  AND  DATE("'.$filters['end'].'") <= PP.end_date ) )', 'LEFT');
																		}
			
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');		
		$this->db->where('fincas.activo_radio', 'Si');

		//$this->db->having('distance <= 25');
		
		if (!empty(trim($filters['name_id']))) {
			$this->db->group_start();
			$this->db->like('fincas.nombre_text', $filters['name_id']);
			$this->db->or_like('identificador_text', $filters['name_id']);
			$this->db->group_end();
		}/*else{*/
		
		if (!empty($filters['start']) && !empty($filters['end'])){
			$prop = $this->get_properties_not_available($filters['start'], $filters['end']);
			$custom=array();
			//print_r($prop);
			foreach($prop as $item){
			array_push($custom, $item->property_id);
									}
			if(count($custom) > 0){
			$this->db->where_not_in('fincas.id', $custom);
			}
																}
																
		if(!empty($type)){	$this->db->like('modalidad_select', $type); }
			if(!empty($department)){
			$this->db->where('departamentos.id', $department); 
			}
			
			if (!empty($filters['order']))
				$this->db->order_by(str_replace('precio_temporada_alta_number','price',$filters['order']));

			if (!empty($filters['capacity'])){
				$capacity = explode('-', $filters['capacity']);
				$this->db->where('capacidad_text >=', $capacity[0]);
				$this->db->where('capacidad_text <=', $capacity[1]);				
				$this->db->order_by('capacidad_text', 'DESC');
			}

			if (!empty($filters['price'])){
			
				$price = explode(';', $filters['price']);
			//$this->db->where('price BETWEEN "'.$price[0].'" AND "'.$price[1].'"');
			
				$this->db->having('(price BETWEEN "'.$price[0].'" AND "'.$price[1].'")'); 
			}

			if (!empty($filters['services'])){
				$services = explode(',', $filters['services']);
				$this->db->group_start();
				for ($i=0; $i < count($services); $i++) { 
					$this->db->or_like('caracteristicas_y_servicios_multirelation', $services[$i]);
				}
				$this->db->group_end();
			}
	/*	}*/

		$this->db->limit($limit, $start);
		
		$a = $this->db->get()->result();
		//echo $this->db->last_query(); exit();
		return $a;
		/*}
*/
	}

function get_properties_min_price($type, $department,  $filters = array()){
		/*$this->db->select('*, 
			((ACOS(SIN('.$lat.' * PI() / 180) * SIN(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) + COS('.$lat.' * PI() / 180) * COS(SUBSTRING_INDEX(ubicacion_map,",",1) * PI() / 180) * COS(('.$lng.' - SUBSTRING_INDEX(ubicacion_map,",",-1)) * PI() / 180)) * 180 / PI()) * 60 * 1.1515 * 1.609344) as distance');*/
		$this->db->select('CAST( (
REPLACE( fincas.precio_temporada_alta_number, ".", "" ) ) AS UNSIGNED
) AS  price');
		$this->db->from('fincas, departamentos');
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 		
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');
		$this->db->where('fincas.activo_radio', 'Si');

		//$this->db->having('distance <= 25');
		
		if (!empty(trim($filters['name_id']))) {
			$this->db->group_start();
			$this->db->like('fincas.nombre_text', $filters['name_id']);
			$this->db->or_like('identificador_text', $filters['name_id']);
			$this->db->group_end();
		}/*else{*/
			if(!empty($type)){$this->db->like('modalidad_select', $type);}
			if(!empty($department)){
			$this->db->where('departamentos.id', $department);
			}

			
			
			$this->db->order_by('price', 'ASC');
			$this->db->limit(1);

			if (!empty($filters['capacity'])){
				$capacity = explode('-', $filters['capacity']);
				$this->db->where('capacidad_text >=', $capacity[0]);
				$this->db->where('capacidad_text <=', $capacity[1]);
			}

			if (!empty($filters['services'])){
				$services = explode(',', $filters['services']);
				$this->db->group_start();
				for ($i=0; $i < count($services); $i++) { 
					$this->db->or_like('caracteristicas_y_servicios_multirelation', $services[$i]);
				}
				$this->db->group_end();
			/*}*/
		}
			$this->db->order_by("price", "asc");
		//$this->db->limit($limit, $start);
		
		$a = $this->db->get()->result();
		//echo  $this->db->last_query();
		return $a;
	}

function get_properties_max_price($type, $department,  $filters = array()){
		$this->db->select(' CAST( (
REPLACE( fincas.precio_temporada_alta_number, ".", "" ) ) AS UNSIGNED
) AS  price');
		$this->db->from('fincas, departamentos');
		$this->db->where('departamentos.id = fincas.departamentos_relation'); 		
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');
		$this->db->where('fincas.activo_radio', 'Si');

		//$this->db->having('distance <= 25');
		
		if (!empty(trim($filters['name_id']))) {
			$this->db->group_start();
			$this->db->like('fincas.nombre_text', $filters['name_id']);
			$this->db->or_like('identificador_text', $filters['name_id']);
			$this->db->group_end();
		}/*else{*/
			if(!empty($type)){$this->db->like('modalidad_select', $type);}
			if(!empty($department)){
			$this->db->where('departamentos.id', $department);
			}

			
			
			$this->db->order_by('price', 'DESC');
			$this->db->limit(1);

			if (!empty($filters['capacity'])){
				$capacity = explode('-', $filters['capacity']);
				$this->db->where('capacidad_text >=', $capacity[0]);
				$this->db->where('capacidad_text <=', $capacity[1]);
			}

			if (!empty($filters['services'])){
				$services = explode(',', $filters['services']);
				$this->db->group_start();
				for ($i=0; $i < count($services); $i++) { 
					$this->db->or_like('caracteristicas_y_servicios_multirelation', $services[$i]);
				}
				$this->db->group_end();
			}
		/*}*/
		$this->db->order_by("price", "desc");
		//$this->db->limit($limit, $start);
		
		$a = $this->db->get()->result();
		//echo  $this->db->last_query();
		return $a;
	}

	function get_services(){
		$this->db->select('*');
		$this->db->from('caracteristicas_y_servicios');
		$this->db->order_by('record_order');
		
		return $this->db->get()->result();
	}

	function get_prices($data){
			
		$this->db->select('id');
		$this->db->from('fincas');		
		$this->db->where('activo_radio', 'Si');		
		$this->db->limit(1);
		$query = $this->db->get();
		$finca =  $query->result();			

		

		
		$this->db->select('title, start_date, end_date');
		$this->db->from('property_price');
		//$this->db->where('property_id =', $data['property_id']);
		$this->db->where('property_id', $finca[0]->id);
		$this->db->order_by('start_date');
		
		$a = $this->db->get()->result();
		//echo $this->db->last_query();
		return $a;
	}

	function add_favorite($user, $property){
		$data = array(
		   'user_id' => $user,
		   'property_id' => $property,
		   'created_at' => date('Y-m-d h:i:s')
		);

		$this->db->insert('favorites', $data); 
	}

	function remove_favorite($user, $property){
		$this->db->where('user_id', $user);
		$this->db->where('property_id', $property);
		$this->db->delete('favorites'); 
	}

	function get_disponibility($property) {
		$this->db->select('*');
		$this->db->where('property_id', $property);
		$this->db->from('property_disponibility');
		$this->db->order_by('start_date');

		$query = $this->db->get();
		return $query->result();
	}

	function get_property_by_agent($agent){
		$this->db->select('*');
		$this->db->from('fincas');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('administrator_relation', $agent);		
		$this->db->where('fincas.estado_aprobacion_radio', 'Aprobada');
		
		return $this->db->get()->result();
	}
	
	
	function get_super_administrators_mail(){
		$this->db->select('email');
		$this->db->from('administrator');		
		$this->db->where('is_super_administrator', '1');

		$query = $this->db->get();
		return $query->result();	 
	}
	function get_agent_property_mail($id){
		$this->db->select('email');
		$this->db->from('administrator');		
		$this->db->where('id', $id);

		$query = $this->db->get();
		$a =  $query->result();	 
//		echo $this->db->last_query();
		return $a;
	}
	
	function property_is_aviable($start, $end, $property){
		$query = $this->db->query("SELECT * FROM property_disponibility WHERE  (
		( DATE('".$start."') = start_date  AND  DATE('".$end."') = end_date )   OR 
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= start_date ) OR 
		( DATE('".$start."') <= end_date    AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') >= start_date  AND  DATE('".$end."') <= end_date )
															 ) AND property_id = '".$property."';");	 
															 

		return $query->num_rows();
		
	}
	
		
	 public function get_properties_not_available($start='', $end=''){
		if(empty($this->start_date) || empty($this->end_date)){
		$f1= explode('/',$start);
		$f2= explode('/',$end);
		$start=$f1[2].'-'.$f1[1].'-'.$f1[0];
		$end=$f2[2].'-'.$f2[1].'-'.$f2[0];
		$this->start_date=$start;
		$this->end_date=$end;
		}
		$start=$this->start_date;
		$end=$this->end_date;
		
		$query = $this->db->query("SELECT id, property_id FROM property_disponibility WHERE  (
		( DATE('".$start."') = start_date  AND  DATE('".$end."') = end_date )   OR 
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= start_date ) OR 
		( DATE('".$start."') <= end_date    AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') <= start_date  AND  DATE('".$end."') >= end_date )   OR
		( DATE('".$start."') >= start_date  AND  DATE('".$end."') <= end_date )
															 )");	 
															 

		
		$a =  $query->result();			
		//echo $this->db->last_query();
		return $a;		
							}
	
		
		
}