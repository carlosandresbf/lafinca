<?php
Class Tourism_model extends CI_Model {

	function get_data() {
		$this->db->select('*');
		$this->db->from('imagen_informacion_turistica');
		
		return $this->db->get()->row();
	}

	function get_tourism($limit, $start, $id_mun) {
		$this->db->select('*');
		$this->db->from('informacion_turistica');
		$this->db->where('activo_radio', 'Si');
		$this->db->order_by('created_at', 'DESC');
		$this->db->where('municipios_relation', $id_mun);
		$this->db->limit($limit, $start);
		
		return $this->db->get()->result();
	}

	function get_total_tourism($id_mun) {
		$this->db->select('COUNT(*) as count');
		$this->db->from('informacion_turistica');
		$this->db->where('activo_radio', 'Si');
		$this->db->where('municipios_relation', $id_mun);
		
		return $this->db->get()->row('count');
	}

	function get_tourism_single($event){
		$this->db->select('*');
		$this->db->from('informacion_turistica');
		$this->db->like('titulo_text', $event);
		
		return $this->db->get()->row();
	}
}