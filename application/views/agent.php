<div id="page-content-agent">
	<div class="wide_container_2">
		<div class="container">
			<div class="content">
				<?php if($agent->picture){ ?>
				<div class="image-border col-md-3 col-sm-3">
					<div class="agency-image" style="background-image:url(<?= base_url() ?>admin/uploads/files/<?= $agent->picture ?>)"></div>
				</div>
				<?php } ?>
				<div class="agent-info col-md-9 col-sm-9 agency-info">
					<h3><a href="#"><?= $agent->name ?></a></h3>
					<br><br>
					<div class="row">
						<div class="text-block agency-contact">
							<div class="row">
								<div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
									<div class="col-md-5 col-sm-5 col-xs-5">
										<h3>Datos de contacto:</h3>
									</div>
									<div class="col-md-7 col-sm-7 col-xs-7">
										<?php if(!empty($agent->telephone_1)){ ?>
										<a href="tel:<?= $agent->telephone_1 ?>"><p class="strong"><i class="fa fa-phone"></i><?= $agent->telephone_1 ?></p></a>
										<?php } ?>
										<?php if(!empty($agent->telephone_2)){ ?>
										<a href="tel:<?= $agent->telephone_2 ?>"><p class="strong"><i class="fa fa-phone"></i><?= $agent->telephone_2 ?></p></a>
										<?php } ?>
									</div>
								</div>
								<div class="col-lg-3 col-md-13">
									<?php if(!empty($agent->facebook) || !empty($agent->twitter)){ ?>
									<h3 class="twitter">Redes sociales:</h3>
									<?php if(!empty($agent->facebook)){ ?>
									<a href="<?= $agent->facebook ?>" target="blank"><i class="fa fa-facebook"></i></a>
									<?php } ?>

									<?php if(!empty($agent->twitter)){ ?>
									<a href="<?= $agent->twitter ?>" target="blank"><i class="fa fa-twitter"></i></a>
									<?php } ?>

									<?php } ?>
								</div>
							</div>
						</div>
					</div><!-- text-block contact -->
					<span class="ffs-bs"><a class="btn btn-large btn-primary" href="#" data-toggle="modal" data-target="#modal-error">Enviar mensaje al agente</a></span>
				</div><!-- agent-info -->
			</div><!-- content -->
		</div><!-- container -->
	</div>
	<div class="wide_container_2">
		<div class="tabs">
			<div class="input-group clearfix"> 
<input type="hidden" id="active_comparations" value="0">
				<h2>Inmuebles del agente (<?= count($properties) ?>)</h2>
			</div>
            <div class="clearfix"></div>
			<!-- tab-links -->
			<div class="tab-content">
            	<!-- tab active -->
				<div id="tab2" class="tab active">
					<div class="wide-2">
						<div class="container">
							<div class="row">
								<?php foreach ($properties as $property) { ?>
								<div class="col-md-3 col-sm-3 col-xs-6 prop">
									<div class="wht-cont">
										<div class="exp-img-2" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
											<span class="filter"></span>
									
											<a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><span class="ffs-bs" style="margin-top:60px;"><label class="btn btn-small btn-primary">Ampliar info</label></span></a>
											<div class="overlay">
												<div class="img-counter"></div>
											</div>
										</div>
										<div class="item-title">
											<h4 ><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><?= $property->nombre_text ?></a></h4>
											<p class="team-color"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->ubicacion_text ?></p>
											<p class="team-color"><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
											<p class="team-color"><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> cuartos</p>
                                            
										</div>
										<hr>
										<div class="item-title btm-part">
											<div class="row">
												<div class="col-md-8 col-sm-8 col-xs-8" style="white-space:nowrap;">
													<p>$<?= $property->precio_temporada_alta_number ?> / <?= $property->tiempo_select ?></p>
													<p class="team-color"><?= $property->modalidad_select ?></p>
												</div>
												<div style="margin-left: 41px;" class="col-md-2 col-sm-2 col-xs-2 favorite">
													<?php if(isset($data)){ ?>
													<div data-property="<?= $property->id ?>" class="bookmark <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
														<span class="title-add">Agregar a favoritos</span>
													</div>
													<?php } ?>
													<div data-property="<?= $property->id ?>" class="compare" data-compare-state="empty">
														<span class="plus-add">Compara 3 fincas</span>
													</div>
												</div>
											</div>
                                            
                                            <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 favorite">
                                                    <a class="comparation_link" style="display:none" href="https://www.lafinca.com.co/comparative"><div class="bt_gris">Ver comparación</div></a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-2 favorite"></div>
                                                    </div>
										</div>
									</div>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
					<!-- end wide-2 -->
				</div>
			</div><!-- tab-content -->
		</div><!-- tabs -->

	</div><!-- wide_container_2 -->
</div>
<input type="hidden" id="contact-agent" name="email" value="<?= $agent->email ?>">
<?php $this->load->view('partials/modal_agent_message', array('agent' => $agent->name)); ?>

<script>
	$(document).ready(function() {
		
		
    	$(document).on('click', '.compare', function(event) {
			event.preventDefault();
			var property = $(this).data('property');
			var actual =  $('#active_comparations').val();

			if ($(this).hasClass('compare-added')){
				url = 'remove_comparative';
				actual =  parseInt(actual) - 1;
			}else{
				url = 'add_comparative';
				actual =  parseInt(actual) + 1;}
				console.log(actual);
			$('#active_comparations').val(actual);
			if(actual > 1){$('.comparation_link').show();}else{$('.comparation_link').hide();}
			$(this).toggleClass('compare-added');
			
			$.ajax({
			  url: $('#base_url').val()+'comparative/'+url,
			  type: 'POST',
			  data: {property: property}
			}).done(function() {});
		});
		
		$('#header').removeClass('menu-wht');
	});
</script>