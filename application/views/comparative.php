 <div id="page-content">
  <div class="container" id="compare">
    <header><h2>Comparar Fincas</h2>
      <p>Vas a estar seguro que escogiste la mejor</p>
    </header>
    <section id="property-package">
      <div class="table-responsive submit-pricing">
      	<?php if($properties){ ?>
        <table class="table">
          <thead>
            <tr>
              <th class="h4">Las fincas:</th>
              <?php foreach ($properties as $property) { ?>
              <th class="picture">
                <div class="property-image" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;
                background-size: cover;">                           
                </div>
              </th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="rigt-align">Precio</td>
              <?php foreach ($properties as $property) { ?>
              <td>$<?= $property->precio_temporada_alta_number ?> /<?= $property->tiempo_select ?></td>
              <?php } ?>
            </tr>
            <tr>
              <td class="rigt-align">Nombre de la finca</td>
              <?php foreach ($properties as $property) { ?>
              <td><?= $property->ubicacion_text ?></td>
              <?php } ?>           
            </tr>
            <tr>
              <td class="rigt-align">Habitaciones</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->numero_de_habitaciones_text ?></td>
              <?php } ?>  
            </tr>
            <tr>
              <td class="rigt-align">Acomodación max.</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->capacidad_text ?></td>
              <?php } ?>
            </tr>
            <tr>
              <td class="rigt-align">Baños</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->numero_de_banos_text ?></td>
              <?php } ?>             
            </tr>
            <tr>
              <td class="rigt-align">Área</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->area_text ?>m<span class="rank">2</span></td>
              <?php } ?> 
            </tr>
 
            <tr>
              <td class="rigt-align">Parqueaderos</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->parqueaderos_text ?></td>
              <?php } ?> 
            </tr>
                        <tr>
              <td class="rigt-align">Número de huespedes</td>
              <?php foreach ($properties as $property) { ?>
              <td class="available"><?= $property->capacidad_text ?></td>
              <?php } ?> 
            </tr> 
            <tr class="buttons">
              <td></td>
              <?php foreach ($properties as $property) { ?>
              <td data-package="free"><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><button type="button" class="btn btn-small btn-primary">Ver más</button></a></td>
              <?php } ?>
            </tr>
          </tbody>
        </table>
        <?php }else{ ?>
        	<h4>No hay fincas que comparar</h4>
        <?php } ?>
      </div>
    </section>
  </div>
</div>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>