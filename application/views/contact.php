<div id="page-content">
	<div class="banner_int" style="background-image:url(<?= base_url() ?>admin/uploads/files/<?= $banner->imagen_file ?>)"></div>
	<div class="container">
		<div class="row">
			<div class="contact-form">
				<h2><?= $banner->titulo_text ?></h2>
				<h6><?= $banner->subtitulo_text ?></h6>
				<form id="contactForm" class="form-submit">
					<div class="col-md-6">
						<div class="input-group">
							<label>Nombre:</label>
							<input name="nombre_text" type="text" class="form-control input-send" placeholder="Nombre" required>
						</div>
					</div>
					<div class="col-md-6">
						<div class="input-group">
							<label>Email:</label>
							<input name="email_text" type="text" class="form-control input-send" placeholder="Email" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="input-group">
							<label>Celular:</label>
							<input name="celular_text" type="text" class="form-control input-send" placeholder="Celular" required>
						</div>
					</div>
					<div class="col-md-12">
						<div class="input-group">
							<label>Mensaje:</label>
							<textarea id="text-area-contact" name="mensaje_textarea" rows="8" cols="45" class="input-send form-control"></textarea>
						</div>
					</div>
					<div class="submit">
						<span class="ffs-bs"><button type="submit" class="btn btn-large btn-primary" style="color:#fff;">Enviar</button></span>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>

<script>
	$(document).ready(function() {
		$('#contactForm').submit(function(event) {
			event.preventDefault();
			data = {};
			$('#contactForm .input-send').each(function(index, el) {
				data[ $(this).attr('name') ] = $(this).val();
			});

			$.ajax({
			  url: $('#base_url').val()+'home/contact',
			  type: 'POST',
			  data: data
			}).done(function(response) {
				alert('Gracias, el mensaje ha sido enviado.');
				$('#errorReportForm .input-send').val('')
			});

		});
	});
</script>