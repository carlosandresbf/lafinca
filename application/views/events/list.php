<div id="page-content">
	<div class="text-center" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $image_events->imagen_file ?>) no-repeat center;background-size:cover;">
		<div class="header-pattern">
			<div class="container blog-sm">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="header-text">
							<h1>Eventos</h1>
							<p><?= $image_events->subtitulo_text ?></p>
						</div>
					</div>
				</div>	
			</div>	
		</div>
	</div>
	<div class="navigation blog-nv blog-small">
		<header class="navbar" id="top">  
			<div class="site-header"></div>
		</header><!-- /.navbar -->
	</div>
	<div class="container top-indent">
		<div class="row">
			<!-- Content -->
			<div class="col-md-12 col-sm-12">
				<section id="posts">
					<div class="row grid">
						<?php foreach ($events as $event) { ?>
						<div class="col-md-6 col-sm-6 col-xs-12 grid-item">
							<article class="blog-post type-post format-image">
								<div class="header-text">
									<a href="<?= base_url() ?>events/show/<?= url_encode($event->titulo_text) ?>"><h3><?= $event->titulo_text ?></h3></a>
								</div>
								<figure class="meta">
									<div class="post-date">
										<a href="#" class="link-icon"><i class="fa fa-map-marker" aria-hidden="true"></i> ¿Donde?: <?= $event->lugar_text ?></a>
									</div>
								</figure>
								<div class="post-thumbnail"><a href="<?= base_url() ?>events/show/<?= url_encode($event->titulo_text) ?>" title="" rel="bookmark"><img src="<?= base_url() ?>admin/uploads/files/<?= $event->imagen_lista_file ?>"></a></div>						
								<?= clip_text($event->contenido_textarea) ?>
								<div class="button-continue">
									<a href="<?= base_url() ?>events/show/<?= url_encode($event->titulo_text) ?>">Ver todo le artículo</a>
								</div>
							</article>
						</div>
						<?php } ?>
					</div>
				</section>
				<section id="pagination">
					<nav id="nav-below" class="site-navigation paging-navigation navbar">
						<ul class="pagination pagination-lg">
							<?= $pagination ?>
						</ul>
					</nav>
				</section>
			</div><!-- /.col-md-9 -->
			<!-- end Content -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>