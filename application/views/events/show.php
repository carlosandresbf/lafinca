<div id="page-content" class="blog-styles">
<div class="text-center" style="background: url(<?= base_url() ?>admin/uploads/files/<?= $event->imagen_interna_file ?>) no-repeat center;background-size:cover;">
	<div class="header-pattern">
		<div class="container blog-sm">
			<div class="row">
				<div class="col-md-12 col-sm-12">
					<div class="header-text">
						<h1><?= $event->titulo_text ?></h1>						
						<figure class="meta">
							<div class="post-date">
								<a href="#" class="link-icon"><i class="fa fa-map-marker" aria-hidden="true"></i>¿Donde?: <?= $event->lugar_text ?></a>
							</div>
						</figure>
					</div>	
				</div>
			</div>	
		</div>	
	</div>
</div>
<div class="navigation blog-nv">
	<header class="navbar" id="top">  
		<div class="container">
			<nav class="secondary col-xs-12">
				<ul class="nav navbar-nav">
					<li class="col-sm-2 col-sm-offset-2 back"><a href="<?= base_url() ?>events/grid/<? echo  $municipios->nombre_text ?>"><i class="fa fa-sort-desc"></i>Volver</a></li>
					
				</ul>				
			</nav><!-- /.navbar collapse-->
		</div>
	</header><!-- /.navbar -->
</div>
<div class="container blog-sm"><!-- Content -->
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<section id="content" style="margin-top:35px;">
				<?= $event->contenido_textarea ?>
			</section>
			<!-- /#content -->
		</div><!-- /.col-md-12 col-sm-12 -->
	</div><!-- /.row -->
</div><!-- /.container -->
<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>