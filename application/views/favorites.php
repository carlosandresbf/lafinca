<?php $data = $this->session->userdata('logged_front') ?>
<div id="page-content">
  <div class="container">
    <header><h2>Favoritos</h2><br>
    </header>
	<div class="wide-2">
	<div class="col-xs-12">
		<br><br>
		<div class="row">
	      	<?php if($properties){ ?>
	        <?php $i=0; foreach ($properties as $property){ ?>
				<div class="col-md-3 col-sm-4 col-xs-6 prop">
					<div class="wht-cont">
						<div class="exp-img-2" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
							<span class="filter"></span>
							<span class="ffs-bs show-gallery"><a gallery="op_<?= $property->id ?>" class="btn btn-small btn-primary">Ver fotos</a></span>
							<a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><span class="ffs-bs"><label class="btn btn-small btn-primary">Ampliar info</label></span></a>
							<div class="overlay">
								<div class="img-counter"></div>
							</div>
						</div>
						<div class="item-title">
							<h4><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><?= $property->nombre_text ?></a></h4>
							<p class="team-color"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->ubicacion_text ?></p>
							<p class="team-color"><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
							<p class="team-color"><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> cuartos</p>
	                        
						</div>
						<hr>
						<div class="item-title btm-part">
							<div class="row">
								<div class="col-md-8 col-sm-8 col-xs-8">
									<p>$<?= $property->precio_temporada_alta_number ?> / <?= $property->tiempo_select ?></p>
									<p class="team-color"><?= $property->modalidad_select ?></p>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4 favorite">
									<?php if(isset($data)){ ?>
									<div data-property="<?= $property->id ?>" class="bookmark <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
										<span class="title-add">Agregar a favoritos</span>
									</div>
									<?php } ?>
									<div class="compare <?= (in_array($property->id, $this->session->userdata('comparative')))? 'compare-added':'' ?>" data-property="<?= $property->id ?>" data-compare-state="empty">
										<span class="plus-add">Compara 3 fincas</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

	            <?php $i++; if ($i % 3 == 0) { 
		            echo "</div><div class='row'>";
		        }
		        ?>
			<?php } ?>
	        <?php }else{ ?>
	        	<h4>No hay fincas en tus favoritos</h4>
	        <?php } ?>
	        </div>
	</div>
	


  </div>
</div>
<input type="hidden" id="user_id" value="<?= $data['user_id'] ?>">
<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>