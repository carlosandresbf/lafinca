<style type="text/css">
.navigation .navbar .navbar-nav > li a {
	padding: 5px 16px;
    margin-right: 12px;
    margin-top: 20px;
    background-color: rgba(0,0,0,.4);
}
input[placeholder='A donde vas?']{
	    width: 100px !important;	
			}
</style>
<div id="page-content_home">
	<div class="home_1" id="background" data-images="<?php foreach ($banner_images as $images) { ?> admin/uploads/home/<?= $images->file ?>, <?php } ?>">
		<div class="header_title" style="margin-bottom:24px">
			<h1>Aquí encontrarás las mejores </h1>
			<h2 style="color:#fff">fincas turísticas en Colombia</h2>
		</div>
			<div id="form-submit" class="form-submit">
				
                
                
      <div class="search">
                
                
                <div class="selector col-md-12  col-sm-12 nopadding">
                <div class="row">
                	<div class="selector col-md-3  col-sm-3 nopadding">
						<select class="selection" id="rent-sale">
							<option value="Fincas de recreo" selected>Fincas de recreo</option>
							<option value="Para arrendar">Para arrendar</option>
							<option value="Para la venta">Para la venta</option>
						</select>
					</div>
                    
                        <div class="selector col-md-2  col-sm-2 nopadding">
                               <input type="text" placeholder="Nombre ó ID" class="form-control name_id " style="font-size: 17px;" value="" >
                         </div>
                               
					<div id="email-label" class="col-md-3 col-sm-3 nopadding noborders">
						<!--<i class="fa fa-location-arrow"></i>
						<input type="text" id="search_field" class="form-control" placeholder="Departamento">-->						<select class="selection" id="department-sale" style="border-radius: 0px !important; ">
                        <option value="" selected>A donde vas?</option>
							<?php $i=0; foreach ($departments as $department) { ?>
	                        	
								<option value="<?= $department->id ?>" <?= ($i==0)?'':'' ?>><?= $department->nombre_text ?></option>
							<?php $i++; } ?>
						</select>

					</div>
					<div class="col-sm-2 col-xs-12 rgt-bord nopadding" id="desde">
						<div class="form-group nopadding">
							<input id="date-from" placeholder="Desde">
							<span class="point"></span>
						</div>
					</div>
					<div class="col-sm-2 col-xs-12 nopadding" id="hasta">
						<div class="form-group nopadding" >
							<input id="date-to" class="border_radius_right" placeholder="Hasta">
							<span class="point"></span>
						</div>
					</div>
                  
                  <span class="ffs-bs col-md-2 col-sm-2" style=" display:none" id="b1">
						<a href="" class="link_search"><button class=" border_radius_left btn btn-small btn-primary">Buscar</button></a>
					</span> 
                  
                    
                </div>
                <div class="row">
               <div class="ffs-bs col-md-5 col-sm-5 nopadding selector ">
     
  </div>
   <span class="ffs-bs col-md-2 col-sm-2" id="b2">
						<a href="" class="link_search"><button class=" border_radius_left btn btn-small btn-primary">Buscar</button></a>
					</span> 
        <div class="ffs-bs col-md-5 col-sm-5 nopadding selector ">
     
  </div>                    
                    
                    
                </div>
                
                </div>
                  
                 
                  
                
                
			
					
				</div>
                
                
                
			</div>
	</div>
    
    <div class="wh_back">
	<div class="container">
		<div class="explore">
			<h2><?= $home->titulo_text ?></h2>
			<h5><?= $home->texto_text ?></h5>
		</div>
		<div class="row">
			<?php foreach ($videos as $video) { ?>
			<div class="col-md-4 col-sm-6">
				<a href="#modal" class="exp-img over" style="background: url(<?= base_url() ?>admin/uploads/files/<?= $video->imagen_file ?>) center;background-size: cover;" data-video="<?= $video->link_youtube_text ?>"></a>
			</div>
			<?php } ?>
		</div>
        
	</div>
    </div>
    
    
	<!-- end wide-1 -->
	<div class="wide-2" style="padding-top:20px;">
		<div class="container">
			<div class="explore">
				<h2>¿Como alquilar en 3 pasos?</h2>
			</div>
			<div class="row gifs">
				<?php foreach ($steps as $step) { ?>
				<div class="col-md-4 col-sm-4">
					<h3><?= $step->titulo_text ?></h3>
					<p><?= $step->descripcion_text ?></p>
                	<img src="<?= base_url() ?>admin/uploads/files/<?= $step->imagen_file ?>" />
                </div>
                <?php } ?>
			</div>
		</div>
	</div>

    <div class="wh_back" style="margin-top:10px; margin-bottom:0px;">
	<div class="container">
        <h3 class="h3_center"><span>Fincas recomendadas</span></h3>
        
		<div class="row">
			<?php foreach ($properties_featured as $property) { ?>
			<div class="col-md-4 col-sm-6">
				<a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>" class="exp-img" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
					<div class="img-info">
						<div class="about_room">
							<div class="box_icon"><?= $property->ubicacion_text ?></div>
                        	<div class="box_icon" data-balloon="<?= $property->numero_de_habitaciones_text ?> alcobas" data-balloon-pos="up">(<?= $property->numero_de_habitaciones_text ?>) <img src="<?= base_url() ?>assets/img/012.png" class="icon "></div>
                        	<div class="box_icon" data-balloon="<?= $property->numero_de_piscinas_text ?> piscina" data-balloon-pos="up">(<?= $property->numero_de_piscinas_text ?>) <img src="<?= base_url() ?>assets/img/013.png" class="icon"></div>
                        	<div class="box_icon" data-balloon="<?= $property->numero_de_banos_text ?> baños" data-balloon-pos="up">(<?= $property->numero_de_banos_text ?>) <img src="<?= base_url() ?>assets/img/014.png" class="icon"></div>
                        </div>
                        <span class="ffs-bs btn btn-small btn-primary mr_top">Ver más</span>
					</div>
				</a>
			</div>
			<?php } ?>
		</div>
	</div>


    <!-- end wide-1 -->
	<div class="wide-2 back_rosa" style="padding-top:20px;">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6 prop ">
                	<div class="box_tipe_a top_rightbox clearfix" style="border-right:solid 2px #aa1b33">
						<a href="contact">
                        <img src="<?= base_url() ?>assets/img/017.png" class="right" width="130px">
						<h2 class="left h2_top">Registra tu finca <br><span>en 5 minutos</span></h2>
                        </a>
                    </div>
                </div>
                
				<div class="col-md-6 col-sm-6 col-xs-6 prop">
                    <div class="box_tipe_a top_rightbox">
                        <img src="<?= base_url() ?>assets/img/018.png" class="left">
                        <div class="txt_comtact right">
                        	<span class="txt20"><?= $home->email_text ?></span><br>
                            <span class="tel_big"><?= $home->telefono_text ?></span><br>
                            <span class="when"><?= $home->horarios_text ?></span>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
    
    
	<!-- end wide-2 -->
	<div class="wide-3 carousel-full-width">
		<div class="explore">
			<h2>Hospedate con nosotros y conoce nuestros recomendados</h2>
        </div>
		<div id="owl-demo" class="owl-carousel owl-theme ">
			<?php foreach ($featured as $new) { ?>
			<a href="<?= base_url() ?>featured/show/<?= url_encode($new->titulo_text) ?>" class="item">
				<div class="image" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $new->imagen_lista_file ?>) center"></div>
				<div class="img-info">
					<div class="row">
						<div class="half">
							<h3><?= $new->titulo_text ?></h3>
							<h6><?= $new->subtitulo_text ?></h6>
						</div>
					</div>
				</div>
			</a>
			<?php } ?>
		</div>
	</div>
    
    

    
    
  <?php if($testimonials){ ?>
  <div class="container">
    <section class="block testimonials" style="margin-bottom:0px;">
      <header class="center">
        <h2 class="no-border">¿Que se dice de nosotros?</h2>
      </header>
      <div class="owl-carousel testimonials-carousel">
        <?php foreach($testimonials as $testimonial){ ?>
        <blockquote class="item">
          <aside class="cite">
            <p class="team-color">"<?= strip_tags($testimonial->testimonio_textarea) ?>"</p>
          </aside>
          <figure>
          	<div class="image">
              <img src="<?= base_url() ?>admin/uploads/files/<?= $testimonial->imagen_file ?>" alt="">
            </div>
            <p><?= $testimonial->nombre_text ?></p>
            <p class="team-color">Un cliente satisfecho</p>
          </figure>
        </blockquote>
        <?php } ?>
      </div><!-- /.testimonials-carousel -->
    </section><!-- /#testimonials -->
  </div>
  <?php } ?>
  
</div>

<!-- VIDEOS YOUTUBE -->
   
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
      <iframe width="100%" height="420" src="" id="iframe-video" frameborder="0" allowfullscreen></iframe>
  </div>
</div>


<script>
	$(document).ready(function() {
		$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#date-from" )
        .datepicker({
        	dateFormat: 'dd/mm/yy',
				   minDate: 0,
          defaultDate: "+1w",
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#date-to" ).datepicker({
      	dateFormat: 'dd/mm/yy',
		   minDate: 0,
        defaultDate: "+1w",
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
		
	});
	/*var input = document.getElementById('search_field');
	var options = {
	  types: ['(cities)'],
	  componentRestrictions: {country: 'co'}
	};
	autocomplete = new google.maps.places.Autocomplete(input, options);

	autocomplete.addListener('place_changed', function() {
	    var place = autocomplete.getPlace();
	    console.log(place)

	    var place = autocomplete.getPlace();

        $('#link_search').attr('href', '<?= base_url() ?>properties/search?type='+$('#rent-sale option:selected').val()+'&name='+place.name+'&lat='+place.geometry.location.lat()+'&lng='+place.geometry.location.lng());
	});*/

	$('.selection').selectize({
    	sortField: 'text',
    });

 $(document).on('change', '#rent-sale', function(event) {
    	event.preventDefault();

    	if ($('#rent-sale').val() == 'Para arrendar' || $('#rent-sale').val() == 'Para la venta') {			
			$('#desde').fadeOut( "slow", function() {});		
			$('#hasta').fadeOut( "slow", function() {});		
			$('#email-label').fadeOut( "slow", function() {});
			$('#b2').hide();
			$('#b1').show();;
			
    	}else{
			$('#desde').fadeIn( "slow", function() {});		
			$('#hasta').fadeIn( "slow", function() {});
			$('#email-label').fadeIn( "slow", function() {});
			$('#b2').show();
			$('#b1').hide();
    	}
    	
    });

    $(document).on('click', '.link_search', function(event) {
    	event.preventDefault();

    /*	if ($('#date-from').val() != '' && $('#date-to').val() != '') {*/
    		window.location.href = '<?= base_url() ?>properties/search?type='+$('#rent-sale option:selected').val()+'&department='+$('#department-sale option:selected').val()+'&start='+$('#date-from').val()+'&end='+$('#date-to').val()+'&services=&name_id='+$('.name_id').val();
    	/*}else{ type=Fincas%20de%20recreo&department=&start=&end=&services=&name_id=&price=400000;3105000
    		alert('Las fechas deben estar seleccionadas');
    	}*/
    	
    });

</script>