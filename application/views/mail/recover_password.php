<html>
	<body>
		<?php $url = base_url().'auth/update_password?token='.base64_encode($id.'/-/'.$hash); ?>
		<?= sprintf('<p>Hola %s.</p>
					<p>Hemos recibido una solicitud de recuperación de contraseña para el acceso a nuestro sistema de gestión de contenidos. Si usted no la ha solicitado, por favor, ignore este mensaje.</p>
					<p>Para escribir una nueva contraseña, haga clic en este enlace <a href="%s">%s</a> o copie y pegue la dirección en su navegador. Recuerde que esta URL sólo estará disponible durante 5 días a partir de hoy, %s.</p>
					<p>Feliz día,</p>
					<p><strong>Lafinca.com.co</strong></p>', $name, $url, $url, date('Y-m-d')) ?>
	</body>
</html>