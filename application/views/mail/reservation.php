<?php 		date_default_timezone_set('America/Bogota');?><html>
	<body>
		<p><strong>Nombres:</strong> <?= $nombre_text ?></p>
		<p><strong>Apellidos:</strong> <?= $apellido_text ?></p>
		<p><strong>Documento:</strong> <?= $documento_de_identidad_text ?></p>
		<p><strong>Telefono:</strong> <?= $telefono_text ?></p>
		<p><strong>Email:</strong> <?= $email_text ?></p>
		<p><strong>Direccion:</strong> <?= $direccion_text ?></p>
        <p><strong>Reserva realizada el:</strong> <?= date('Y-m-d H:i:s') ?></p>
		<p><strong>Numero de huespedes:</strong> <?= $numero_de_personas_number ?></p>
		<p><strong>Mensaje:</strong><br><?= $info_reserva_text ?></p>
	</body>
</html>