<style type="text/css">
.navigation .navbar .navbar-nav > li a {
	padding: 5px 16px;
    margin-right: 12px;
    margin-top: 20px;
    background-color: rgba(0,0,0,.4);
}
input[placeholder='A donde vas?']{
	    width: 100px !important;	
			}
</style>
<div id="page-content_home">
	<div class="home_1" id="background" data-images="<?php foreach ($banner_images as $images) { ?> admin/uploads/home/<?= $images->file ?>, <?php } ?>">
		<div class="header_title" style="margin-bottom:24px">
			<h1>ERROR 404</h1>
			<h2 style="color:#fff">La página que estas buscando no ha sido encontrada.</h2>
		</div>
			
	</div>
    
    
    
    
	<!-- end wide-1 -->
	<div class="wide-2" style="padding-top:20px;">
		<div class="container">
			<div class="explore">
				<h2>¿Como alquilar en 3 pasos?</h2>
			</div>
			<div class="row gifs">
				<?php foreach ($steps as $step) { ?>
				<div class="col-md-4 col-sm-4">
					<h3><?= $step->titulo_text ?></h3>
					<p><?= $step->descripcion_text ?></p>
                	<img src="<?= base_url() ?>admin/uploads/files/<?= $step->imagen_file ?>" />
                </div>
                <?php } ?>
			</div>
		</div>
	</div>

    <div class="wh_back" style="margin-top:10px; margin-bottom:0px;">


    <!-- end wide-1 -->
	
    
    
	<!-- end wide-2 -->
	<div class="wide-3 carousel-full-width">
		<div class="explore">
			<h2>Hospedate con nosotros y conoce nuestros recomendados</h2>
        </div>
		<div id="owl-demo" class="owl-carousel owl-theme ">
			<?php foreach ($featured as $new) { ?>
			<a href="<?= base_url() ?>featured/show/<?= url_encode($new->titulo_text) ?>" class="item">
				<div class="image" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $new->imagen_lista_file ?>) center"></div>
				<div class="img-info">
					<div class="row">
						<div class="half">
							<h3><?= $new->titulo_text ?></h3>
							<h6><?= $new->subtitulo_text ?></h6>
						</div>
					</div>
				</div>
			</a>
			<?php } ?>
		</div>
	</div>
    
    

    
    
  <?php if($testimonials){ ?>
  <div class="container">
    <section class="block testimonials" style="margin-bottom:0px;">
      <header class="center">
        <h2 class="no-border">¿Que se dice de nosotros?</h2>
      </header>
      <div class="owl-carousel testimonials-carousel">
        <?php foreach($testimonials as $testimonial){ ?>
        <blockquote class="item">
          <aside class="cite">
            <p class="team-color">"<?= strip_tags($testimonial->testimonio_textarea) ?>"</p>
          </aside>
          <figure>
          	<div class="image">
              <img src="<?= base_url() ?>admin/uploads/files/<?= $testimonial->imagen_file ?>" alt="">
            </div>
            <p><?= $testimonial->nombre_text ?></p>
            <p class="team-color">Un cliente satisfecho</p>
          </figure>
        </blockquote>
        <?php } ?>
      </div><!-- /.testimonials-carousel -->
    </section><!-- /#testimonials -->
  </div>
  <?php } ?>
  
</div>

<!-- VIDEOS YOUTUBE -->
   
<div class="remodal" data-remodal-id="modal" role="dialog" aria-labelledby="modal1Title" aria-describedby="modal1Desc">
  <button data-remodal-action="close" class="remodal-close" aria-label="Close"></button>
  <div>
      <iframe width="100%" height="420" src="" id="iframe-video" frameborder="0" allowfullscreen></iframe>
  </div>
</div>


<script>
	$(document).ready(function() {
		$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#date-from" )
        .datepicker({
        	dateFormat: 'dd/mm/yy',
				   minDate: 0,
          defaultDate: "+1w",
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#date-to" ).datepicker({
      	dateFormat: 'dd/mm/yy',
		   minDate: 0,
        defaultDate: "+1w",
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
		
	});
	/*var input = document.getElementById('search_field');
	var options = {
	  types: ['(cities)'],
	  componentRestrictions: {country: 'co'}
	};
	autocomplete = new google.maps.places.Autocomplete(input, options);

	autocomplete.addListener('place_changed', function() {
	    var place = autocomplete.getPlace();
	    console.log(place)

	    var place = autocomplete.getPlace();

        $('#link_search').attr('href', '<?= base_url() ?>properties/search?type='+$('#rent-sale option:selected').val()+'&name='+place.name+'&lat='+place.geometry.location.lat()+'&lng='+place.geometry.location.lng());
	});*/

	$('.selection').selectize({
    	sortField: 'text',
    });

 $(document).on('change', '#rent-sale', function(event) {
    	event.preventDefault();

    	if ($('#rent-sale').val() == 'Para arrendar' || $('#rent-sale').val() == 'Para la venta') {			
			$('#desde').fadeOut( "slow", function() {});		
			$('#hasta').fadeOut( "slow", function() {});		
			$('#email-label').fadeOut( "slow", function() {});
			$('#b2').hide();
			$('#b1').show();;
			
    	}else{
			$('#desde').fadeIn( "slow", function() {});		
			$('#hasta').fadeIn( "slow", function() {});
			$('#email-label').fadeIn( "slow", function() {});
			$('#b2').show();
			$('#b1').hide();
    	}
    	
    });

    $(document).on('click', '.link_search', function(event) {
    	event.preventDefault();

    /*	if ($('#date-from').val() != '' && $('#date-to').val() != '') {*/
    		window.location.href = '<?= base_url() ?>properties/search?type='+$('#rent-sale option:selected').val()+'&department='+$('#department-sale option:selected').val()+'&start='+$('#date-from').val()+'&end='+$('#date-to').val()+'&services=&name_id='+$('.name_id').val();
    	/*}else{ type=Fincas%20de%20recreo&department=&start=&end=&services=&name_id=&price=400000;3105000
    		alert('Las fechas deben estar seleccionadas');
    	}*/
    	
    });

</script>