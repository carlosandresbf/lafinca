<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="//v2.zopim.com/?44BBOM3CHygpC5BDQqzvrhD7XJ8ZHIaf";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");

/*$.get( "pages/get_menu_turismo", function( data ) {
  $( ".info-turistica-menu" ).html( data );
});
$.get( "pages/get_menu_eventos", function( data ) {
  $( ".info-events-menu" ).html( data );
});*/
</script>


<div class="navigation">
	<header class="navbar" id="top">  
		<div class="container">
			<div class="navbar-brand nav">
				<a class="navbar-brand nav logo" href="<?= base_url(); ?>" title="" rel="home">
					<img src="<?= base_url() ?>admin/uploads/files/<?= $config->picture ?>" />
				</a>
			</div>
                        <style>
.dropdown-submenu {
    position: relative;
}
.dropdown-submenu-more {
    position: relative;
}
.dropdown-submenu li a {
	padding: 5px 16px !important;
    margin-right: 12px!important;
    margin-top: 20px!important;
    background-color: rgba(0,0,0,.4)!important;
	color:#fff !important;
}

.dropdown-submenu .dropdown-menu{
	background-color:transparent !important;	
	border:0px !important;
	box-shadow: 0 0 0 rgba(0, 0, 0, 0) !important;}

.dropdown-submenu>.dropdown-menu {
    top: -6px !important;
    left: 10% !important;
    margin-top: 36px !important ;
    margin-left: -1px 10% !important;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu-more>.dropdown-menu {
    top: -6px !important;
    left: 100%;
    margin-top: -6px !important;
    margin-left: -1px 10% !important;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}


.dropdown-submenu-more:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}

.dropdown-submenu.pull-left {
    float: none;
}

.dropdown-submenu.pull-left>.dropdown-menu {
    left: -100%;
    margin-left: 10px;
    -webkit-border-radius: 6px 0 6px 6px;
    -moz-border-radius: 6px 0 6px 6px;
    border-radius: 6px 0 6px 6px;
}         
.sub a{
  margin-top: 11px !important;	
	}     </style>
			<nav class="primary">
				<ul class="nav navbar-nav pull-right">
					<li><a href="<?= base_url() ?>">Inicio</a></li>
					<li><a href="<?= base_url() ?>us">Nosotros</a></li>
					<li  class="dropdown-submenu"><a  tabindex="-1" href="<?= base_url() ?>tourism">Info. turistica</a>
                    <ul class="dropdown-menu info-turistica-menu">
					<?php menu_turismo()?>
                       </ul>
                    </li>
					<li  class="dropdown-submenu"><a href="<?= base_url() ?>events">Eventos</a>
                    <ul class="dropdown-menu info-events-menu">
					<?php menu_eventos()?>
                       </ul>
                       </li>
					<li><a href="<?= base_url() ?>contact">Contáctenos</a></li>
				</ul>
                <div id="absolute_logs">
                	<?php $data = $this->session->userdata('logged_front') ?>
                	<?php if(!isset($data)){ ?>
                		<div class="bt_gris" data-toggle="modal" data-target="#modal-login" id="login">Iniciar sesión</div>
                		<div class="bt_gris" data-toggle="modal" data-target="#modal-register" id="reg">Regístrarme</div>
                        <div class="bt_gris" onclick="window.open('<?= $config->blog ?>','_blank');" >Ir al blog</div>
                        
					<?php }else{ ?>
						<a href="<?= base_url(); ?>auth/logout"><div class="bt_gris">Cerrar Sesión</div></a>
						<a href="<?= base_url(); ?>favorites"> <div class="bt_gris">Favoritos</div></a>
					<?php } ?>
					<!-- <a href="<?= base_url(); ?>comparative"><div class="bt_gris">Ver comparación</div></a> -->
					
					<?php $session = $this->session->userdata('logged_front'); ?>
					<?php if($session){ ?>
						<div class="bt_gris" style="border-color:transparent;">Hola <?= $this->session->userdata('logged_front')['name'] ?></div>
					<?php } ?>
				</div>
            </nav>
		</div>
		<div class="site-header">
			<a href="#" data-toggle="dropdown" class="pull-right drop-left">Menu
				<div class="gamb-button"> 
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</div>
			</a>
			<div class="navbar-brand nav">
				<a class="navbar-brand nav logo" href="<?= base_url(); ?>" title="" rel="home">
					<img src="<?= base_url() ?>admin/uploads/files/<?= $config->picture ?>"> 
				</a>
			</div>

			<div class="mob-menu drop-close hidden">
				<a href="#" data-toggle="dropdown" class="pull-right drop-close hidden black-cross">Close
					<span class="cross"></span>
				</a>
				<nav class="secondary">	<br>				
					<ul class="nav navbar-nav ">
						<?php if($session){ ?>
						<li><a href="#" onclick="event.preventDefault();">Hola <?= $this->session->userdata('logged_front')['name'] ?></a></li>
						<hr>
						<?php } ?>
                        <li><a href="<?= base_url() ?>">Inicio</a></li>
						<li><a href="<?= base_url() ?>us">Nosotros</a></li>
						<li><a  href="<?= base_url() ?>tourism">Info. turistica</a></li>
						<li><a href="<?= base_url() ?>events">Eventos</a></li>
						<li><a href="<?= base_url() ?>contact">Contáctenos</a></li>
						<hr>
						<?php if(!isset($data)){ ?>
	                		<li><a href="" data-toggle="modal" data-target="#modal-login">Iniciar sesión</a></li>
	                        <li><a href="" data-toggle="modal" data-target="#modal-register">Regístrarme</a></li>
						<?php }else{ ?>
							<li><a href="<?= base_url(); ?>auth/logout">Cerrar Sesión</a></li>
							<li><a href="<?= base_url(); ?>favorites">Favoritos</a></li>
						<?php } ?>
						<li><a href="<?= base_url(); ?>comparative">Ver comparación</a></li>
            
                    </ul>				
                  
				</nav><!-- /.navbar collapse-->
			</div>
		</div>
	</header><!-- /.navbar -->
</div>

<div id="modal-olvide" class="modal" tabindex="-1">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header text-center">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h2>¿Olvidaste tu contraseña?</h2>
				<span>no te preocupes nosotros nos acordamos</span>
			</div>
			<form id="recoveryPasswordForm">
				<div class="modal-body">
					<div class="form-group">
						<input type="text" name="user-email" class="input-send" placeholder="Email" required>
						<span class="fa fa-at"></span>
					</div>
				</div>
				<div class="modal-footer">
					<div class="form-group clearfix">
						<button type="submit" class="btn btn-primary">Enviar</button>					
					</div>
				</div>		
			</form>
		</div>
	</div>
</div>