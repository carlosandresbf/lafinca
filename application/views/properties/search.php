<?php $data = $this->session->userdata('logged_front') ?>
<input type="hidden" id="user_id" value="<?= $data['user_id'] ?>">
<div id="page-content-search">
	<div class="container">
		<div class="wide_container_2">
			<div class="tabs" style="width:100%;">
				
				<!-- tab-links -->
					<div id="tab1" class="tab" style="display: block;">
                    <header class="col-md-8 col-xs-12 no-pad">
					<div class="location-map col-sm-3 col-xs-3">
						<div class="input-group"> 
<input type="hidden" id="active_comparations" value="0">
							<!--<input type="text" class="form-control" id="address-map" name="address" placeholder="Departamento">
							<i class="fa fa-search"></i>-->
							<select class="selection department">
                            
                        <option value="" selected>A donde vas?</option>
								<?php foreach ($departments as $department) { ?>
								<option value="<?= $department->id ?>" <?= (isset($_GET['department']) && $_GET['department']==$department->id)?'selected':''  ?>><?= $department->nombre_text ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="select-block col-sm-3 col-xs-3">
						<select class="selection capacity">
							<option selected value="">Capacidad</option>
							<option value="1-15" <?= (isset($_GET['capacity']) && $_GET['capacity']=='1-15')?'selected':''  ?>>De 1 a 15 personas</option>
							<option value="16-25" <?= (isset($_GET['capacity']) && $_GET['capacity']=='16-25')?'selected':''  ?>>De 16 a 25 personas</option>
							<option value="26-35" <?= (isset($_GET['capacity']) && $_GET['capacity']=='26-35')?'selected':''  ?>>De 26 a 35 personas</option>
							<option value="36-45" <?= (isset($_GET['capacity']) && $_GET['capacity']=='36-45')?'selected':''  ?>>De 36 a 45 personas</option>
							<option value="45-1000" <?= (isset($_GET['capacity']) && $_GET['capacity']=='45-1000')?'selected':''  ?>>Más de 45</option>
						</select>
					</div>
					<div class=" select-block col-sm-3 col-xs-3">
						<select class="selection type" id="filter-type">
							<option value="">Tipo</option>
							<option value="Fincas de recreo" <?= ((isset($_GET['type']) && $_GET['type']=='Fincas de recreo'))?'selected':''  ?>>Finca de recreo</option>
							<option value="Para arrendar" <?= (isset($_GET['type']) && $_GET['type']=='Para arrendar')?'selected':''  ?>>Para arrendar</option>
                            <option value="Para la venta" <?= (isset($_GET['type']) && $_GET['type']=='Para la venta')?'selected':''  ?>>Para la venta</option>
						</select>
					</div>
					<div class="select-block col-md-3 col-xs-3 last">
						<a class="options-button" id="toggle-link">Otros filtros</a>
					</div>
         
					<div class="options-overlay col-sm-offset-8 col-sm-4" id="hidden_content" style="display: none; ">
						<div class="row">
							<div class="col-xs-12 top-mrg">
								<div class="internal-container features">
									<label>Caracteristicas y servicios:</label>
                                    
                                   <?php 
								   $servicios = array();
								   $servicios = @explode(',',$_GET['services']);
								  // print_r($servicios);
								   ?>
									<section class="block">
										<section>
											<ul class="submit-features">
												<?php foreach ($services as $service) { ?>
												<li><div class="checkbox"><label><input type="checkbox" name="services[]" `<?=(in_array($service->id ,$servicios))?' checked ':'';?> class="filter-services" value="<?= $service->id ?>"><?= $service->nombre_text ?></label></div></li>
												<?php } ?>
											</ul>
										</section>
									</section>
								</div>
							</div>
						</div>
					</div><!-- options-overlay -->
					
                    </header>
                    
                  
                
                
                    <header class=" col-lg-8  col-md-8 col-sm-12 col-xs-12 no-pad my-header">
					<div class=" col-lg-3 col-md-3 col-sm-3 col-xs-12  internal-container">
						  <input type="text" placeholder="Nombre ó ID" class="form-control name_id"   name="name_id" style="border: 0px;     padding: 17px 0px 17px 10px;    width: 100%;"                       value="<?= (isset($_GET['name_id']))?$_GET['name_id']:'' ?>">
                          		
					</div>

<div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 wide-3 internal-container">
						
		<input id="date-from" placeholder="Desde" name="start"  value="<?= (isset($_GET['start']))?$_GET['start']:'' ?>">
                          		
					</div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 wide-3 internal-container">
						
		<input id="date-to" class="border_radius_right"  placeholder="Hasta" name="end" value="<?= (isset($_GET['end']))?$_GET['end']:'' ?>">
                          		
					</div>                          
                          
                          
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-4 wide-3 internal-container">
						
                          	<select class="selection order select-order select-order" data-filter="order">
													<option value="">Ordenar por:</option>
													<option value="precio_temporada_alta_number DESC">Mayor precio</option>
													<option value="precio_temporada_alta_number ASC">Menor precio</option>
													<option value="numero_de_visitas_text DESC">Más relevante</option>
												</select>
					
					</div>


                                            
                                            
                                            
					
 
                                            
				</header><!-- end header -->
				
                      <ul class="tab-links col-md-4 col-xs-12">
					<li class="col-lg-3 col-lg-offset-2 col-md-4 col-xs-4 no-pad active"><a href="#tab1" class="map2"><img src="<?= base_url() ?>assets/img/map.png" alt=""/>Mapa</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad"><a href="#tab2"><i class="fa fa-th" aria-hidden="true"></i>Grilla</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad"><a href="#tab3"><i class="fa fa-th-list"></i>Lista</a></li>
				</ul>
						<div class="sidebar col-sm-4 col-xs-12">
							<!-- Map -->
							<div id="map"></div>
							<!-- end Map -->
						</div><!-- sidebar -->
						<div class="content col-sm-8 col-xs-12">
							<!-- Range slider -->
							<div class="col-lg-10  col-md-10 col-sm-12 col-xs-12">
								<div class="row no-pad">
									<form method="post">
										<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
																							<label class="top-indent rank-price" >Rango de precio</label>
										
										</div>
										<div class="col-lg-9  col-md-8 col-sm-8 col-xs-12">
										
												<div class="price-range price-range-wrapper my-bar-price-range">
<input id="price_slider" class="price-input" type="text" name="price" value="<?php 
if(isset($_GET['price']) & !empty($_GET['price'])){ 
$price = explode(';', $_GET['price']);
if($price[0]){
echo $price[0];
}else {
echo '0';
}
echo ';';
if(@!empty($price[1])){
if($price[1]< $property_min_price ){$price[1] = $max_price;}
echo $price[1];}else{
echo '1';
	}
					} else{
if($property_min_price){
echo $property_min_price;
}else{
echo '0';	
	}
echo ';';
if($max_price){
echo $max_price;
}else{
echo '10000000'; }

} ?>">
												</div>
											
										</div>
									</form>
								</div><!-- row -->
							</div>	<!-- explore_grid -->
							<!-- End Range slider -->


<div class="col-lg-2  col-md-2 col-sm-12 col-xs-12 wide-2" style=" border-top: solid 0px #ccc !important;">
						<span class="ffs-bs item-price top-indent-filter bottom-indent-filter" >
							<a href="#" id="doFilter" class="btn btn-default btn-small">Filtrar <i class="fa fa-filter"></i></a>
						</span>
					</div>

							<div class="wide-2 top_size">
								<div class="col-xs-12">
									<div class="row">
										<?php if($properties){ ?>
										<?php $i=0; $j = 1; foreach ($properties as $property){ ?>
											<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 prop">
												<div class="wht-cont">
													<div class="exp-img-2" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
	                                                	<div class="letra"><?= $j ?> </div>
														<span class="filter"></span>
														<span class="ffs-bs show-gallery"><a gallery="op_<?= $property->id ?>" class="btn btn-small btn-primary">Ver fotos</a></span>
														<a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><span class="ffs-bs"><label class="btn btn-small btn-primary">Ampliar info</label></span></a>
														<div class="overlay">
															<div class="img-counter"></div>
														</div>
													</div>
													<div class="item-title">
														<h4><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><?= $property->nombre_text ?></a></h4>
														<p class="team-color"><i class="fa fa-barcode" aria-hidden="true" title="Código de la propiedad"></i> <?= $property->identificador_text ?></p>
														<p class="team-color"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->ubicacion_text ?></p>
														<p class="team-color"><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
														<p class="team-color"><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> cuartos</p>
	                                                    
													</div>
													<hr>
													<div class="item-title btm-part">
														<div class="row">
															<div class="col-md-8 col-sm-8 col-xs-8">
																<p>$<?= $property->price ?> / <?= $property->tiempo_select ?></p>
																<p class="team-color"><?= $property->modalidad_select ?></p>
															</div>
															<div class="col-md-4 col-sm-4 col-xs-4 favorite">
																<?php if(isset($data)){ ?>
																<div data-property="<?= $property->id ?>" class="bookmark <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
																	<span class="title-add">Agregar a favoritos</span>
																</div>
																<?php } ?>
																<div class="compare <?= (in_array($property->id, $this->session->userdata('comparative')))? 'compare-added':'' ?>" data-property="<?= $property->id ?>" data-compare-state="empty">
																	<span class="plus-add">Compara 3 fincas</span>
																</div>
															</div>
														</div>
                                                        <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 favorite">
                                                    <a class="comparation_link" style="display:none" href="https://www.lafinca.com.co/comparative"><div class="bt_gris">Ver comparación</div></a>
                                                    </div>
                                                    <div class="col-md-2 col-sm-2 col-xs-2 favorite"></div>
                                                    </div>
													</div>
												</div>
											</div>

								            
										<?php $i++; $j++; } ?>
                                        </div>
										<?php }else{ ?>
											<div class="col-md-12 prop">
												<h3> No hay resultados para este criterio de busqueda.</h3>
											</div>
										<?php } ?>
							        </div>
								</div><!-- end container -->
								<div class="col-xs-12 content_2 top-indent">
									<nav class="site-navigation paging-navigation navbar">
                                    <ul class="pagination pagination-lg">
										<?= $pagination  ?>
                                    </ul>
									</nav>
								</div>
							</div>	<!-- end wide-2 -->
						</div>	<!-- content -->
				
					<div id="tab2" class="tab">
					<div class="banner_int" style="   margin-bottom: 40px !important; background-image:url(<?= base_url() ?>assets/img/024.jpg)"></div>
						<ul class="here_man tab-links col-lg-12 col-lg-offset-12 col-md-12 col-md-offset-12">
					<li class="col-lg-3 col-lg-offset-2 col-md-4 col-xs-4 no-pad "><a href="#tab1" class="map2"><img src="<?= base_url() ?>assets/img/map.png" alt=""/>Mapa</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad active"><a href="#tab2"><i class="fa fa-th" aria-hidden="true"></i>Grilla</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad"><a href="#tab3"><i class="fa fa-th-list"></i>Lista</a></li>
				</ul>
					<div class="col-xs-12 content_2">
						<div class="col-md-10 col-md-offset-1">
								<!-- Range slider -->
								<div class="explore_grid" style="margin-bottom:30px">
									<div class="row">
										<form method="post">
											<div class="col-md-2 col-sm-3">
												<div class="form-inline">
													<label class="top-indent">Rango de precio:</label>
												</div>
											</div>
											<div class="col-md-5 col-sm-6">
												<div class="form-group">
													<div class="price-range price-range-wrapper"></div>
												</div>
											</div>
											<div class="col-xs-2 pull-right" style="padding-top:10px; border-top:0; margin-top:20px;">
												<span class="ffs-bs item-price">
													<a href="#" id="doFilter" class="btn btn-default btn-small">Filtrar <i class="fa fa-filter"></i></a>
												</span>
											</div>
											<div class="select-block no-border pull-right col-sm-2 col-xs-12">
												<select class="selection order select-order select-order" data-filter="order">
													<option value="">Ordenar por:</option>
													<option value="precio_temporada_alta_number DESC">Mayor precio</option>
													<option value="precio_temporada_alta_number ASC">Menor precio</option>
													<option value="numero_de_visitas_text DESC">Más relevante</option>
												</select>
											</div>	<!-- select-block -->
										</form>
									</div><!-- row -->
								</div>
								<!-- End Range slider -->
								<div class="wide-2">
									
									<div class="row">
										
										<?php if($properties){ ?>
										<?php $i=0; foreach ($properties as $property){ ?>

										<div class="col-md-3 col-sm-3 col-xs-6 prop">
											<div class="wht-cont">
												<div class="exp-img-2" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
													<span class="filter"></span>
													<span class="ffs-bs"><label for="op_<?= $property->id ?>" class="btn btn-small btn-primary">Ver fotos</label></span>
													<a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><span class="ffs-bs"><label class="btn btn-small btn-primary">Ampliar info</label></span></a>
													<div class="overlay">
														<div class="img-counter"></div>
													</div>
												</div>
												<div class="item-title">
													<h4><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><?= $property->nombre_text ?></a></h4>
													<p class="team-color"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->ubicacion_text ?></p>
													<p class="team-color"><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
													<p class="team-color"><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> cuartos</p>
                                                    
												</div>
												<hr>
												<div class="item-title btm-part">
													<div class="row">
														<div class="col-md-8 col-sm-8 col-xs-8">
															<p>$<?= $property->price ?> / <?= $property->tiempo_select ?></p>
															<p class="team-color"><?= $property->modalidad_select ?></p>
														</div>
														<div class="col-md-4 col-sm-4 col-xs-4 favorite">
															<?php if(isset($data)){ ?>
															<div data-property="<?= $property->id ?>" class="bookmark <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
																<span class="title-add">Agregar a favoritos</span>
															</div>
															<?php } ?>
															<div class="compare <?= (in_array($property->id, $this->session->userdata('comparative')))? 'compare-added':'' ?>" data-property="<?= $property->id ?>" data-compare-state="empty">
																<span class="plus-add">Compara 3 fincas</span>
															</div>
														</div>
													</div>
                                                    <div class="row">
                                                    <div class="col-md-8 col-sm-8 col-xs-8">
                                                    </div>
                                                    <div class="col-md-4 col-sm-4 col-xs-4 favorite">
                                                    <a href="https://www.lafinca.com.co/comparative"><div class="bt_gris">Ver comparación</div></a>
                                                    </div>
                                                    </div>
												</div>
											</div>
										</div>
										<?php $i++; if ($i % 4 == 0) { 
									            echo "</div><div class='row'>";
									        }
									        ?>
										<?php } ?>
										<?php }else{ ?>
											<div class="col-md-12 prop">
												<h3> No hay resultados para este criterio de busqueda.</h3>
											</div>
										<?php } ?>
									
									</div>
								</div>
								<!-- content_2 -->
							</div>
						</div>
						<div class="col-xs-12 content_2 top-indent">
									<nav class="site-navigation paging-navigation navbar">
                                    <ul class="pagination pagination-lg">
										<?= $pagination  ?>
                                    </ul>
									</nav>
								</div>
					</div>
					<div id="tab3" class="tab">
					<div class="banner_int" style="    margin-bottom: 40px !important; background-image:url(<?= base_url() ?>assets/img/024.jpg)"></div>
						<ul class="here_man tab-links col-lg-12 col-lg-offset-12 col-md-12 col-md-offset-12">
					<li class="col-lg-3 col-lg-offset-2 col-md-4 col-xs-4 no-pad"><a href="#tab1" class="map2"><img src="<?= base_url() ?>assets/img/map.png" alt=""/>Mapa</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad"><a href="#tab2"><i class="fa fa-th" aria-hidden="true"></i>Grilla</a></li>
					<li class="col-lg-3 col-md-4 col-xs-4 bdr-rgh no-pad active"><a href="#tab3"><i class="fa fa-th-list"></i>Lista</a></li>
				</ul>
						<div class="col-xs-12 content_2">
							<div class="col-lg-10 col-lg-offset-1 col-md-12">
								<!-- Range slider -->
								<div class="row">
									<form method="post">
										<div class="col-md-2 col-sm-3">
											<div class="form-inline">
												<label class="top-indent">Rango de precio</label>
											</div>
										</div>
										<div class="col-md-5 col-sm-7">
											<div class="form-group">
												<div class="price-range price-range-wrapper"></div>
											</div>
										</div>
										<div class="col-xs-2 pull-right" style="padding-top:10px; border-top:0; margin-top:20px;">
											<span class="ffs-bs item-price">
												<a href="#" id="doFilter" class="btn btn-default btn-small">Filtrar <i class="fa fa-filter"></i></a>
											</span>
										</div>
										<div class="select-block no-border pull-right col-sm-2 col-xs-12">
											<select class="selection order select-order" data-filter="order">
													<option value="">Ordenar por:</option>
													<option value="precio_temporada_alta_number DESC">Mayor precio</option>
													<option value="precio_temporada_alta_number ASC">Menor precio</option>
													<option value="numero_de_visitas_text DESC">Más relevante</option>
											</select>
										</div>	<!-- select-block -->
									</form>
								</div><!-- row -->
                                
                                <div style="height:20px"></div>
								<!-- End Range slider -->
								<div class="wide-2 w100">

									<?php if($properties){ ?>
									<?php $i=0; foreach ($properties as $property){ ?>
									<div class="row white">
										<div class="col-md-4 col-sm-4 prp-img">
											<div class="exp-img-2" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>) center;background-size: cover;">
												<span class="filter"></span>
												<span class="ffs-bs"><label for="op_<?= $property->id ?>" class="btn btn-small btn-primary">Ver fotos</label></span>
												<div class="overlay">
													<div class="img-counter"></div>
												</div>
											</div>
										</div>
										<div class="item-info col-lg-4 col-md-4 col-sm-4">
											<h4><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>"><?= $property->nombre_text ?></a></h4>
											<p class="team-color"><i class="fa fa-map-marker" aria-hidden="true"></i> <?= $property->ubicacion_text ?></p>
											<div class="block">
												<div class="col-md-6 col-sm-6 col-xs-6 cat-img">
													<p class="info-line"><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6 cat-img">
													<p class="info-line"><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> cuartos</p>
												</div>
											</div>
											<div class="col-md-6 col-sm-6 col-xs-6 line"></div>
											<div class="col-md-6 col-sm-6 col-xs-6 line"></div>
											<hr>
										</div>
										<div class="item-price col-lg-4 col-md-4 col-sm-4 col-xs-12">
											<div class="sum col-sm-12 col-xs-6">
												<p><?= $property->price ?> /<?= $property->tiempo_select ?></p>
												<p class="team-color"><?= $property->modalidad_select ?></p>
											</div>
											<span class="ffs-bs col-xs-12 btn-half-wth"><a href="<?= base_url() ?>properties/show/<?= url_encode($property->nombre_text) ?>" class="btn btn-default btn-small">Ver detalle <i class="fa fa-caret-right"></i></a></span>
											<div class="sum favorite col-sm-12 col-xs-6">
												<?php if(isset($data)){ ?>
                                                <div data-property="<?= $property->id ?>" class="bookmark col-xs-1 <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
													<span class="title-add">Agregar a favoritos</span>
												</div>
												<?php } ?>
												<p class="col-xs-4">Favorita</p>
												<div class="compare col-xs-1" data-compare-state="empty">
													<span class="plus-add">Compara 3 fincas</span>
												</div>
												<p class="col-xs-4">Comparar</p>
											</div>
										</div>
									</div>
									<?php } ?>
									<?php }else{ ?>
										<div class="col-md-12 prop">
											<h3> No hay resultados para este criterio de busqueda.</h3>
										</div>
									<?php } ?>

                           				
								</div>	
								<!-- end wide-2 -->
							</div>
						</div>
						<div class="col-xs-12 content_2 top-indent">
									<nav class="site-navigation paging-navigation navbar">
                                    <ul class="pagination pagination-lg">
										<?= $pagination  ?>
                                    </ul>
									</nav>
								</div>
					</div>
				</div><!-- tab-content -->
			</div><!-- tabs -->
		</div>
	</div>
</div>


<?php foreach ($properties as $property) { ?>
<div class="custom-galery" id="op_<?= $property->id ?>">
	<input type="checkbox" class="gal">
	<div class="lower"></div>
	<div class="overlay overlay-hugeinc">
		<label gallery="op_<?= $property->id ?>"></label>
		<nav>
			<!-- Owl carousel -->
			<div class="owl-carousel owl-theme carousel-full-width owl-demo-3">
				<?php $images = get_images_gallery($property->imagenes_destacadas_gallery); ?>
				<?php foreach ($images as $image) { ?>
				<div class="item" style="background-image: url(<?= base_url() ?>admin/uploads/<?= $image->folder ?>/<?= $image->file ?>);"></div>
				<?php } ?>
			</div>
			<!-- End Owl carousel -->
		</nav>
	</div>
</div>
<?php } ?>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
		//$('#address-map').val('<?= @$_GET['department'] ?>');

		$(document).on('click', '.show-gallery', function(event) {
			event.preventDefault();
			var gallery = $(this).find('a').attr('gallery');
			$('#'+gallery).find('.overlay').css({
				'opacity': '1',
				'visibility': 'visible'
			})
		});

		$(document).on('click', '.custom-galery .overlay label', function(event) {
			event.preventDefault();
			var gallery = $(this).attr('gallery');
			$('#'+gallery).find('.overlay').css({
				'opacity': '0',
				'visibility': 'hidden'
			})
		}); 
		
		<?php /*?>if( $(".price-input").length > 0) {
		
	        $(".price-input").each(function() {
							
	            var vSLider = $(this).slider({
	                from:  '<?= ($property_min_price)?str_replace('.','', $property_min_price):0;?>',
	                to: '<?= ($max_price)?str_replace('.','', $max_price):1 ?>',
	                smooth: true, 
	                round: 0,       
	                dimension: '$&nbsp;',
	                onstatechange: function( value ){
						
					    window.history.replaceState('', '', updateURLParameter(window.location.href, 'price', value));
						
					}
	            }); 

	            <?php if(isset($_GET['price']) & !empty($_GET['price'])){ $price = explode(';', $_GET['price']); ?>
							
			    vSLider.slider("value", '<?= ($price[0])?$price[0]:0 ?>','<?= (@!empty($price[1]))?@$price[1]:1 ?>')
				<?php } else{?>
				vSLider.slider("value", '<?= ($property_min_price)?str_replace('.','', $property_min_price):0; ?>','<?= ($max_price)?str_replace('.','', $max_price):10000000 ?>')
				<?php } ?>
	        });
	    }
	    <?php */?>
	        
	
			
								
	       $("#price_slider").slider({
	                from:  <?= ($property_min_price)?$property_min_price:0;?>,
	                to: <?= ($max_price)?$max_price:100 ?>,
	                smooth: true, 
	                round: 0,    
					value: "<?php 
if(isset($_GET['price']) & !empty($_GET['price'])){ 
$price = explode(';', $_GET['price']);
if($price[0]){
echo $price[0];
}else {
echo '0';
}
echo ';';
if(@!empty($price[1])){
	if($price[1]< $property_min_price ){$price[1] = $max_price;}
echo $price[1];}else{
echo '1';
	}
					} else{
if($property_min_price){
echo $property_min_price;
}else{
echo '0';	
	}
echo ';';
if($max_price){
echo $max_price;
}else{
echo '10000000'; }

} ?>",   
	                dimension: '$&nbsp;',
	                onstatechange: function( value ){
					
				    window.history.replaceState('', '', updateURLParameter(window.location.href, 'price',  $("#price_slider").val()));
						
					}
	            }); 

	            
	        
	  

	    $('.selection.type').selectize({
	    	sortField: 'text',
	    	onChange: function(value) {
	            if (value !== "") {
	            	window.history.replaceState('', '', updateURLParameter(window.location.href, 'type', value));
	            }
	        }
	    });

	    $('.selection.capacity').selectize({
	    	sortField: 'text',
	    	onChange: function(value) {
	            if (value !== "") {
	            	window.history.replaceState('', '', updateURLParameter(window.location.href, 'capacity', value));
	            }
	        }
	    });

	    $('.selection.department').selectize({
	    	sortField: 'text',
	    	onChange: function(value) {
	            if (value !== "") {
	            	window.history.replaceState('', '', updateURLParameter(window.location.href, 'department', value));
	            }
	        }
	    });

	    $('.selection.order').selectize({
	    	sortField: 'text',
	    	onChange: function(value) {
	            if (value !== "") {
	            	window.history.replaceState('', '', updateURLParameter(window.location.href, 'order', value));
	            }
	        }
	    });

    	$(".selectize-input input").attr('readonly','readonly');

    	$(document).on('click', '.compare', function(event) {
			event.preventDefault();
			var property = $(this).data('property');
			var actual =  $('#active_comparations').val();

			if ($(this).hasClass('compare-added')){
				url = 'remove_comparative';
				actual =  parseInt(actual) - 1;
			}else{
				url = 'add_comparative';
				actual =  parseInt(actual) + 1;}
				console.log(actual);
			$('#active_comparations').val(actual);
			if(actual > 1){$('.comparation_link').show();}else{$('.comparation_link').hide();}
			$(this).toggleClass('compare-added');
			
			$.ajax({
			  url: $('#base_url').val()+'comparative/'+url,
			  type: 'POST',
			  data: {property: property}
			}).done(function() {});
		});

	});

	function updateURLParameter(url, param, paramVal){
		parent.location.hash = ''
	    var newAdditionalURL = "";
	    var tempArray = url.split("?");
	    var baseURL = tempArray[0];
	    var additionalURL = tempArray[1];
	    var temp = "";
	    if (additionalURL) {
	        tempArray = additionalURL.split("&");
	        for (i=0; i<tempArray.length; i++){
	            if(tempArray[i].split('=')[0] != param){
	                newAdditionalURL += temp + tempArray[i];
	                temp = "&";
	            }
	        }
	    }

	    var rows_txt = temp + "" + param + "=" + paramVal;
	    return baseURL + "?" + newAdditionalURL + rows_txt;
	}

	/*var input = document.getElementById('address-map');
	var options = {
	  types: ['(cities)'],
	  componentRestrictions: {country: 'co'}
	};
	autocomplete = new google.maps.places.Autocomplete(input, options);

	autocomplete.addListener('place_changed', function() {
	    var place = autocomplete.getPlace();
	    console.log(place)

	    var place = autocomplete.getPlace();

        window.history.replaceState('', '', updateURLParameter(window.location.href, 'name', place.name));
        window.history.replaceState('', '', updateURLParameter(window.location.href, 'lat', place.geometry.location.lat()));
        window.history.replaceState('', '', updateURLParameter(window.location.href, 'lng', place.geometry.location.lng()));
	});*/

	$(document).on('click', '#doFilter', function(event) {
		event.preventDefault();
		var filter_services = '';
		$('.filter-services').each(function(index, el) {
			if ($(this).attr("checked")) {
		        filter_services += $(this).val()+',';
		    }
			
		});

		window.history.replaceState('', '', updateURLParameter(window.location.href, 'services', filter_services.slice(0,-1)));

		var name_id = $('.name_id').val();
		window.history.replaceState('', '', updateURLParameter(window.location.href, 'name_id', name_id));
		
		var desde_d = $('#date-from').val();
		window.history.replaceState('', '', updateURLParameter(window.location.href, 'start', desde_d));
		
		var hasta_d = $('#date-to').val();
		window.history.replaceState('', '', updateURLParameter(window.location.href, 'end', hasta_d));
		

		location.reload();
	});




	//createHomepageGoogleMap(lat,lng);

	



	$(document).ready(function() {
		$( function() {
    var dateFormat = "dd/mm/yy",
      from = $( "#date-from" )
        .datepicker({
        	dateFormat: 'dd/mm/yy',
				   minDate: 0,
          defaultDate: "+1w",
          numberOfMonths: 1
        })
        .on( "change", function() {
          to.datepicker( "option", "minDate", getDate( this ) );
        }),
      to = $( "#date-to" ).datepicker({
      	dateFormat: 'dd/mm/yy',
		   minDate: 0,
        defaultDate: "+1w",
        numberOfMonths: 1
      })
      .on( "change", function() {
        from.datepicker( "option", "maxDate", getDate( this ) );
      });
 
    function getDate( element ) {
      var date;
      try {
        date = $.datepicker.parseDate( dateFormat, element.value );
      } catch( error ) {
        date = null;
      }
 
      return date;
    }
  } );
				
		
	});
	
	
	
	
	$(document).ready(function(e) {
		
		
		

    if( document.getElementById('map') != null ){		
		
var locations = [
        <?php $i=1; foreach ($properties as $property){ ?>
        	<?php $latlng = explode(',',$property->ubicacion_map) ?>
			["<?= $property->nombre_text ?>", "<?= $property->ubicacion_text ?>", "$<?= $property->price ?> /<?= $property->tiempo_select ?><br> <?= $property->modalidad_select ?>", <?= ($latlng[0])?$latlng[0]:'4.690846' ?>, <?= ($latlng[1])?$latlng[1]:'-74.052783'; ?>, "<?= base_url()?>properties/show/<?= url_encode($property->nombre_text) ?>", "<?= base_url()?>admin/uploads/files/<?= url_encode($property->imagen_principal_file) ?>", "<?= base_url()?>assets/img/<?= $i; ?>.svg"],
			   
        <?php $i++; } ?>
        ];

var bounds = new google.maps.LatLngBounds();

// DEFINE YOUR MAP AND MARKER 
var map = null, marker = null; 
var myCenter = new google.maps.LatLng(4.690846, -74.052783);


function initialize() { 
    var mapProp = { 
         zoom: 8,
            zoomControl: true,
            streetViewControl: true,
            zoomControlOptions:{
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            streetViewControlOptions:{
                position: google.maps.ControlPosition.RIGHT_TOP
            },
            scrollwheel: false,
            center: new google.maps.LatLng(4.690846, -74.052783)
    }; 

     map = new google.maps.Map(document.getElementById('map'), mapProp);

	   var i;
        var newMarkers = [];
        for (i = 0; i < locations.length; i++){
            var pictureLabel = document.createElement("img");
            pictureLabel.src = locations[i][7];
            var boxText = document.createElement("div");
            
			infoboxOptions = {
                content: boxText,
                disableAutoPan: false,
                pixelOffset: new google.maps.Size(-100, 0),
                zIndex: null,
                alignBottom: true,
                boxClass: "infobox-wrapper",
                enableEventPropagation: true,
                closeBoxMargin: "0px 0px -8px 0px",
                closeBoxURL: "../assets/img/close-btn.png",
                infoBoxClearance: new google.maps.Size(1, 1)
            };
			
			
            var marker = new MarkerWithLabel({
                title: locations[i][0],
                position: new google.maps.LatLng(locations[i][3], locations[i][4]),
                map: map,
                labelContent: '<div class="marker-loaded"><div class="map-marker"><img src="' + locations[i][7] + '" alt="" /></div></div>',
                labelAnchor: new google.maps.Point(50, 0),
                labelClass: "marker-style"/*,
				animation: google.maps.Animation.DROP,*/
            });

            if (locations.length > 1) {
            	bounds.extend(marker.position);
            }else{
            	map.setCenter(marker.position);
            }
            
            newMarkers.push(marker);
            boxText.innerHTML =
            '<div class="infobox-inner">' +
            '<a href="' + locations[i][5] + '">' +
            '<div class="infobox-image" style="position: relative">' +
            '<img src="' + locations[i][6] + '">' + '<div><span class="infobox-price">' + locations[i][2] + '</span></div>' +
            '</div>' +
            '</a>' +
            '<div class="infobox-description">' +
            '<div class="infobox-title"><a href="'+ locations[i][5] +'">' + locations[i][0] + '</a></div>' +
            '<div class="infobox-location">' + locations[i][1] + '</div>' +
            '</div>' +
            '</div>';
            //Define the infobox
            newMarkers[i].infobox = new InfoBox(infoboxOptions);
            google.maps.event.addListener(marker, 'click', (function(marker, i){
                return function(){
                    for (h = 0; h < newMarkers.length; h++){
                        newMarkers[h].infobox.close();
                    }
                    newMarkers[i].infobox.open(map, this);
                }
            })(marker, i));
        }

        if (locations.length > 1) {
        	map.fitBounds(bounds);
        }
		
 /*       google.maps.event.addListener(map, 'click',  function() {
//Have to put this within the domready or else it can't find the div element (it's null until the InfoBox is opened)

  
                         infobox.close();

      
  
});  */
		google.maps.event.addDomListener(window, 'resize', function() {
   var center = map.getCenter();
   google.maps.event.trigger(map, "resize");
    map.setCenter(center);
				});
	 
}	
	setTimeout(function(){ initialize();  }, 2000);
	
	
		
		
	}
});

	
	</script>
    <style>
    .selectize-input input{
		width: 90px !important;
		}
    
    </style>