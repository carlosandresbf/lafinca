<?php $data = $this->session->userdata('logged_front') ?>
<input type="hidden" id="user_id" value="<?= $data['user_id'] ?>">
<div id="page-property-content">
	<div class="wide_container_3 carousel-full-width">
		<div class="tabs">
			<div class="tab-content">
				<div id="tab1" class="tab active">
					<!-- Owl carousel -->
					<div id="owl-demo-2" class="owl-carousel owl-theme">
						<?php $images = get_images_gallery($property->imagenes_destacadas_gallery); ?>
						<?php foreach ($images as $image) { ?>
						<div class="item">
							<div class="image" style="background: url(<?= base_url() ?>admin/uploads/<?= $image->folder ?>/<?= $image->file ?> ) center"></div>
						</div>
						<?php } ?>
					</div>
					<!-- End Owl carousel -->
				</div>
				<div id="tab22" class="tab">
					<!-- Map -->
					<div id="map4"></div>
					<!-- end Map -->
				</div>
			</div>
			<ul class="tab-links col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3">
				<li class="active col-xs-4"><a href="#tab1"><img src="<?= base_url() ?>assets/img/camera-black.png" alt=""/>Fotos</a></li>
				<li class="col-xs-4"><a href="#tab22" id="tb" class="map4"><img src="<?= base_url() ?>assets/img/map.png" alt=""/>Mapa</a></li>
			</ul>
		</div>
	</div>
	<div class="wide-2">
		<div class="container">
			<ol class="breadcrumb">
				<li><a href="<?= base_url() ?> ">Inicio</a></li>
				<li><a href="<?= base_url()  ?>properties/search?department=<?php echo $property->departamentos_relation?>&type=<?php echo $property->	modalidad_select?>">Propiedades</a></li>
				<li class="active"><?= $property->nombre_text ?></li>
			</ol>
		</div>
		<div class="container">
			<div class="row">
				<aside class="pr-summary col-md-4 col-xs-12">
					<form>
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hl-bl">
							<h2>$<?= $property->precio_temporada_alta_number ?><span id="modality">/<?= $property->tiempo_select ?></span></h2>
							<h5 class="team-color"><?= $property->modalidad_select ?></h5>
						</div>

						<div class="row">
							<div class="col-md-12 col-sm-6 col-xs-12">
								<div class="row">
									<div class="col-lg-5 col-md-6 col-xs-6 cat-img">
										<p><i class="fa fa-users" aria-hidden="true"></i> Capacidad <?= $property->capacidad_text ?></p>
									</div>
									<div class="col-lg-7 col-md-6 col-xs-6 cat-img cat-img">
										<p><i class="fa fa-bed" aria-hidden="true"></i> <?= $property->numero_de_habitaciones_text ?> habitaciones</p>
									</div>
								</div>
								<hr class="full-width">
							</div>
						</div>
						<div class="picker-block col-md-12 col-sm-6 col-xs-12">
							<div class="row">
								<h3 class="chek">Ver disponibilidad</h3>
								<div class="col-xs-6 rgt-bord">
									<div class="form-group">
										<input id="date-from" placeholder="Desde" readonly>
										<span class="point"></span>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="form-group">
										<input id="date-to" placeholder="Hasta" readonly>
										<span class="point"></span>
									</div>
								</div>
								<div id="price-aprox" style="display:none;margin-bottom:15px;"><strong>Precio aproximado $<span class="price"></span><span class="modality"></span></strong><br></div>
							</div>
                            
                            
                            
 
                            
                            
						</div>
                        
                        	<div class="row">
    <div class="col-md-12 col-sm-6 col-xs-12">
								<span class="ffs-bs">
									<a class="btn btn-large btn-primary show-my-gallery" id="work-pls" href="#">
									Solicitar Reserva
									</a>
								</span>
                                
    </div>
    </div>                           
                            
                            
							<div class="row">
								<div class=" col-xs-12">
									<?php if($property->administrator_picture){ ?><br />

									<div class="circle">
										<img src="<?= base_url() ?>admin/uploads/files/<?= $property->administrator_picture ?>" alt="">
									</div>
									<?php } ?>
									<div class="team-info">
										<h3><?= $property->administrator_name?></h3>
										<p class="team-color">Agente encargado</p> 
									</div>
								</div>
							</div>
                        
						<div class="row">
							<div class="col-md-12 col-sm-6 col-xs-12"><br />

								<span class="ffs-bs ffs-bs2">
									<a class="btn btn-large btn-primary" id="contact_agent_1" style="background-color:#FF2C4F; margin-bottom:26px;" href="<?= base_url() ?>agent/show/<?= $property->administrator_relation ?>">
									contactar agente
									</a>
								</span>
                                
<script>
$(document).ready(function(){
$(".ffs-bs2").mouseover(function() {
	//console.log('trye'+ $("#contact_agent_1").length);
 $("#contact_agent_1").css('background-color','#f45040');
  }).mouseout(function() {
	//  	console.log('tryes');
$("#contact_agent_1").css('background-color','#FF2C4F');	
  });

			});
</script>
								<div class="col-xs-12 fav-block">
									<?php if(isset($data)){ ?>
									<div data-property="<?= $property->id ?>" class="bookmark col-xs-6 <?= (verify_favorite($data['user_id'], $property->id))?'bookmark-added':'' ?>" data-bookmark-state="empty">
										<span class="title-add">Agregar a favoritos</span>
										<p class="col-xs-9 fav-text">Favorito</p>
									</div>
									<?php } ?>

									
									<div class="compare col-xs-6 <?= (in_array($property->id, $this->session->userdata('comparative')))? 'compare-added':'' ?>" data-property="<?= $property->id ?>" data-compare-state="empty">
										<span class="plus-add">Comparar</span>
										<p class="fav-text">Compara 3 fincas</p>
									</div>
                         

                                    
								</div>
							</div>
						</div>
                        
                                    <div class="row">
							<div style="display:none" class="col-md-12 col-sm-6 col-xs-12 ver_compracion">
                            <a href="https://www.lafinca.com.co/comparative">
                            <button type="button" class="btn pull-right btn-small btn-primary" id="form-blog-reply-submit">Ver comparación</button>
                            </a>
                               </div>  
                               </div>
					</form>
				</aside>
				<div class="pr-info col-md-8 col-xs-12">
					<h2><?= $property->nombre_text ?></h2>
					<?php if(!empty($property->identificador_text)){ ?>
					<h2><small>Identificador: <?= $property->identificador_text ?></small></h2>
					<?php } ?>
					<div class="map-marker"></div>
					<h5 class="team-color"><?= $property->ubicacion_text ?> &nbsp;&nbsp;|&nbsp;&nbsp;<i class="fa fa-eye"></i><?= $total_views->count+1 ?></h5>
					<?= $property->descripcion_textarea ?>
				</div>
				<div class="pr-info col-md-8 col-xs-12">
					<h3>Características y servicios:</h3>
					<section class="block">
						<ul class="submit-features">
							<?php $features = multirelacion_data($property->caracteristicas_y_servicios_multirelation, 'caracteristicas_y_servicios') ?>
							<?php foreach ($features as $feature) { ?>
							<li class="col-md-4 col-xs-4"><div ><?= $feature->nombre_text ?></div></li>
							<?php } ?>
						</ul>
					</section>
				</div>
				<div class="pr-info col-md-8 col-xs-12">
					<h3>Galería de fotos:</h3>
					<div class="row cust-row">
						<?php $images = get_images_gallery($property->galeria_de_imagenes_gallery); ?>
						<?php 
						$co=0;
						foreach ($images as $image) {?>
						<div class="col-xs-3 cust-pad">
							<div class="pr-img" style="background: url(<?= base_url() ?>admin/uploads/<?= $image->folder ?>/<?= $image->file ?>) center;">
                            	<span class="filter"></span>
								<label class="img-label pho" for="op" onclick="$('.owl-demo-3').trigger('to.owl.carousel', [<?=$co?>,1]);">+<?= count($images) ?> Más</label>
                            </div>
						</div>
						<?php $co++;  } ?>

					</div>
					
					<div class="row">
						<div class="col-xs-12">
							<?php if(!empty($property->video_text)){ ?>
							<h3>Video:</h3>
							<p><iframe src="<?= $property->video_text ?>" width="100%" height="300px"></iframe></p>
							<?php } ?>
							<hr>
							<div class="row">
								<section class="social-block col-sm-6 col-xs-12">
									<ul class="social">
										<li class="social-text">Compartir:</li>
										<li>
                                        <?php 	$images = get_images_gallery($property->galeria_de_imagenes_gallery); ?>	 <a href="<?= base_url($_SERVER['https_HOST'].$_SERVER['REQUEST_URI'] )?>" data-image="<?= $images[0]->folder ?>/<?= $images[0]->file ?>" data-title="LAFINCA.COM - Propiedad: <?=ucfirst(str_replace('_',' ', $this->uri->segment(3)))?>" data-desc="<?= str_replace('<p>','',str_replace('</p>','',$config->website_description ))?>" class="btnShare"><i class="fa fa-facebook"></i></a>
<script>
$(document).ready(function(e) {
    
$('.btnShare').click(function(){
elem = $(this);
postToFeed(elem.data('title'), elem.data('desc'), elem.prop('href'), elem.data('image'));

return false;
});

});
</script>
<?php /*?> <a class="facebook" st_image href="httpss://www.facebook.com/sharer/sharer.php?u=" target="blank"></a><?php */?>
                                        
                                        
                                        </li>
										<li><a class="twitter" href="https://twitter.com/share?text=<?= $property->nombre_text ?>&url=https://<?= $_SERVER['https_HOST'].$_SERVER['REQUEST_URI'] ?>" target="blank"><i class="fa fa-twitter"></i></a></li>
									</ul>
								</section>
								<p class="error-block col-sm-6 col-xs-12 pull-right"><a href="#" data-toggle="modal" data-target="#modal-error"><i class="fa fa-exclamation-triangle"></i>Reportar un error</a></p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
	<div class="wide_container_3">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-8">
					<header><h4><?= count($comments)  ?> Comentarios</h4></header>
					<ul class="comments">
						<?php foreach ($comments as $comment) { ?>
						<li class="comment">
							<div class="comment-wrapper">
								<div class="name pull-left"><?= $comment->nombre_text ?></div>
								<p><?= $comment->comentario_textarea ?>
								</p>
							</div>
						</li>
						<?php } ?>
					</ul>

					<span class="ffs-bs transparent">
						<button type="submit" id="show-reply-form" class="btn btn-default btn-large"><i class="fa fa-commenting"></i><span class="btn-text">Dejar comentario</span></button>
					</span>
					<section id="leave-reply">
						<header><h4>Dejar comentario</h4></header>
						<div class="clearfix">
							<aside>
								<!-- div class="rating rating-user">
									<div class="inner"></div>
								</div -->
							</aside>
						</div>
						<div class="row">
							<form id="form-single-property-reply" method="post">
								<div class="col-sm-6 col-xs-12">
									<input type="text" class="form-control input-send-comment" id="form-blog-reply-name" name="nombre_text" placeholder="Tu nombre" required>
								</div><!-- /.form-group -->
								<div class="col-sm-6 col-xs-12">
									<input type="email" class="form-control input-send-comment" id="form-blog-reply-email" name="email_text" placeholder="Tu email" required>
								</div><!-- /.form-group -->
								<div class="col-xs-12">
									<textarea class="form-control input-send-comment" id="form-blog-reply-message" rows="5" name="comentario_textarea" placeholder="Dejanos tu comentario"></textarea>
								</div><!-- /.form-group -->
								<input type="hidden" name="fincas_relation" id="property_id" class="input-send" value="<?= $property->id ?>">
								<div class="col-xs-12 btm-indent">
									<button type="submit" class="btn pull-right btn-small btn-primary" id="form-blog-reply-submit">Enviar</button>
								</div><!-- /.form-group -->
								<div id="form-rating-status"></div>
							</form><!-- /#form-contact -->
						</div>
					</section>
				</div>
				<?php if ($same_properties) { ?>
				<div class="col-md-4 col-xs-12 some-prp">
					<h4>Otras fincas que te pueden interesar</h4>
					<p class="team-color">Para <?= $property->modalidad_select ?></p>
					<hr>
					<?php foreach ($same_properties as $same_property) { ?>
					<div class="property-block-small">
						<a href="<?= base_url() ?>properties/show/<?= url_encode($same_property->nombre_text) ?> ">
							<div class="property-image-small" style="background: url(<?= base_url() ?>admin/uploads/files/<?= $same_property->imagen_principal_file ?>) center;"></div>
							<h3><?= $same_property->nombre_text ?></h3>
							<p class="team-color"><?= $same_property->ubicacion_text ?></p>
							<h4>$<?= $same_property->precio_temporada_alta_number ?>/<?= $same_property->tiempo_select ?></h4>
						</a>
					</div>
					<hr>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<div class="custom-galery" id="galle_form">
	<input type="checkbox" class="gal" id="op">
	<div class="lower"></div>
	<div class="overlay overlay-hugeinc">
		<label for="op"></label>
		<nav>
			<!-- Owl carousel -->
			<div class="owl-carousel owl-theme carousel-full-width owl-demo-3">
				<?php $images = get_images_gallery($property->galeria_de_imagenes_gallery); ?>
				<?php foreach ($images as $image) { ?>
				<div class="item" style="background-image: url(<?= base_url() ?>admin/uploads/<?= $image->folder ?>/<?= $image->file ?>);"></div>

				<?php } ?>
			</div>
			<!-- End Owl carousel -->
		</nav>
	</div>
</div>

<div class="custom-galery" id="op_Form">
	<input type="checkbox" class="gal" id="op">
	<div class="lower"></div>
	<div class="overlay overlay-hugeinc">
		<label for="op"></label>
            <!-- -->
	<div class="wide-2">
	<div class="container border_radius_right custom_form_modal">

						<header><h3>Información de la reserva</h3></header>
						<div class="clearfix">
							<aside>
								<!-- div class="rating rating-user">
									<div class="inner"></div>
								</div -->
							</aside>
						</div>
							
					
					<form id="form-single-property-reservation" method="post">	
                    	
						<div class="row form-group" >
                       	
                        <div class="col-sm-4 col-xs-12">
<input id="llegada_datetime" name="llegada_datetime" class="form-control input-send-reservation" placeholder="Fecha Inicio" readonly>
</div>
                                <div class="col-sm-2 col-xs-12">
														</div>
                                
                     <div class="col-sm-4 col-xs-12">
<input id="salida_datetime"  name="salida_datetime" class="form-control input-send-reservation" placeholder="Fecha Fin" readonly>									
								</div>
    			<div class="col-sm-2 col-xs-12"> </div>
                                
                                </div>
                    	
						<div class="row form-group" >
                       			
                        <div class="col-sm-6 col-xs-12">
									<input type="text" class="form-control input-send-reservation" id="form-blog-reply-name" name="nombre_text" placeholder="Tu Nombre" required>

 <input type="hidden" name="administrator_relation"   class="input-send-reservation" value="<?= $property->administrator_relation ?>">
  <input type="hidden" name="record_order" id="record_order"   class="input-send-reservation" value="">
  	                                                      
								</div><!-- /.form-group -->		
                          
                     <div class="col-sm-6 col-xs-12">
									<input type="text" class="form-control input-send-reservation" id="form-blog-reply-name" name="apellido_text" placeholder="Tu Apellido" required>
								</div><!-- /.form-group -->
                                </div>
                                
                        <div class="row form-group">			
                             <div class="col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control input-send-reservation" id="form-blog-reply-name" name="documento_de_identidad_text" placeholder="Numero de Documento" required> 
                                                        </div><!-- /.form-group -->
                                                        <div class="col-sm-6 col-xs-12">
                                                            <input type="email" class="form-control input-send-reservation" id="form-blog-reply-email" name="telefono_text" placeholder="Tu Teléfono" required>
                                                        </div><!-- /.form-group -->
                                                        </div>

                        <div class="row form-group">			
                             <div class="col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control input-send-reservation" id="form-blog-reply-name" name="email_text" placeholder="Tu E-mail" required>
                                                        </div><!-- /.form-group -->
                                                        <div class="col-sm-6 col-xs-12">
                                                            <input type="email" class="form-control input-send-reservation" id="form-blog-reply-email" name="direccion_text" placeholder="Tu Dirección" required>
                                                        </div><!-- /.form-group -->
                                                        </div>

<div class="row form-group">			
                             <div class="col-sm-6 col-xs-12">
                                                            <input type="text" class="form-control input-send-reservation" id="form-blog-reply-name" name="numero_de_personas_number" placeholder="Número de huespedes" onkeypress='return event.charCode >= 48 && event.charCode <= 57' required>
                                                        </div>
                                                        </div>
                                
                        <div class="row form-group">			
                                                        <div class="col-xs-12">
                                                            <textarea class="form-control input-send-reservation" id="form-blog-reply-message" rows="5" name="info_reserva_text" placeholder="Comentarios"></textarea>
                                                        </div><!-- /.form-group -->
                                                
                                                        <div class="col-xs-12">
                                                            <button type="submit" class="btn pull-right btn-small btn-primary" id="form-reservation-property">Enviar</button>
                                                        </div><!-- /.form-group -->
                                                        <div id="form-rating-status"></div>
                                                    
                                                </div><!-- /#form-contact -->
                                                </form>
					
    </div>
        <!-- -->
    </div>
             
                    
	</div>
</div>


<?php $this->load->view('partials/modal_error_report'); ?>

<script>
	$(document).ready(function() {		
	
	
		setTimeout(function(){
			//console.log('test');
				$.ajax({
		  	url: $('#base_url').val()+"properties/new_view",
		  	data: { property: '<?= $property->id ?>' },
		  	type: 'POST'
		}).done(function(){
			//console.log('all rigth');
			});
			},20);
	
	
		$(document).on('click', '#form-reservation-property',
		 function(event) {
			event.preventDefault();
			var data = {}; 
			if($('.input-send-reservation[name="record_order"]').val()=='' || $('.input-send-reservation[name="llegada_datetime"]').val()=='' || $('.input-send-reservation[name="salida_datetime"]').val()=='' || $('.input-send-reservation[name="nombre_text"]').val()=='' || $('.input-send-reservation[name="apellido_text"]').val() == '' || $('.input-send-reservation[name="telefono_text"]').val() == '' || $('.input-send-reservation[name="email_text"]').val()== '' || $('.input-send-reservation[name="direccion_text"]').val() == '' || $('.input-send-reservation[name="info_reserva_text"]').val() == '' ){
				if($('.input-send-reservation[name="record_order"]').val()!=''){
			alert('Falta uno o mas campos en el formulario.');	
				}else{
				var data2 = {};
data2['from'] = $('#llegada_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();
data2['to'] = $('#salida_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();;
IsAvailable(data2);		
					}
				}else{
					

					
					
					
			var comentario = $('.input-send-reservation[name="info_reserva_text"]').val();		
			comentario = comentario+" - Fechas de reserva solicitadas - Llegada: "+$("#date-from").val()+" - Salida: "+$("#date-to").val();
			$('.input-send-reservation[name="info_reserva_text"]').val(comentario);	
			$('.input-send-reservation').each(function(index, el) {
				data[ $(this).attr('name') ] = $(this).val();
			});

			data['fincas_relation'] = <?= $property->id ?>;
			$("#form-reservation-property").hide();
			$.ajax({
			  	url: $('#base_url').val()+"properties/new_reservation",
			  	data: data,
			  	type: 'POST'
			}).done(function() {
				$('.input-send-reservation[name="salida_datetime"], .input-send-reservation[name="llegada_datetime"], .input-send-reservation[name="record_order"], .input-send-reservation[name="nombre_text"], .input-send-reservation[name="apellido_text"], .input-send-reservation[name="documento_de_identidad_text"], .input-send-reservation[name="telefono_text"], .input-send-reservation[name="email_text"], .input-send-reservation[name="direccion_text"]').val('');
				$('.input-send-reservation[name="info_reserva_text"]').val('');
				        
			  	alert('Gracias, la solicitud de reserva ha sido enviada.\nPronto el agente se comunicará contigo.');
				$("#form-reservation-property").show();
			var gallery = 'op_Form'
			$('#'+gallery).find('.overlay').css({
				'opacity': '0',
				'visibility': 'hidden'
			});
			$('#'+gallery).find('input').css({
				'display': 'none'
			});
			
			});
			}
	
		});
	
		
		
		$(document).on('click', '.pho', function(event) {
			event.preventDefault();
			
			var gallery = 'galle_form'
			$('#'+gallery).find('.overlay').css({
				'opacity': '1',
				'visibility': 'visible'
			});
				$('#'+gallery).find('input').css({
				'display': 'block'
			});
			
		});
			

			$(document).on('click', '.show-my-gallery', function(event) {
			event.preventDefault();
			if($('#date-from').val()=='' || $('#date-to').val()==''){
				alert('Debes seleccionar un rango de fechas para tu reserva.');
				return false;
				}else{
			$("#llegada_datetime").val($('#date-from').val());
			$("#salida_datetime").val($('#date-to').val());
			
			var gallery = 'op_Form'
			$('#'+gallery).find('.overlay').css({
				'opacity': '1',
				'visibility': 'visible'
			});
				$('#'+gallery).find('input').css({
				'display': 'block'
			});
			}
		});

		$(document).on('click', '#op_Form .overlay label', function(event) {
			event.preventDefault();
			var gallery = 'op_Form'
			$('#'+gallery).find('.overlay').css({
				'opacity': '0',
				'visibility': 'hidden'
			})
		});
		
		
		$(document).on('click', "#galle_form .overlay label", function(event) {
			event.preventDefault();
			var gallery = 'galle_form'
			$('#'+gallery).find('.overlay').css({
				'opacity': '0',
				'visibility': 'hidden'
			})
		});


		var current_head = $('head').html();
		var add_head = '<meta property="og:title" content="<?= $property->nombre_text ?> | lafinca.com.co" />';
		add_head += '<meta property="og:url" content="https://<?= $_SERVER['https_HOST'].$_SERVER['REQUEST_URI'] ?>" />';
		add_head += '<meta property="og:type" content="website" />'
		add_head += '<meta property="og:description" content="<?= $property->nombre_text ?> | LaFinca.com.co" />'
		add_head += '<meta property="og:image" content="<?= base_url() ?>admin/uploads/files/<?= $property->imagen_principal_file ?>" />'

		$('head').append(add_head);

		$('#header').removeClass('menu-wht');

		$(document).on('submit', '#form-single-property-reply', function(event) {
			event.preventDefault();
			var data = {};
			$('.input-send-comment').each(function(index, el) {
				data[ $(this).attr('name') ] = $(this).val();
			});

			data['fincas_relation'] = <?= $property->id ?>

			$.ajax({
			  	url: $('#base_url').val()+"properties/new_comment",
			  	data: data,
			  	type: 'POST'
			}).done(function() {
				$('.input-send[type="text"], textarea.input-send').val('');
			  	alert('Gracias, el comentario ha sido enviado y está sujeto a previa aprobación')
			});
		});

	

		var disabledDays = ["<?= $disponibility ?>"];

 		$("#date-from").datepicker({
 			showOtherMonths: true,
dateFormat: 'yy-mm-dd',
 			selectOtherMonths: true,
 			beforeShowDay: function(date){
		        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
		        return [ disabledDays.indexOf(string) == -1 ]
		    },
		    onSelect: function(dateText) {
		    	var to = $('#date-to').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    	if (to != null) {
		    		var data = {};
		    		data['from'] = this.value;
		    		data['to'] = $('#date-to').datepicker({ dateFormat: 'yy-mm-dd' }).val();
					
					IsAvailable(data);
					getPrices(data);
		    	}

		    	
		  	}
 		});

 		$("#date-to").datepicker({
 			showOtherMonths: true,
dateFormat: 'yy-mm-dd',
 			selectOtherMonths: true,
 			beforeShowDay: function(date){
		        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
		        return [ disabledDays.indexOf(string) == -1 ]
		    },
		    onSelect: function(dateText) {
		    	var from = $('#date-from').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    	if (from != null) {
		    		var data = {};
		    		data['from'] = $('#date-from').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    		data['to'] = this.value;
					
					IsAvailable(data);
		    		getPrices(data);
		    	}
		  	}
 		});
		
		
 		$("#llegada_datetime").datepicker({
 			showOtherMonths: true,
dateFormat: 'yy-mm-dd',
 			selectOtherMonths: true,
 			beforeShowDay: function(date){
		        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
		        return [ disabledDays.indexOf(string) == -1 ]
		    },
		    onSelect: function(dateText) {
		    	var to = $('#salida_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    	if (to != null) {
		    		var data = {};
		    		data['from'] = this.value;
		    		data['to'] = $('#salida_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();
					
					IsAvailable(data);
					getPrices(data);
		    	}

		    	
		  	}
 		});

 		$("#salida_datetime").datepicker({
 			showOtherMonths: true,
dateFormat: 'yy-mm-dd',
 			selectOtherMonths: true,
 			beforeShowDay: function(date){
		        var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
		        return [ disabledDays.indexOf(string) == -1 ]
		    },
		    onSelect: function(dateText) {
		    	var from = $('#llegada_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    	if (from != null) {
		    		var data = {};
		    		data['from'] = $('#llegada_datetime').datepicker({ dateFormat: 'yy-mm-dd' }).val();
		    		data['to'] = this.value;
		    		getPrices(data);
					IsAvailable(data);
		    	}
		  	}
 		});

 		function getPrices(data){
 			data['property_id'] = $('#property_id').val();
 			$.ajax({
			  	url: $('#base_url').val()+"properties/get_prices",
			  	data: data,
			  	type: "POST"
			}).done(function(response) {
			  	$('#price-aprox .price').text( response.price );
			  	$('#price-aprox .modality').text( $('#modality').text() );
			  	$('#price-aprox').fadeIn(200);
			});
 		}
		
		
 		function IsAvailable(data){
 			data['property_id'] = $('#property_id').val();
 			$.ajax({
			  	url: $('#base_url').val()+"properties/is_aviable",
			  	data: data,
			  	type: "POST"
			}).done(function(response) {
				console.log(response);
				if( response.response != '0'){
				alert("Uno o más dias seleccionados, no se encuentran disponibles para reservaciones.\nPor favor selecciona el rango de fechas nuevamente.");	
				$('#record_order').val('');	
					}else{
				$('#record_order').val('0');			
						}
			  	
			});
 		}

 		$(document).on('click', '.compare', function(event) {
			event.preventDefault();
			var property = $(this).data('property');

			if ($(this).hasClass('compare-added'))
				url = 'remove_comparative';
			else
				url = 'add_comparative';

			$(this).toggleClass('compare-added');
			$(".ver_compracion").toggle();
			

			$.ajax({
			  url: $('#base_url').val()+'comparative/'+url,
			  type: 'POST',
			  data: {property: property}
			}).done(function() {});
		});
		
 	});



$(document).ready(function(e) {
<?php $xplode_coordenadas = explode(',',$property->ubicacion_map)?>

var myCenter = new google.maps.LatLng( <?= ($xplode_coordenadas[0])?$xplode_coordenadas[0]:'4.690846' ?>, <?= ($xplode_coordenadas[1])?$xplode_coordenadas[1]:'-74.052783'; ?>);

// DEFINE YOUR MAP AND MARKER 
var map = null, marker = null; 
var latlng = {lat: <?= ($xplode_coordenadas[0])?$xplode_coordenadas[0]:'4.690846' ?>, lng:  <?= ($xplode_coordenadas[1])?$xplode_coordenadas[1]:'-74.052783'; ?>};

function initialize() { 
    var mapProp = { 
        center: myCenter, 
        zoom: <?=($property->ubicacion_map)?'15':'5';?>, mapTypeId: google.maps.MapTypeId.ROADMAP 
    }; 

     map = new google.maps.Map(document.getElementById("map4"), mapProp); 

	var marker = new MarkerWithLabel({
			position: latlng,
			map: map,
			labelContent: '<div class="marker-loaded"><div class="map-marker" style= " background-color: #FFF"><i style="color:#E52F51; font-size: 20px;    margin: 8px;" class="fa fa-circle" aria-hidden="true"></i></div></div>',
			labelClass: "marker-style"
		});
		var contentString =   '<div id="mapinfo">'+
		'<h4 class="firstHeading"><?= $property->nombre_text ?></h4>'+
		'<h6><?= $property->ubicacion_text ?></h6>';
		var infowindow = new google.maps.InfoWindow({
			content: contentString
		});
		marker.addListener('click', function() {
			infowindow.open(map, marker);
		});
		
        
		google.maps.event.addDomListener(window, 'resize', function() {
   			map.setCenter(myCenter);
			});
	 
}	
	
    $('#tb').click(function(){
		initialize();
	  google.maps.event.trigger(tb, 'resize');

        // Force the center of the map
        map.setCenter(myCenter);
		});
});

</script>