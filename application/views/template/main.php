<!DOCTYPE html>
<html lang="es-co">
<head>
	<meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0"> 
     <meta name="description" content="<?= $config->website_description ?>">
	<meta name="keywords" content="<?= $config->website_keywords ?>">    
    <meta name="author" content="Diseño web: imaginamos.com">
    <meta name="rating" content="general">
	<meta name="robots" content="All">

    <link href="<?= base_url() ?>assets/fonts/font-awesome.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="<?= base_url() ?>assets/css/jquery.slider.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/bootstrap/css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-select.min.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/selectize.css" type="text/css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/balloon.min.css" type="text/css">
    
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/remodal.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/remodal-default-theme.css">
    
    <link rel="shortcut icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon">
	<link rel="icon" href="<?= base_url() ?>favicon.ico" type="image/x-icon">

   
	
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7ftD8BnerWtJiFmCqWIpuO63HNrf95h0&v=3&libraries=places&callback=initialize"
  type="text/javascript"></script>

	<!-- End Modal login, register, custom gallery -->
<?php /*?>	<script src="https://maps.googleapis.com/maps/api/js?key= AIzaSyA7ftD8BnerWtJiFmCqWIpuO63HNrf95h0&v=3&libraries=places"></script><?php */?>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.js"></script>    
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.validate.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.placeholder.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/icheck.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/tmpl.js"></script>
   	<script type="text/javascript" src="<?= base_url() ?>assets/js/draggable-0.1.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.slider.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.dependClass-0.1.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/selectize.min.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/waypoints.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>assets/js/markerwithlabel_packed.js"></script>
    
    <script src="<?= base_url() ?>assets/js/remodal.js"></script>
    

	<?php if(!empty($config->google_analytics_code)){ ?>
	<script>
	

	
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', '<?= $config->google_analytics_code ?>', 'auto');
      ga('send', 'pageview');

    </script>
    
	
	<?php } ?>
   <script>
   var map = null, marker = null;
   </script>


	<style>
	.remodal-bg.with-red-theme.remodal-is-opening,
	.remodal-bg.with-red-theme.remodal-is-opened {
	  filter: none;
	}

	.remodal-overlay.with-red-theme {
	  background-color: #f44336;
	}

	.remodal.with-red-theme {
	  background: #fff;
	}
	</style>

    
    
  
    
<script type="text/javascript">
_atrk_opts = { atrk_acct:"ZHEyn1QolK10WR", domain:"lafinca.com.co",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=ZHEyn1QolK10WR" style="display:none" height="1" width="1" alt="" /></noscript>



 
<?php if($this->uri->segment(1)=='properties' && $this->uri->segment(2)=='show'){
	$images = get_images_gallery($property->galeria_de_imagenes_gallery); ?>	
<meta property="og:url" content="<?=  base_url(uri_string()) ?>"/>
<meta property="og:title"  content="LAFINCA.COM - Propiedad: <?=ucfirst(str_replace('_',' ', $this->uri->segment(3)))?>" /> 
<meta property="og:description" content="<?= str_replace('<p>','',str_replace('</p>','',$config->website_description ))?>" />	
<meta property="og:image" content="<?= str_replace('www.','',base_url()) ?>admin/uploads/<?= $images[0]->folder ?>/<?= $images[0]->file ?>" />
<meta property="og:site_name" content="lafinca.com" />
<meta property="og:type" content="website" />
<meta property="fb:app_id" content="610124955859218" />  
<?php }else{?>
<meta property="og:url" content="<?=  base_url() ?>"/>
<meta property="og:title"  content="Alquiler Fincas Villavicencio | Eje Cafetero: LaFinca.com.co" /> 
<meta property="og:description" content="Lafinca.com.co te garantiza una experiencia fácil, rápida y segura para el alquiler de fincas en los Llanos Orientales y el Eje Cafetero… Aparta la tuya ya!" />	
<meta property="og:image" content="<?= str_replace('www.','',base_url()) ?>admin/uploads/files/logo_finca_200.jpg" />
<meta property="og:site_name" content="lafinca.com" />
<meta property="og:type" content="website" />
<meta property="fb:app_id" content="610124955859218" />  
<?php }?>
<title>Alquiler Fincas Villavicencio | Eje Cafetero: LaFinca.com.co</title>
  
</head>
<input type="hidden" id="base_url" value="<?= base_url() ?>">

<body class="page-homepage" id="page-top">
<script>
  // This is called with the results from from FB.getLoginStatus().
  var getFacebookData = function(){
	 
	      FB.api('/me?fields=email,name', function(response) {
   		$.ajax({
		  url: "<?=base_url()?>auth/loginfacebook",
		  data: response,
		  type: 'POST'
		}).done(function(response) {
			// console.log(response);
			if (response.redirect) {
				location.reload();
			}
		});
		
    });
	  						}
  
  function statusChangeCallback(response) {
   
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
    getFacebookData();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
     // document.getElementById('status').innerHTML = 'Please log ' +
                    FB.login(function(response) {
					   getFacebookData();
					 });
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
                     FB.login(function(response) {
						   getFacebookData();
						 });

    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
window.fbAsyncInit = function() {
    FB.init({
      appId      : '610124955859218',
    cookie     : true,  // enable cookies to allow the server to access 
    status		: true,
      xfbml      : true,
      version    : 'v2.9'
    });
    FB.AppEvents.logPageView();
	
	 $(".fb_login_imaginamos").click(function() {
		 		   checkLoginState();
                }); 
	
	<?php /*?>FB.getLoginStatus(function(response) {
    statusChangeCallback(response);
  });<?php */?>
   
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
	
				
   
   
<?php /*?>  window.fbAsyncInit = function() {
  FB.init({
    appId      : '610124955859218',//'1191889434168203',
    cookie     : true,  // enable cookies to allow the server to access 
    status		: true,
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use version 2.2
  });

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));<?php */?>

function postToFeed(title, desc, url, image){
var obj = {method: 'feed',link: url, picture: '<?= str_replace('www.','',base_url()) ?>admin/uploads/'+image,name: title,description: desc};
function callback(response){}
FB.ui(obj, callback);
}
  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    FB.api('/me?fields=email,name', function(response) {
      	$.ajax({
		  url: "auth/loginFacebook",
		  data: response,
		  type: 'POST'
		}).done(function(response) {
			if (response.redirect) {
				location.reload();
			}
		});
    });
  }
  

  
  
</script>


	<!-- Preloader -->
	<div id="page-preloader">
		<div class="loader-ring"></div>
		<div class="loader-ring2"></div>
	</div>
	<!-- End Preloader -->
	
	<!-- Wrapper -->
	<div class="wrapper">
		<!-- Start Header -->
		<div id="header" class="menu-wht">
			<?php 
			$this->load->view('partials/header_page'); ?>
		</div>
		<!-- End Header -->
		
		<!-- Page Content -->
		<?= $section ?>
		<!-- end Page Content -->

		<div class="h20"></div>

		<!-- Start Footer -->
		
		<footer id="page-footer">
			<div class="inner">
				<section id="footer-main">
					<div class="container">
						<div class="row">
							<div class="col-md-4 col-sm-4">
								<article>
									<h3>Redes sociales</h3>
									<div class="social_footer">
										<?php if(!empty($config->facebook)){ ?>
		                                	<a target="_blank" href="<?= $config->facebook ?>" class="social_line clearfix">
		                                    	<div class="box_social_icon facebook"><i class="fa fa-facebook" aria-hidden="true"></i></div>Facebook
		                                	</a>
	                                	<?php } ?>
	                                    
	                                    <?php if(!empty($config->twitter)){ ?>
	                                	<a target="_blank" href="<?= $config->twitter ?>" class="social_line clearfix">
		                                    <div class="box_social_icon twitter"><i class="fa fa-twitter" aria-hidden="true"></i></div> Twitter
	                                	</a>
	                                    <?php } ?>
										
										<?php if(!empty($config->google)){ ?>
	                                	<a target="_blank" href="<?= $config->google ?>" class="social_line clearfix">
		                                    <div class="box_social_icon google"><i class="fa fa-google-plus" aria-hidden="true"></i></div> Google+
	                                    </a>
	                                    <?php } ?>
	                                </div>
	                            </article>
							</div><!-- /.col-sm-3 -->
							<div class="col-md-3 col-sm-3">
								<article>
									<h3>Links de interés</h3>
	                                <ul>
	                                	<?php foreach ($pages as $page) { ?>
	                                		<?php if ($page->es_link_radio == 'Si') { ?>
	                                			<li><a href="<?= $page->link_text ?>" target="_blank"><?=  $page->titulo_text ?></a></li>
	                                		<?php }else{ ?>
	                                			<li><a href="<?= base_url() ?>pages/show/<?= url_encode($page->titulo_text) ?>"><?=  $page->titulo_text ?></a></li>
	                                		<?php } ?>
	                                		
	                                	<?php } ?>
	                                </ul>
								</article>
							</div><!-- /.col-sm-3 -->
							<div class="col-md-5 col-sm-5">
								<article class="contact-us">
									<h2>Suscribete a nuestro newsletter</h2>
	                                <div class="newsletter_box clearfix">
	                                	<input type="text" placeholder="e-mail" id="newsletter_email">
	                                    <input type="submit" value="suscribeme" id="new_newsletter">
	                                </div><br>
	                                  <center id="success_newsletter"><strong style="color:#ec6c2d;">Gracias por suscribirse a nuestro newsletter. <br><br></strong></center>
	                                  <center>Al dar click en "suscribirme" esta aceptando <br> nuestros  <a href="<?= base_url() ?>pages/show/terminos_y_condiciones" style="text-decoration:underline">términos y condiciones</a></center>
								</article>
							</div><!-- /.col-sm-3 -->
							<div class="col-md-12 col-sm-12">
								<span class="pull-right"><a href="#page-top" class="roll">Ir al principio</a></span>
							</div>
						</div><!-- /.row -->
					</div><!-- /.container -->
				</section><!-- /#footer-main -->
				<section id="footer-thumbnails" class="footer-thumbnails"></section><!-- /#footer-thumbnails -->
				<section id="footer-copyright">
					<div class="container">
						<?php 
						
						 $copy = get_copy_r();
						 echo $copy[0]->copy;
						 ?>
						
						
						<span class="pull-right">desarrollo web : <a href="#">imaginamos.com</a></span>
					</div>
				</section>
			</div><!-- /.inner -->
		</footer>



		<!-- End Footer -->
	</div>

	<!-- Modal login, register, custom gallery -->
	<div id="login-modal-open">
		<?php $this->load->view('partials/modal_log_in'); ?>
	</div>
	<div id="register-modal-open">
		<?php $this->load->view('partials/modal_register'); ?>
	</div>

	<script type="text/javascript" src="<?= base_url() ?>assets/js/infobox.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/custom.js"></script>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/custom-map.js"></script>
	<!--[if gt IE 8]>
	<script type="text/javascript" src="<?= base_url() ?>assets/js/ie.js"></script>
	<![endif]-->
</body>
</html>