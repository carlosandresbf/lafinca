<div id="page-content">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<?= base_url() ?>">Home</a></li>
			<li class="active">Gracias</li>
		</ol>
	</div>
	<div class="container">
		<div class="thank_you_block col-sm-8 col-sm-offset-2">
        	<img src="assets/img/029.png" width="200px"><p></p>
			<h1 style="color:#ff1b51">Gracias por tu registro</h1>
			<a href="<?= base_url() ?>" class="link-arrow">ir al inicio</a>
		</div>
	</div>
</div>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>