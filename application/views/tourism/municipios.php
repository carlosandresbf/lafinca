<div id="page-content">
	<div class="text-center" style="background:url(<?= base_url() ?>admin/uploads/files/<?= $image_tourism->imagen_file ?>) no-repeat center;background-size:cover;">
		<div class="header-pattern">
			<div class="container blog-sm">
				<div class="row">
					<div class="col-md-12 col-sm-12">
						<div class="header-text">
							<h1>Información Turistica</h1>
							<p><?= $image_tourism->subtitulo_text ?></p>
						</div>
					</div>
				</div>	
			</div>	
		</div>
	</div>
	<div class="navigation blog-nv blog-small">
		<header class="navbar" id="top">  
			<div class="site-header"></div>
		</header><!-- /.navbar -->
	</div>
	<div class="container top-indent">
		<div class="row">
			<!-- Content -->
			<div class="col-md-12 col-sm-12">
				<section id="posts">
					<div class="row grid">
						<?php foreach ($municipios as $event) { ?>
						<div class="col-md-6 col-sm-6 col-xs-12 grid-item">
							<article class="blog-post type-post format-image">
								<div class="header-text">
									<a href="<?= base_url() ?>tourism/grid/<?= url_encode($event->nombre_text) ?>"><h3><?= $event->nombre_text ?></h3></a>
								</div>
								<div class="post-thumbnail"><a href="<?= base_url() ?>tourism/grid/<?= url_encode($event->nombre_text) ?>" title="" rel="bookmark"><img src="<?= base_url() ?>admin/uploads/files/<?= $event->imagen_file ?>"></a></div>						
								</article>
						</div>
						<?php } ?>
					</div>
				</section>
				<section id="pagination">
					<nav id="nav-below" class="site-navigation paging-navigation navbar"> 
						<ul class="pagination pagination-lg">
							<?= $pagination ?>
						</ul>
					</nav>
				</section>
			</div><!-- /.col-md-9 -->
			<!-- end Content -->
		</div><!-- /.row -->
	</div><!-- /.container -->
</div>

<script>
	$(document).ready(function() {
		$('#header').removeClass('menu-wht');
	});
</script>