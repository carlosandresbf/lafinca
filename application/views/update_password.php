<div class="container into-auth">
    <div class="col-md-4 col-md-offset-4">
        <div class="card">
            <div class="header">
                <h2 class="title login">Nueva contraseña</h2>
            </div>
            <div class="content">
                <div class="header">
                    <h5 class="title"><?= $this->lang->line('new_password') ?></h5>
                </div>
                <div class="content">
                   <form id="newPasswordForm">
						<div class="modal-body">
							<div class="form-group">
								<input type="hidden" name="id" class="input-send" value="<?= $id ?>">
                                <input type="hidden" name="hash" class="input-send" value="<?= $hash ?>">
								<input type="password" name="password" class="new-password input-send" placeholder="Contraseña" required>
								<span class="fa fa-at"></span>
							</div>
						</div>
						<div class="modal-footer">
							<div class="form-group clearfix">
								<button type="submit" class="btn btn-primary">Enviar</button>					
							</div>
						</div>		
					</form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$('#newPasswordForm').submit(function(e){
		e.preventDefault();
		if ($('input.new-password').val() != "") {
			data = {};
			$('.input-send').each(function(index, el) {
				data[ $(this).attr('name') ] = $(this).val();
			});

			$.ajax({
			  	url: $('#base_url').val()+'auth/save_password',
			  	method: 'post',
			  	data: data;
			}).done(function(response) {
				alert(response.msg);
			});
		}
	})
</script>