 <div id="page-content" class="contact-us">
  <div class="container">
    <ol class="breadcrumb">
      <li><a href="<?= base_url() ?>">Home</a></li>
      <li class="active">Nosotros</li>
    </ol>
    <h2><?= $us->titulo_text  ?></h2>
    <?= $us->descripcion_textarea  ?>
    <div class="row">
      <div class="col-xs-6 txt-block">
        <?= $us->columna_izquierda_textarea  ?>
        
      </div>
      <div class="col-xs-6 txt-block">
        <?= $us->columna_derecha_textarea  ?>
      </div>
    </div>
    <h3>Información Comercial</h3>
    <p class="team-color">Establecimiento de Comercio: lafinca.com.co</p>
    <div class="row" style="padding-bottom:30px;">
      <div class="col-12 txt-block">
        <div class="circle">
          <a href="agent_profile.html">
            <img src="<?= base_url() ?>admin/uploads/files/<?= $us->imagen_file ?>" alt="">
          </a>
        </div>
        <div class="team-info col-sm-6 col-xs-12">
          <h4>Razón Social:  <?= $us->razon_social_text  ?></h4>
          <p class="team-color">Nit: <?= $us->nit_text  ?></p> 
          <hr class="separate">
          <p><i class="fa fa-phone"></i><a href="tel:3138192758">Teléfono: <?= $us->telefono_text  ?></a></p>
          <p><i class="fa fa-envelope"></i><?= $us->email_text  ?></p>
        </div>
      </div>
    </div>
  </div>
  <div id="fun-facts" class="block counting-numbers">
    <div class="container">
      <div class="row">
        <div class="fun-facts col-md-12">
          <div class="col-sm-3  underline">
            <div class="number-wrapper col-sm-12">
              <div class="number" data-from="1" data-to="<?= $us->fincas_para_alquilar_text ?>"><?= $us->fincas_para_alquilar_text ?></div>
              <figure>Fincas para alquilar</figure>
            </div><!-- /.number-wrapper -->
          </div><!-- /.col-md-3 col-sm-3 underline -->
          <div class="col-md-3 col-sm-3 underline">
            <div class="number-wrapper col-sm-12">
              <div class="number" data-from="1" data-to="<?= $us->clientes_satisfechos_text ?>"><?= $us->clientes_satisfechos_text ?></div>
              <figure>Clientes satisfechos</figure>
            </div><!-- /.number-wrapper -->
          </div><!-- /.col-md-3 col-sm-3 underline -->
          <div class="col-md-3 col-sm-3 underline">
            <div class="number-wrapper col-sm-12">
              <div class="number" data-from="1" data-to="<?= $us->propiedades_vendidas_text ?>"><?= $us->propiedades_vendidas_text ?></div>
              <figure>Propiedades vendidas</figure>
            </div><!-- /.number-wrapper -->
          </div><!-- /.col-md-3 col-sm-3 underline -->
          <div class="col-md-3 col-sm-3 underline">
            <div class="number-wrapper col-sm-12">
              <div class="number" data-from="1" data-to="<?= $us->propiedades_arrendadas_text ?>"><?= $us->propiedades_arrendadas_text ?></div>
              <figure>Propiedades arrendadas</figure>
            </div><!-- /.number-wrapper -->
          </div><!-- /.col-md-3 col-sm-3 underline -->
        </div><!-- /.fun-facts -->
      </div><!-- /.row -->
    </div>
  </div>
  <?php if($testimonials){ ?>
  <div class="container">
    <section class="block testimonials">
      <header class="center">
        <h2 class="no-border">¿Que se dice de nosotros?</h2>
      </header>
      <div class="owl-carousel testimonials-carousel">
        <?php foreach($testimonials as $testimonial){ ?>
        <blockquote class="item">
          <aside class="cite">
            <p class="team-color">"<?= strip_tags($testimonial->testimonio_textarea) ?>"</p>
          </aside>
          <figure>
          	<div class="image">
              <img src="<?= base_url() ?>admin/uploads/files/<?= $testimonial->imagen_file ?>" alt="">
            </div>
            <p><?= $testimonial->nombre_text ?></p>
            <p class="team-color">Un cliente satisfecho</p>
          </figure>
        </blockquote>
        <?php } ?>
      </div><!-- /.testimonials-carousel -->
    </section><!-- /#testimonials -->
  </div>
  <?php } ?>
</div>
<script>
  $(document).ready(function() {
    $('#header').removeClass('menu-wht');
  });
</script>