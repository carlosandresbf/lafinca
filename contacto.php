<!doctype html>
<html>
<head>
<meta https-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$("#submit").click(function(e){
		e.preventDefault();
		var name = $("#name").val();
		var email = $("#email").val();
		var phone = $("#phone").val();
		var celphone = $("#celphone").val();
		var empresa = $("#empresa").val();
		var comment = $("#comment").val();
		if(name=="" || email=="" || phone=="" || celphone=="" || empresa=="" || comment==""){
			alert("Hay campos sin llenar en el formulario");
		}else{
			$.ajax({
				type	: "POST",
				data	: "name="+name+"&email="+email+"&phone="+phone+"&celphone="+celphone+"&empresa="+empresa+"&comment="+comment,
				url		: "test.php",
				success : function(msg){
					msg = jQuery.trim(msg);
					if(msg=="1"){
						$("#actionZone").show("fast");
					}else{
						alert("Hubo un error inesperado "+msg);
					}
				}
			});
		}
	});
});
</script>
</head>

<body>
<form action="contacto.php" method="post">
    <table width="400" border="0" cellspacing="2" cellpadding="0">
    <tr>
    <td width="29%" class="bodytext">Nombre:</td>
    <td width="71%"><input name="name" type="text" id="name" size="32"></td>
    </tr>
    <tr>
    <td width="29%" class="bodytext">E-mail:</td>
    <td width="71%"><input name="email" type="text" id="email" size="32" /></td>
    </tr>
    <tr>
    <td width="29%" class="bodytext">Telefono:</td>
    <td width="71%"><input name="phone" type="text" id="phone" size="32" /></td>
    </tr>
    <tr>
    <td width="29%" class="bodytext">Celular:</td>
    <td width="71%"><input name="celphone" type="text" id="celphone" size="32" /></td>
    </tr>
    <tr>
    <td width="29%" class="bodytext">Empresa:</td>
    <td width="71%"><input name="empresa" type="text" id="empresa" size="32" /></td>
    </tr>
    <tr>
    <td class="bodytext">Commentario:</td>
    <td><textarea name="comment" id="comment" class="bodytext" cols="45" rows="6" ></textarea></td>
    </tr>
    <tr>
    <td class="bodytext">&nbsp;</td>
    <td align="left" valign="top"><input type="submit" id="submit" name="Submit" value="Send"></td>
    </tr>
    </table>
</form> 
<div id="actionZone" style="display:none">Mensaje enviado exitosamente.</div>
</body>
</html>